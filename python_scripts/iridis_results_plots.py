"""
IRIDIS results plots
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

class setA_plots:

       def __init__(self):

              self.results = pd.read_csv("../IRIDIS_results/results_for_plots.csv",sep=";")

              self.folder = "../Latex_documents/images/Results_plots"
              self.ext    = ".png"

              self.unit_supply = self.unit_supply(self.results,self.folder,self.ext)
              self.non_unit_supply = self.non_unit_supply(self.results,self.folder,self.ext)

              self.plot_width  = 5
              self.plot_height = 5

              """
              Unit Supply

              1      n vs Time for all methods

              """

       class unit_supply:

              def __init__(self,results,folder,ext):

                     self.results = results
                     self.folder = folder
                     self.ext     = ext
                     self.unit_results = self.results.loc[self.results["single_supply"] == True]

                     self.plot_width  = 5
                     self.plot_height = 5
                     
                     # self.n_vs_time()
                     # self.n_vs_nodes()
                     # self.n_vs_time_ksb_methods()
                     # self.n_vs_nodes_ksb_methods()
                     # self.n_vs_time_mccormick()
                     # self.n_vs_nodes_mccormick()
                     # self.ratio_under_timelimit()
                     # self.ratio_solved_in_given_time()
                     # self.n_vs_time_with_non_unit_supply_methods()
                     # self.n_vs_time_algorithm_only_SMC()
                     # self.n_vs_nodes_algorithm_only_SMC()

                     # self.n_vs_time_every_combo()
                     # self.n_vs_nodes_every_combo()

                     # self.n_vs_time_smc()
                     # self.n_vs_nodes_smc()

                     self.n_vs_time_single_supply()
                     self.n_vs_nodes_single_supply()

                     self.n_vs_time_KSBCP_DMC()
                     self.n_vs_nodes_KSBCP_DMC()

                     self.n_vs_time_every_combo_2()
                     self.n_vs_nodes_every_combo_2()
                     self.ratio_under_given_time_every_combo_2()

                     self.n_vs_time_y_SMC_algorithm()
                     self.n_vs_nodes_y_SMC_algorithm()

                     self.M_vs_time()
                     self.B_vs_time()
                     self.M_vs_nodes()
                     self.B_vs_nodes()

              def M_vs_nodes(self):

                     pd.pivot_table(self.unit_results,
                                          values='No_Nodes',
                                          index=['M'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="M",
                                                               ylabel   ="Nodes",
                                                               style    = ['g-', 'g--',
                                          'y-', 'y--',
                                          'r-', 'r--']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_M_vs_nodes" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def B_vs_nodes(self):

                     pd.pivot_table(self.unit_results,
                                          values='No_Nodes',
                                          index=['B'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="B",
                                                               ylabel   ="Nodes",
                                                               style    = ['g-', 'g--',
                                          'y-', 'y--',
                                          'r-', 'r--']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_B_vs_nodes" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def M_vs_time(self):

                     pd.pivot_table(self.unit_results,
                                          values='Cropped_Time',
                                          index=['M'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="M",
                                                               ylabel   ="Time",
                                                               style    = ['g-', 'g--',
                                          'y-', 'y--',
                                          'r-', 'r--']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_M_vs_time" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def B_vs_time(self):

                     pd.pivot_table(self.unit_results,
                                          values='Cropped_Time',
                                          index=['B'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="B",
                                                               ylabel   ="Time",
                                                               style    = ['g-', 'g--',
                                          'y-', 'y--',
                                          'r-', 'r--']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_B_vs_time" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_single_supply(self):

                     single_supply = self.results.loc[self.results["single_supply"] == False]
                     single_supply = single_supply.loc[single_supply["Supply_max"] == 1]
                     single_supply = single_supply.loc[single_supply["Type"] == "MV"]
                     single_supply = single_supply.loc[single_supply["Algorithm"] == "SFCP"]
                     
                     A = pd.pivot_table(single_supply + self.unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean)

                     B = A.to_numpy()
                     geomean = np.zeros((1,24))
                     # print(A,B)

                     # for i in range(24):

                     #        print(B[:9,i])
                     #        geomean[0][i] = np.prod(B[:9,i])**(1/9)

                     # print(geomean)

                     # print(hi)
                     pd.pivot_table(pd.concat([self.unit_results,single_supply]),
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               style    = ['g.-', 'g*-',
                                                                             'bx-',
                                                                             'y.-', 'y*-',
                                                                             'r.-', 'r*-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_single_supply" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_single_supply(self):

                     single_supply = self.results.loc[self.results["single_supply"] == False]
                     single_supply = single_supply.loc[single_supply["Supply_max"] == 1]
                     single_supply = single_supply.loc[single_supply["Type"] == "MV"]
                     single_supply = single_supply.loc[single_supply["Algorithm"] == "SFCP"]
                     
                     A = pd.pivot_table(single_supply + self.unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean)

                     B = A.to_numpy()
                     geomean = np.zeros((1,24))
                     # print(A,B)

                     # for i in range(24):

                     #        print(B[:9,i])
                     #        geomean[0][i] = np.prod(B[:9,i])**(1/9)

                     # print(geomean)

                     # print(hi)
                     pd.pivot_table(pd.concat([self.unit_results,single_supply]),
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               style    = ['g.-', 'g*-',
                                                                           'bx-',  
                                                                             'y.-', 'y*-',
                                                                             'r.-', 'r*-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_single_supply" + self.ext,
                                   bbox_inches='tight')

                     plt.close()


              def n_vs_time_KSBCP_DMC(self):

                     KSBCP = self.unit_results.loc[self.unit_results["Algorithm"] == "KSBCP"]

                     pd.pivot_table(KSBCP.loc[KSBCP["McCormick_Type"] == "DMC"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_KSBCP_DMC" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_KSBCP_DMC(self):

                     KSBCP = self.unit_results.loc[self.unit_results["Algorithm"] == "KSBCP"]

                     pd.pivot_table(KSBCP.loc[KSBCP["McCormick_Type"] == "DMC"],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_KSBCP_DMC" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def ratio_under_given_time_every_combo_2(self):

                     # Method problems solved within a given time
                     A = pd.pivot_table(self.unit_results,#[self.unit_results["Time Limit"] == "UNDER"],
                                          values='Supply_max',
                                          index=['Time'],
                                          columns=["Type","McCormick_Type",'Algorithm'],
                                          aggfunc=np.count_nonzero,
                                          fill_value=0).cumsum()

                     A.to_csv("unit_ratio.csv",sep=";")

                     total = A.iloc[-1]

                     B = A/total

                     B = B.head(9524)

                     B.plot(xlabel   ="Time",
                            ylabel   ="Ratio",
                            ylim     =[0.8,1],
                            style    = ['g-', 'g--',
                                          'y-', 'y--',
                                          'r-', 'r--']).legend(bbox_to_anchor=(1.0, 1.0))


                     plt.savefig(self.folder + "/unit_supply_ratio_solved_in_time_algorithm" + self.ext,
                                   bbox_inches='tight')

                     plt.close()                     

              def n_vs_time_every_combo_2(self):

                     pd.pivot_table(self.unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               style    = ['g.-', 'g*-',
                                                                             'y.-', 'y*-',
                                                                             'r.-', 'r*-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_every_combo" + self.ext,
                                   bbox_inches='tight')

                     plt.close()
              
              def n_vs_nodes_every_combo_2(self):

                     pd.pivot_table(self.unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               style    = ['g.-', 'g*-',
                                                                             'y.-', 'y*-',
                                                                             'r.-', 'r*-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_every_combo" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_y_SMC_algorithm(self):

                     Y = self.unit_results.loc[self.unit_results["Type"] == "US"]

                     pd.pivot_table(Y.loc[Y["McCormick_Type"] == "SMC"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_y_SMC_algorithm" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_y_SMC_algorithm(self):

                     Y = self.unit_results.loc[self.unit_results["Type"] == "US"]

                     pd.pivot_table(Y.loc[Y["McCormick_Type"] == "SMC"],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     A = pd.pivot_table(Y.loc[Y["McCormick_Type"] == "SMC"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean)

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_y_SMC_algorithm" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time(self):

                     pd.pivot_table(self.unit_results,
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_algorithm" + self.ext)

                     plt.close()

              def n_vs_nodes(self):

                     pd.pivot_table(self.unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_algorithm" + self.ext)

                     plt.close()

              def n_vs_time_ksb_methods(self):

                     pd.pivot_table(self.unit_results.loc[self.unit_results['Algorithm'] == "KSBCP"],
                                          values='Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_ksb_methods" + self.ext)

                     plt.close()

              def n_vs_nodes_ksb_methods(self):

                     pd.pivot_table(self.unit_results.loc[self.unit_results['Algorithm'] == "KSBCP"],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_ksb_methods" + self.ext)

                     plt.close()

              def n_vs_time_mccormick(self):

                     pd.pivot_table(self.unit_results,
                                          values='Time',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_mccormick" + self.ext)

                     plt.close()

              def n_vs_nodes_mccormick(self):

                     pd.pivot_table(self.unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_mccormick" + self.ext)

                     plt.close()

              def n_vs_time_algorithm_only_SMC(self):

                     pd.pivot_table(self.unit_results.loc[self.unit_results["McCormick_Type"] == "SMC"],
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_algorithm_SMC_only" + self.ext)

                     plt.close()

              def n_vs_nodes_algorithm_only_SMC(self):

                     pd.pivot_table(self.unit_results.loc[self.unit_results["McCormick_Type"] == "SMC"],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_algorithm_SMC_only" + self.ext)

                     plt.close()

              def ratio_under_timelimit(self):

                     A = pd.pivot_table(self.unit_results[self.unit_results["Time Limit"] == "UNDER"],
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     B = pd.pivot_table(self.unit_results,
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     C = A/B
                     C.plot(xlabel   ="n",
                            ylabel   ="Ratio",
                            figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_ratio_under_timelimit_algorithm" + self.ext)

                     plt.close()

              def ratio_solved_in_given_time(self):

                     # Method problems solved within a given time
                     A = pd.pivot_table(self.unit_results[self.unit_results["Time Limit"] == "UNDER"],
                                          values='Supply_max',
                                          index=['Time'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero,
                                          fill_value=0).cumsum()

                     total = A.iloc[-1]

                     B = A/total

                     B.plot(xlabel   ="Time",
                            ylabel   ="Ratio",
                            ylim     =[0.8,1],
                            figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_ratio_solved_in_time_algorithm" + self.ext)

                     plt.close()

              def n_vs_time_with_non_unit_supply_methods(self):

                     self.single_supply_results = self.results.loc[self.results["Supply_max"] == 1]

                     pd.pivot_table(self.single_supply_results,
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                                ylabel   ="Time",
                                                                figsize  =(self.plot_width,self.plot_height))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_with_non_unit_methods" + self.ext)

                     plt.close()

              def n_vs_time_every_combo(self):

                     pd.pivot_table(self.unit_results,
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_every_combo" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_every_combo(self):

                     pd.pivot_table(self.unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_every_combo" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_smc(self):

                     pd.pivot_table(self.unit_results.loc[self.unit_results["McCormick_Type"] == "SMC"],
                                          values='Time',
                                          index=['n'],
                                          columns=['Algorithm',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_time_smc" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_smc(self):

                     pd.pivot_table(self.unit_results.loc[self.unit_results["McCormick_Type"] == "SMC"],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/unit_supply_n_vs_nodes_smc" + self.ext,
                                   bbox_inches='tight')

                     plt.close()


       class non_unit_supply:

              def __init__(self,results,folder,ext):

                     self.results = results
                     self.folder = folder
                     self.ext     = ext
                     self.non_unit_results = self.results.loc[results["single_supply"] == False]
                     self.results_with_best_ksbs()

                     self.n_vs_time_ybar_cutting_planes()
                     self.n_vs_nodes_ybar_cutting_planes()
                     
                     self.n_vs_time_algorithm()
                     self.n_vs_nodes_algorithm()

                     self.n_vs_time_type()
                     self.n_vs_nodes_type()

                     self.n_vs_time_mccormick()
                     self.n_vs_nodes_mccormick()

                     self.n_vs_time_gamma_ksb_delta()
                     self.n_vs_time_gamma_ksb_delta_plus()
                     self.n_vs_time_ybar_DMC_ksb_delta()
                     self.n_vs_time_ybar_DMC_ksb_delta_plus()
                     self.n_vs_time_ybar_SMC_ksb_delta()
                     self.n_vs_time_ybar_SMC_ksb_delta_plus()

                     self.n_vs_nodes_gamma_ksb_delta()
                     self.n_vs_nodes_gamma_ksb_delta_plus()
                     self.n_vs_nodes_ybar_DMC_ksb_delta()
                     self.n_vs_nodes_ybar_DMC_ksb_delta_plus()
                     self.n_vs_nodes_ybar_SMC_ksb_delta()
                     self.n_vs_nodes_ybar_SMC_ksb_delta_plus()


                     self.n_vs_time_ksb_delta()
                     self.n_vs_nodes_ksb_delta()

                     self.n_vs_time_ksb_delta_plus()
                     self.n_vs_nodes_ksb_delta_plus()

                     self.ratio_under_timelimit_algorithm()
                     self.ratio_under_timelimit_type()
                     self.ratio_under_timelimit_mccormick()

                     self.ratio_solved_in_given_time_algorithm()
                     self.ratio_solved_in_given_time_type()
                     self.ratio_solved_in_given_time_mccormick()
                     self.ratio_under_timelimit_every_combo()

                     # self.n_vs_time_every_combo()
                     # self.n_vs_nodes_every_combo()

                     self.n_vs_time_every_combo_best_ksbs()
                     self.n_vs_nodes_every_combo_best_ksbs()

                     # self.n_vs_time_lambda()
                     # self.n_vs_nodes_lambda()

                     # self.n_vs_no_solutions_lambda()
                     # self.n_vs_prop_lambda_stage_2_infeas()

                     self.supply_max_vs_time()
                     self.M_vs_time()
                     self.B_vs_time()
                     # self.s_max_vs_time()

              def results_with_best_ksbs(self):

                     non_ksb = self.non_unit_results.loc[self.non_unit_results["KSB_no_sols"].isin([0])]

                     best_gamma_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["GAMMA"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["Algorithm"].isin(["KSBnB"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["GAMMA"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["GAMMA"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_DMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["KSB_no_sols"].isin([4])]

                     best_ybar_DMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["KSB_no_sols"].isin([1])]


                     dataframe_lst = [non_ksb,best_gamma_delta,best_gamma_delta_plus,
                                          best_ybar_DMC_delta,best_ybar_DMC_delta_plus,
                                          best_ybar_SMC_delta,best_ybar_SMC_delta_plus]

                     self.results_best_ksbs = pd.concat(dataframe_lst)                     

              def s_max_vs_time(self):

                     pd.pivot_table(self.results_best_ksbs,
                                          values='Cropped_Time',
                                          index=['Supply_max'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="Supply_max",
                                                               ylabel   ="Time").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_supply_max_vs_time" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def M_vs_time(self):

                     pd.pivot_table(self.results_best_ksbs,
                                          values='Cropped_Time',
                                          index=['M'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="M",
                                                               ylabel   ="Time",
                                                               style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                                             'g.-','g,-',
                                                                             'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                                                                             'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_M_vs_time" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def B_vs_time(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['B'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="B",
                                                               ylabel   ="Time",
                                                               style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                                             'g.-','g,-',
                                                                             'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                                                                             'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_B_vs_time" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def supply_max_vs_time(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['Supply_max'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="Supply Max",
                                                               ylabel   ="Time",
                                                               style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                                             'g.-','g,-',
                                                                             'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                                                                             'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_supply_max_vs_time" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_ybar_cutting_planes(self):

                     results = self.non_unit_results.loc[self.non_unit_results["Type"] == "DI"]
                     results = results.loc[results["McCormick_Type"] == "SMC"]
                     results = results.loc[results["Algorithm"].isin(["SFCP","NSFCP","DB+","KSBnB+"])]

                     pd.pivot_table(results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               style     =["b--","g*-","r^-"])#.legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ybar_cutting_planes" + self.ext,
                                   bbox_inches='tight')

                     plt.close()    
                     
              def n_vs_nodes_ybar_cutting_planes(self):

                     results = self.non_unit_results.loc[self.non_unit_results["Type"] == "DI"]
                     results = results.loc[results["McCormick_Type"] == "SMC"]
                     results = results.loc[results["Algorithm"].isin(["SFCP","NSFCP","DB+","KSBnB+"])]

                     pd.pivot_table(results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               style     =["b--","g*-","r^-"])#.legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ybar_cutting_planes" + self.ext,
                                   bbox_inches='tight')

                     plt.close()    

              def n_vs_no_solutions_lambda(self):

                     pd.pivot_table(self.non_unit_results.loc[self.non_unit_results["Type"] == "LA"],
                                          values='No_Lambda_Sols',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Number of Solutions").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_no_solutions_lambda" + self.ext,
                                   bbox_inches='tight')

                     plt.close()    

              def n_vs_prop_lambda_stage_2_infeas(self):

                     lambda_results = self.non_unit_results.loc[self.non_unit_results["Type"] == "LA"]

                     A = pd.pivot_table(lambda_results.loc[lambda_results["Stage_2_Feas"] == "False"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     B = pd.pivot_table(lambda_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     C = A/B
                     C.plot(xlabel   ="n",
                            ylabel   ="Ratio")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_prop_lambda_stage_2_infeas" + self.ext)

                     plt.close()                                      

              def n_vs_time_lambda(self):

                     pd.pivot_table(self.non_unit_results.loc[self.non_unit_results["Type"] == "LA"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_lambda" + self.ext,
                                   bbox_inches='tight')

                     plt.close()                     

              def n_vs_nodes_lambda(self):

                     pd.pivot_table(self.non_unit_results.loc[self.non_unit_results["Type"] == "LA"],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_lambda" + self.ext,
                                   bbox_inches='tight')

                     plt.close()                     

              def n_vs_time_every_combo_best_ksbs(self):

                     non_ksb = self.non_unit_results.loc[self.non_unit_results["KSB_no_sols"].isin([0])]

                     best_gamma_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["MV"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["Algorithm"].isin(["KSBnB"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["MV"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["MV"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_DMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["KSB_no_sols"].isin([4])]

                     best_ybar_DMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["KSB_no_sols"].isin([1])]


                     dataframe_lst = [non_ksb,best_gamma_delta,best_gamma_delta_plus,
                                          best_ybar_DMC_delta,best_ybar_DMC_delta_plus,
                                          best_ybar_SMC_delta,best_ybar_SMC_delta_plus]

                     df = pd.concat(dataframe_lst)

                     subplots = [("DI","SMC"),("DI","DMC"),("MV","NAP")]#("LA","NAP"),

                     for subplot in subplots:

                            sub_data = df.loc[df["Type"] == subplot[0]]
                            sub_data = sub_data.loc[sub_data["McCormick_Type"] == subplot[1]]

                            pd.pivot_table(sub_data,
                                          values = "Cropped_Time",
                                          index = ["n"],
                                          columns = ["Algorithm"],
                                          aggfunc=np.mean).plot(xlabel="n",
                                                               ylabel="Time",
                                                               ylim  =[0,2000],
                                                               title ="(" + subplot[0] + "," + subplot[1] + ")",
                                                               style =['r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                            plt.savefig(self.folder + "/non_unit_supply_n_vs_time_every_combo_best_ksbs_" + subplot[0] + "_" + subplot[1] + self.ext,
                                          )#bbox_inches='tight')

                            plt.close()

                     # DM formulations

                     sub_data = df.loc[df["Type"].isin(["DMKKT","DMVF"])]
                     # sub_data = sub_data.loc[sub_data["McCormick_Type"] == subplot[1]]

                     pd.pivot_table(sub_data,
                                   values = "Cropped_Time",
                                   index = ["n"],
                                   columns = ["Type"],
                                   aggfunc=np.mean).plot(xlabel="n",
                                                        ylabel="Time",
                                                        ylim  =[0,2000],
                                                        title ="(DMKKT, NAP) and (DMVF, NAP)",
                                                        style =['r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_every_combo_best_ksbs_DM_NAP" + self.ext,
                                   )#bbox_inches='tight')

                     plt.close()

                     

                     # pd.pivot_table(pd.concat(dataframe_lst),
                     #                      values='Cropped_Time',
                     #                      index=['n'],
                     #                      columns=['Type',"McCormick_Type","Algorithm"],
                     #                      aggfunc=np.mean).plot(xlabel   ="n",
                     #                                           ylabel   ="Time",
                     #                                           style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                     #                                                         'g.-','g,-',
                     #                                                         'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                     #                                                         'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     # plt.savefig(self.folder + "/non_unit_supply_n_vs_time_every_combo_best_ksbs" + self.ext,
                     #               bbox_inches='tight')

                     # plt.close()

              def n_vs_nodes_every_combo_best_ksbs(self):

                     non_ksb = self.non_unit_results.loc[self.non_unit_results["KSB_no_sols"].isin([0])]

                     best_gamma_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["MV"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["Algorithm"].isin(["KSBnB"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["MV"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["MV"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_DMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["KSB_no_sols"].isin([4])]

                     best_ybar_DMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["DI"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["KSB_no_sols"].isin([1])]


                     dataframe_lst = [non_ksb,best_gamma_delta,best_gamma_delta_plus,
                                          best_ybar_DMC_delta,best_ybar_DMC_delta_plus,
                                          best_ybar_SMC_delta,best_ybar_SMC_delta_plus]

                     df = pd.concat(dataframe_lst)

                     subplots = [("DI","SMC"),("DI","DMC"),("MV","NAP")]#,("LA","NAP")

                     for subplot in subplots:

                            sub_data = df.loc[df["Type"] == subplot[0]]
                            sub_data = sub_data.loc[sub_data["McCormick_Type"] == subplot[1]]

                            if subplot[0] == "DI" and subplot[1] == "DMC":

                                   y_limit = [0,500000]

                            else:

                                   y_limit = [0,200000]

                            pd.pivot_table(sub_data,
                                          values = "No_Nodes",
                                          index = ["n"],
                                          columns = ["Algorithm"],
                                          aggfunc=np.mean).plot(xlabel="n",
                                                               ylabel="Nodes",
                                                               ylim  = y_limit,
                                                               title ="(" + subplot[0] + "," + subplot[1] + ")",
                                                               style =['r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                            plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_every_combo_best_ksbs_" + subplot[0] + "_" + subplot[1] + self.ext,
                                          )#bbox_inches='tight')

                            plt.close()

                     sub_data = df.loc[df["Type"].isin(["DMKKT","DMVF"])]
                     # sub_data = sub_data.loc[sub_data["McCormick_Type"] == subplot[1]]

                     pd.pivot_table(sub_data,
                                   values = "No_Nodes",
                                   index = ["n"],
                                   columns = ["Type"],
                                   aggfunc=np.mean).plot(xlabel="n",
                                                        ylabel="Nodes",
                                                        ylim  =[0,200000],
                                                        title ="(DMKKT, NAP) and (DMVF, NAP)",
                                                        style =['r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_every_combo_best_ksbs_DM_NAP" + self.ext,
                                   )#bbox_inches='tight')

                     plt.close()


                     A = pd.pivot_table(pd.concat(dataframe_lst),
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean)

                     pd.pivot_table(pd.concat(dataframe_lst),
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type","Algorithm"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                                             'g.-','g,-',
                                                                             'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                                                                             'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-'],
                                                               ylim     = [0,50000]).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_every_combo_best_ksbs" + self.ext,
                                   bbox_inches='tight')

                     plt.close()
                     
              def n_vs_time_algorithm(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               style    = ['b.-', 'y,-', 'g.-', 'r+-','b*-', 'yo-', 'rx-', 'g,-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_algorithm" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_algorithm(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_node_algorithm" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_type(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               style     = ['b,-','g,-','y,-','r,-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_type" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_type(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['Type',"McCormick_Type"],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_type" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_ksb_delta(self):

                     ksb_results = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_nodes_ksb_delta(self):

                     ksb_results = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_time_ksb_delta_plus(self):

                     ksb_results = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_nodes_ksb_delta_plus(self):

                     ksb_results = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_time_mccormick(self):

                     pd.pivot_table(self.non_unit_results.loc[self.non_unit_results["McCormick_Type"].isin(["DMC","SMC"])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time",
                                                               style    = ["y,-","r,-"]).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_mccormick" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_mccormick(self):

                     pd.pivot_table(self.non_unit_results.loc[self.non_unit_results["McCormick_Type"].isin(["DMC","SMC"])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes").legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_mccormick" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def ratio_under_timelimit_algorithm(self):

                     pd.pivot_table(self.non_unit_results[self.non_unit_results["Time Limit"] == "UNDER"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     A = pd.pivot_table(self.non_unit_results[self.non_unit_results["Time Limit"] == "UNDER"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     B = pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero)

                     C = A/B
                     C.plot(xlabel   ="n",
                            ylabel   ="Ratio")

                     plt.savefig(self.folder + "/non_unit_supply_ratio_under_timelimit_algorithm" + self.ext)

                     plt.close()

              def ratio_under_timelimit_every_combo(self):

                     non_ksb = self.non_unit_results.loc[self.non_unit_results["KSB_no_sols"].isin([0])]

                     best_gamma_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["GAMMA"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["Algorithm"].isin(["KSBnB"])]
                     best_gamma_delta = best_gamma_delta.loc[best_gamma_delta["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["GAMMA"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_gamma_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["GAMMA"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_gamma_delta_plus = best_gamma_delta_plus.loc[best_gamma_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_DMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_DMC_delta = best_ybar_DMC_delta.loc[best_ybar_DMC_delta["KSB_no_sols"].isin([4])]

                     best_ybar_DMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["McCormick_Type"].isin(["DMC"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_DMC_delta_plus = best_ybar_DMC_delta_plus.loc[best_ybar_DMC_delta_plus["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["Algorithm"].isin(["KSBnB"])]
                     best_ybar_SMC_delta = best_ybar_SMC_delta.loc[best_ybar_SMC_delta["KSB_no_sols"].isin([2])]

                     best_ybar_SMC_delta_plus = self.non_unit_results.loc[self.non_unit_results["Type"].isin(["YBAR"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["McCormick_Type"].isin(["SMC"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["Algorithm"].isin(["KSBnB+"])]
                     best_ybar_SMC_delta_plus = best_ybar_SMC_delta_plus.loc[best_ybar_SMC_delta_plus["KSB_no_sols"].isin([1])]


                     dataframe_lst = [non_ksb,best_gamma_delta,best_gamma_delta_plus,
                                          best_ybar_DMC_delta,best_ybar_DMC_delta_plus,
                                          best_ybar_SMC_delta,best_ybar_SMC_delta_plus]

                     df = pd.concat(dataframe_lst)

                     subplots = [("DI","SMC"),("DI","DMC"),("MV","NAP")]#("LA","NAP"),

                     for subplot in subplots:

                            sub_data = df.loc[df["Type"] == subplot[0]]
                            sub_data = sub_data.loc[sub_data["McCormick_Type"] == subplot[1]]

                            # Method problems solved within a given time
                            A = pd.pivot_table(sub_data,
                                                 values='Supply_max',
                                                 index=['Cropped_Time'],
                                                 columns=["Algorithm"],
                                                 aggfunc=np.count_nonzero,
                                                 fill_value=0).cumsum()

                            total = A.iloc[-1]

                            B = A/total

                            B.to_csv("ratio",sep=";")

                            B.plot(xlabel   ="Time",
                                   ylabel   ="Ratio",
                                   title    = "(" + subplot[0] + ", " + subplot[1] + ")",
                                   xlim     =[0,3600],
                                   ylim     =[0.5,1],
                                   markevery=500,
                                   style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                 'g.-','g,-',
                                                 'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                                                 'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                            plt.savefig(self.folder + "/non_unit_supply_ratio_solved_in_time_every_combo_" + subplot[0] + "_" + subplot[1] + self.ext)

                            plt.close()

                     sub_data = df.loc[df["Type"].isin(["DMKKT","DMVF"])]

                     # Method problems solved within a given time
                     A = pd.pivot_table(sub_data,
                                          values='Supply_max',
                                          index=['Cropped_Time'],
                                          columns=["Type"],
                                          aggfunc=np.count_nonzero,
                                          fill_value=0).cumsum()

                     total = A.iloc[-1]

                     B = A/total

                     B.to_csv("ratio",sep=";")

                     B.plot(xlabel   ="Time",
                            ylabel   ="Ratio",
                            title    = "(DMKKT, NAP) and (DMVF, NAP)",
                            xlim     =[0,3600],
                            ylim     =[0.5,1],
                            markevery=50,
                            style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                          'g.-','g,-',
                                          'y.-', 'y,-', 'y+-','y*-', 'yo-','yx-',
                                          'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_ratio_solved_in_time_every_combo_DM_NAP" + self.ext)

                     plt.close()


              def ratio_under_timelimit_type(self):

                     pd.pivot_table(self.non_unit_results[self.non_unit_results["Time Limit"] == "UNDER"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type'],
                                          aggfunc=np.count_nonzero)

                     A = pd.pivot_table(self.non_unit_results[self.non_unit_results["Time Limit"] == "UNDER"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type'],
                                          aggfunc=np.count_nonzero)

                     B = pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['Type'],
                                          aggfunc=np.count_nonzero)

                     C = A/B
                     C.plot(xlabel   ="n",
                            ylabel   ="Ratio")

                     plt.savefig(self.folder + "/non_unit_supply_ratio_under_timelimit_type" + self.ext)

                     plt.close()

              def ratio_under_timelimit_mccormick(self):

                     pd.pivot_table(self.non_unit_results[self.non_unit_results["Time Limit"] == "UNDER"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.count_nonzero)

                     A = pd.pivot_table(self.non_unit_results[self.non_unit_results["Time Limit"] == "UNDER"],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.count_nonzero)

                     B = pd.pivot_table(self.non_unit_results,
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.count_nonzero)

                     C = A/B
                     C.plot(xlabel   ="n",
                            ylabel   ="Ratio")

                     plt.savefig(self.folder + "/non_unit_supply_ratio_under_timelimit_mccormick" + self.ext)

                     plt.close()

              def ratio_solved_in_given_time_algorithm(self):

                     # Method problems solved within a given time
                     A = pd.pivot_table(self.non_unit_results,
                                          values='Supply_max',
                                          index=['Cropped_Time'],
                                          columns=['Algorithm'],
                                          aggfunc=np.count_nonzero,
                                          fill_value=0).cumsum()

                     total = A.iloc[-1]

                     B = A/total

                     B.plot(xlabel   ="Time",
                            ylabel   ="Ratio",
                            xlim     =[0,3600],
                            ylim     =[0.5,1])

                     plt.savefig(self.folder + "/non_unit_supply_ratio_solved_in_time_algorithm" + self.ext)

                     plt.close()

              def ratio_solved_in_given_time_type(self):

                     # Method problems solved within a given time
                     A = pd.pivot_table(self.non_unit_results,
                                          values='Supply_max',
                                          index=['Cropped_Time'],
                                          columns=['Type'],
                                          aggfunc=np.count_nonzero,
                                          fill_value=0).cumsum()

                     total = A.iloc[-1]

                     B = A/total

                     B.plot(xlabel   ="Time",
                            ylabel   ="Ratio",
                            xlim     =[0,3600],
                            ylim     =[0.5,1])

                     plt.savefig(self.folder + "/non_unit_supply_ratio_solved_in_time_type" + self.ext)

                     plt.close()

              def ratio_solved_in_given_time_mccormick(self):

                     # Method problems solved within a given time
                     A = pd.pivot_table(self.non_unit_results,
                                          values='Supply_max',
                                          index=['Cropped_Time'],
                                          columns=['McCormick_Type'],
                                          aggfunc=np.count_nonzero,
                                          fill_value=0).cumsum()

                     total = A.iloc[-1]

                     B = A/total

                     B.plot(xlabel   ="Time",
                            ylabel   ="Ratio",
                            xlim     =[0,3600],
                            ylim     =[0.5,1])

                     plt.savefig(self.folder + "/non_unit_supply_ratio_solved_in_time_mccormick" + self.ext)

                     plt.close()

              def n_vs_time_every_combo(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='Time',
                                          index=['n'],
                                          columns=["Type","McCormick_Type",'Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Cropped_Time",
                                                               style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                                             'y.-', 'y,-', 'y+-','y*-', 'yo-',
                                                                             'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_every_combo" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_nodes_every_combo(self):

                     pd.pivot_table(self.non_unit_results,
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=["Type","McCormick_Type",'Algorithm'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes",
                                                               style    = ['b.-', 'b,-', 'b+-','b*-', 'bo-', 'bx-',
                                                                             'y.-', 'y,-', 'y+-','y*-', 'yo-',
                                                                             'r.-', 'r,-', 'r+-','r*-', 'ro-', 'rx-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_every_combo" + self.ext,
                                   bbox_inches='tight')

                     plt.close()

              def n_vs_time_gamma_ksb_delta(self):

                     ksb_results1 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["MV"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_gamma_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_time_gamma_ksb_delta_plus(self):

                     ksb_results1 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["MV"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_gamma_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_time_ybar_DMC_ksb_delta(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["DMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ybar_DMC_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_time_ybar_DMC_ksb_delta_plus(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["DMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ybar_DMC_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_time_ybar_SMC_ksb_delta(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["SMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ybar_SMC_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_time_ybar_SMC_ksb_delta_plus(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["SMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='Cropped_Time',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Time")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_time_ybar_SMC_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_nodes_gamma_ksb_delta(self):

                     ksb_results1 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["MV"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_gamma_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_nodes_gamma_ksb_delta_plus(self):

                     ksb_results1 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["MV"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_gamma_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_nodes_ybar_DMC_ksb_delta(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["DMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ybar_DMC_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_nodes_ybar_DMC_ksb_delta_plus(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["DMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ybar_DMC_ksb_delta_plus" + self.ext)

                     plt.close()

              def n_vs_nodes_ybar_SMC_ksb_delta(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["SMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ybar_SMC_ksb_delta" + self.ext)

                     plt.close()

              def n_vs_nodes_ybar_SMC_ksb_delta_plus(self):

                     ksb_results2 = self.non_unit_results.loc[self.non_unit_results["Algorithm"].isin(["KSBnB+"])]

                     ksb_results1 = ksb_results2.loc[ksb_results2["McCormick_Type"].isin(["SMC"])]

                     ksb_results = ksb_results1.loc[ksb_results1["Type"].isin(["DI"])]

                     pd.pivot_table(ksb_results.loc[ksb_results["KSB_no_sols"].isin([1,2,4,8])],
                                          values='No_Nodes',
                                          index=['n'],
                                          columns=['KSB_no_sols'],
                                          aggfunc=np.mean).plot(xlabel   ="n",
                                                               ylabel   ="Nodes")

                     plt.savefig(self.folder + "/non_unit_supply_n_vs_nodes_ybar_SMC_ksb_delta_plus" + self.ext)

                     plt.close()

class setB_plots:

       def __init__(self):

              self.results = pd.read_csv("../IRIDIS_results/setB_results.csv",sep=";")

              self.folder = "../Latex_documents/images/Results_plots/Set B"
              self.ext    = ".png"

              self.plot_width  = 5
              self.plot_height = 5

              # remove 0.25 results
              self.results = self.results.loc[self.results["Supply_coef"].isin([0.5,0.75,1,1.25,1.5,1.75])]

              # Plots
              self.supply_coef_vs_time()
              self.supply_coef_vs_nodes()
              self.supply_coef_vs_time_each_d()
              self.supply_coef_vs_nodes_each_d()

              self.supply_coef_vs_d_heatmap()

              self.d_vs_time()
              self.d_vs_nodes()
              self.d_vs_time_each_supply_coef()
              self.d_vs_nodes_each_supply_coef()

              self.ratio_under_given_time()

       def supply_coef_vs_d_heatmap(self):

              MC = ["SMC","DMC"]
              AL = ["SFCP","NSFCP"]

              

              for mc in MC:
                     for al in AL:

                            res = self.results.loc[self.results["Algor"] == al]
                            res = res.loc[res["McCormick_Type"] == mc]

                            A = pd.pivot_table(res,
                                   values = "Cropped_Time",
                                   index  = ['Supply_coef'],
                                   columns= "d_sum")

                            hm = sns.heatmap(A.round(decimals=0),cmap="YlGnBu",vmin=0,vmax=3600)

                            hm.invert_yaxis()

                            plt.xlabel("d")
                            plt.ylabel(r'$s_{coef}$')

                            plt.savefig(self.folder + "/supply_coef_vs_d_heatmap_" + str(al) + "_" + str(mc) + self.ext,
                                          bbox_inches='tight')

                            plt.close()

       def supply_coef_vs_time(self):

              pd.pivot_table(self.results,
                                   values='Cropped_Time',
                                   index=['Supply_coef'],
                                   columns=['Algor',"McCormick_Type",],
                                   aggfunc=np.mean).plot(xlabel   =r'$s_{coef}$',
                                                        ylabel   ="Time",
                                                        style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

              plt.savefig(self.folder + "/supply_coef_vs_time" + self.ext,
                            bbox_inches='tight')

              plt.close()

       def supply_coef_vs_nodes(self):

              pd.pivot_table(self.results,
                                   values='No_Nodes',
                                   index=['Supply_coef'],
                                   columns=['Algor',"McCormick_Type",],
                                   aggfunc=np.mean).plot(xlabel   =r'$s_{coef}$',
                                                        ylabel   ="Nodes",
                                                        style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

              plt.savefig(self.folder + "/supply_coef_vs_nodes" + self.ext,
                            bbox_inches='tight')

              plt.close()

       def supply_coef_vs_time_each_d(self):

              for d in range(5,55,5):

                     pd.pivot_table(self.results.loc[self.results["d_sum"] == d],
                                          values='Cropped_Time',
                                          index=['Supply_coef'],
                                          columns=['Algor',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="Supply_coef",
                                                               ylabel   ="Time",
                                                        style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/supply_coef_vs_time_" + str(d) + self.ext,
                                   bbox_inches='tight')

                     plt.close()

       def supply_coef_vs_nodes_each_d(self):

              for d in range(5,55,5):

                     pd.pivot_table(self.results.loc[self.results["d_sum"] == d],
                                          values='No_Nodes',
                                          index=['Supply_coef'],
                                          columns=['Algor',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="Supply_coef",
                                                               ylabel   ="Nodes",
                                                        style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/supply_coef_vs_nodes_" + str(d) + self.ext,
                                   bbox_inches='tight')

                     plt.close()

       def d_vs_time(self):

              pd.pivot_table(self.results,
                                   values='Cropped_Time',
                                   index=['d_sum'],
                                   columns=['Algor',"McCormick_Type",],
                                   aggfunc=np.mean).plot(xlabel   ="d_sum",
                                                        ylabel   ="Time",
                                                        style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

              plt.savefig(self.folder + "/d_sum_vs_time" + self.ext,
                            bbox_inches='tight')

              plt.close()

       def d_vs_nodes(self):

              pd.pivot_table(self.results,
                                   values='No_Nodes',
                                   index=['d_sum'],
                                   columns=['Algor',"McCormick_Type",],
                                   aggfunc=np.mean).plot(xlabel   ="d_sum",
                                                        ylabel   ="Nodes",
                                                        style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

              plt.savefig(self.folder + "/d_sum_vs_nodes" + self.ext,
                            bbox_inches='tight')

              plt.close()

       def d_vs_time_each_supply_coef(self):

              for supply_coef in [0.5,0.75,1,1.25,1.5,1.75]:

                     pd.pivot_table(self.results.loc[self.results["Supply_coef"] == supply_coef],
                                          values='Cropped_Time',
                                          index=['d_sum'],
                                          columns=['Algor',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="d_sum",
                                                               ylabel   ="Time",
                                                               style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/d_sum_vs_time_" + str(supply_coef) + self.ext,
                                   bbox_inches='tight')

                     plt.close()

       def d_vs_nodes_each_supply_coef(self):

              for supply_coef in [0.5,0.75,1,1.25,1.5,1.75]:

                     pd.pivot_table(self.results.loc[self.results["Supply_coef"] == supply_coef],
                                          values='No_Nodes',
                                          index=['d_sum'],
                                          columns=['Algor',"McCormick_Type",],
                                          aggfunc=np.mean).plot(xlabel   ="d_sum",
                                                               ylabel   ="Nodes",
                                                               style    = ['b--','r--','b.-','r.-']).legend(bbox_to_anchor=(1.0, 1.0))

                     plt.savefig(self.folder + "/d_sum_vs_nodes_" + str(supply_coef) + self.ext,
                                   bbox_inches='tight')

                     plt.close()

       def ratio_under_given_time(self):

              # Method problems solved within a given time
              A = pd.pivot_table(self.results,
                                   values='Supply_coef',
                                   index=['Time'],
                                   columns=['Algor','McCormick_Type'],
                                   aggfunc=np.count_nonzero,
                                   fill_value=0).cumsum()

              total = A.iloc[-1]

              B = A/total

              B.plot(xlabel   ="Time",
                     ylabel   ="Ratio",
                     xlim     =[0,3600],
                     ylim     =[0,1],
                     style    = ['b--','r--','b.-','r.-'],
                     markevery=100)

              plt.savefig(self.folder + "/ratio_under_given_time" + self.ext)

              plt.close()





setA_plots()
setB_plots()