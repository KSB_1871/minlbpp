import matplotlib.pyplot as plt
import numpy as np

m = np.linspace(1,101,26)
m_func = lambda m: 2**(np.floor(np.log2(m+1))) - 1
m_crit = m_func(m)

print(m)
print(m_crit)

plt.plot(m,m_crit/m)
plt.show()