import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.ticker import MaxNLocator

"""
Moore and Bard Plot

min     -x - 10y

    min     y
            -25x+20y <= 30
            x + 2y <= 10
            2x - y <= 15
            2x + 10y >= 15
"""
# plt.style.use('seaborn-whitegrid')

# fig = plt.figure()
# ax = plt.axes()

# """Axis stuff"""
# plt.axis('equal')

# ax.set(xlim=(-0.5, 8.5), ylim=(1, 3),
#        xlabel='x', ylabel='y',
#        )

# """Upper part"""
# ax.plot([0,2,8],[1.5,4,1])

# """Lower Part"""
# ax.plot([8,7.5,0],[1,0,1.5],linestyle='dashed')

# # """IR points"""
# # ax.plot([1],[2],marker="^",color="green")
# # ax.plot([2],[2],marker="^",color="green")
# # ax.plot([3],[1],marker="^",color="green")
# # ax.plot([4],[1],marker="^",color="green")
# # ax.plot([5],[1],marker="^",color="green")
# # ax.plot([6],[1],marker="^",color="green")
# # ax.plot([7],[1],marker="^",color="green")
# # ax.plot([8],[1],marker="^",color="green")

# # """HPR Opt Solution"""
# # ax.plot([2],[4],marker="o",color="green")

# # """HPR Feasible solutions"""
# # ax.plot([2],[3],marker="o",color="black")
# # ax.plot([3],[3],marker="o",color="black")
# # ax.plot([3],[2],marker="o",color="black")
# # ax.plot([4],[3],marker="o",color="black")
# # ax.plot([4],[2],marker="o",color="black")
# # ax.plot([5],[2],marker="o",color="black")
# # ax.plot([6],[2],marker="o",color="black")

# plt.savefig('bilevel_optimisation_example_non_int.png')
# plt.show()

"""
optimistic and pessemistic
"""

# fig = plt.figure()
# ax = plt.axes()

# """Axis stuff"""
# plt.axis('equal')

# ax.set(xlim=(-1, 1), ylim=(-1, 1),
#        xlabel='x', ylabel='y',
#        )

# ax.plot([-1,1],[-1,1])

# plt.show()

"""
mirrlees problem
"""
# fig = plt.figure()
# ax = plt.axes()

# """Axis stuff"""
# plt.axis('equal')

# ax.set(xlim=(-1.5, 1.5), ylim=(0, 3),
#        xlabel='y', ylabel='x',
#        )

# y = np.linspace(-0.999,0.999,100)
# x = ((1-y)/(1+y))*np.exp(4*y)

# ax.plot(y,x)

# plt.show()

"""Variables and constraints"""

# fig, (ax_var,ax_con) = plt.subplots(2,1)

"""Axis stuff"""
# plt.axis('equal')

# dmc_vars = lambda d: n*np.sum([d])
# dmc_cons = lambda d: 4*n*np.sum([d])
# bmc_vars = lambda s: 2*np.sum([np.floor(np.log2(s)) + 1 for j in range(n)])
# bmc_cons = lambda s: n + 4*np.sum([np.floor(np.log2(s)) + 1 for j in range(n)])

# n = 1000
# d = 5
# s = 5


# print(dmc_vars(d))
# print(dmc_cons(d))
# print(bmc_vars(s))
# print(bmc_cons(s))

# dmc_vars_vals = []

# my_range = 20

# # Variables

# ax_var.plot([d for d in range(1,my_range)],[dmc_vars(d) for d in range(1,my_range)],label='Direct McCormick')
# ax_var.plot([d for d in range(1,my_range)],[bmc_vars(d) for d in range(1,my_range)],label='Binary Expansion McCormick')
# ax_var.legend()

# ax_var.set_xlabel('$s$ or $d$')
# ax_var.set_ylabel('# Variables')
# ax_var.xaxis.set_major_locator(MaxNLocator(integer=True))

# # Constraints

# ax_con.plot([d for d in range(1,my_range)],[dmc_cons(d) for d in range(1,my_range)])
# ax_con.plot([d for d in range(1,my_range)],[bmc_cons(d) for d in range(1,my_range)])

# ax_con.set_xlabel('$s$ or $d$')
# ax_con.set_ylabel('# Constraints')
# ax_con.xaxis.set_major_locator(MaxNLocator(integer=True))


# plt.show()

n = 1000
d = 5
s = 5
my_range = 20

dmc_vars = lambda d: 2*n*np.sum([d])
bmc_vars = lambda d,s: n*np.sum([d]) + 2*np.sum([np.floor(np.log2(s)) + 1 for j in range(n)])
gam_vars = lambda s: 2*n + 2*np.sum([np.floor(np.log2(s)) + 1 for j in range(n)])

dmc_cons = lambda d: 3*n + 5*n*np.sum([d])
bmc_cons = lambda d,s: 4*n + n*np.sum([d]) + 4*np.sum([np.floor(np.log2(s)) + 1 for j in range(n)])
gam_cons = lambda s: 8*n + 4*np.sum([np.floor(np.log2(s)) + 1 for j in range(n)])

x, y = np.linspace(1, my_range, my_range), np.linspace(1, my_range, my_range)
xx, yy = np.meshgrid(x, y, sparse=True)

z_dmc_vars = np.zeros((my_range,my_range))
z_bmc_vars = np.zeros((my_range,my_range))
z_gam_vars = np.zeros((my_range,my_range))

z_dmc_cons = np.zeros((my_range,my_range))
z_bmc_cons = np.zeros((my_range,my_range))
z_gam_cons = np.zeros((my_range,my_range))


for ddx in range(1,my_range):
    for sdx in range(1,my_range):

        z_dmc_vars[ddx-1,sdx-1] = dmc_vars(ddx)
        z_bmc_vars[ddx-1,sdx-1] = bmc_vars(ddx,sdx)
        z_gam_vars[ddx-1,sdx-1] = gam_vars(sdx)

        z_dmc_cons[ddx-1,sdx-1] = dmc_cons(ddx)
        z_bmc_cons[ddx-1,sdx-1] = bmc_cons(ddx,sdx)
        z_gam_cons[ddx-1,sdx-1] = gam_cons(sdx)

print(z_dmc_vars)

z_dmc_vars = z_dmc_vars[:-1, :-1]

z_var_min, z_var_max = min([z_dmc_vars.min(),z_bmc_vars.min(),z_gam_vars.min()]), \
                max([z_dmc_vars.max(),z_bmc_vars.max(),z_gam_vars.max()])

z_con_min, z_con_max = min([z_dmc_cons.min(),z_bmc_cons.min(),z_gam_cons.min()]), \
                       max([z_dmc_cons.max(),z_bmc_cons.max(),z_gam_cons.max()])

from matplotlib.transforms import offset_copy


cols = ["Direct", "Binary", "Gamma"]
rows = ["Variables","Constraints"]

fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(12, 8))
plt.setp(axes.flat, xlabel='s', ylabel='d')

pad = 5 # in points

for ax, col in zip(axes[0], cols):
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

for ax, row in zip(axes[:,0], rows):
    ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                xycoords=ax.yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center')

fig.tight_layout()
# tight_layout doesn't take these labels into account. We'll need 
# to make some room. These numbers are are manually tweaked. 
# You could automatically calculate them, but it's a pain.
fig.subplots_adjust(left=0.15, top=0.95)

# Setup figure
for idx in range(2):
    for jdx in range(3):
#         ax[idx][jdx].set_aspect('equal', adjustable='box')
        axes[idx][jdx].set_yticks([5,10,15,20])
#         # ax[idx][jdx].set_xlabel("s")
#         # ax[idx][jdx].set_ylabel("d")

# ax[0][0].set_title("Direct",xy=(0.5,1))
# ax[0][1].set_title("Binary Expansion")
# ax[0][2].set_title("$\gamma$")

# # ax[0][0].annotate("HI",xy=(0,0.5))

# Variables
c1 = axes[0][0].pcolormesh(xx, yy, z_dmc_vars, cmap='RdBu', vmin=z_var_min, vmax=z_var_max)
c1 = axes[0][1].pcolormesh(xx, yy, z_bmc_vars, cmap='RdBu', vmin=z_var_min, vmax=z_var_max)
c1 = axes[0][2].pcolormesh(xx, yy, z_gam_vars, cmap='RdBu', vmin=z_var_min, vmax=z_var_max)

# Constraints
c2 = axes[1][0].pcolormesh(xx, yy, z_dmc_cons, cmap='RdBu', vmin=z_con_min, vmax=z_con_max)
c2 = axes[1][1].pcolormesh(xx, yy, z_bmc_cons, cmap='RdBu', vmin=z_con_min, vmax=z_con_max)
c2 = axes[1][2].pcolormesh(xx, yy, z_gam_cons, cmap='RdBu', vmin=z_con_min, vmax=z_con_max)



axes[0][0].axis([1,my_range,1,my_range])

fig.subplots_adjust(right=0.8)
cbar_ax1 = fig.add_axes([0.85, 0.6, 0.05, 0.3])
fig.colorbar(c1, cax=cbar_ax1)

cbar_ax2 = fig.add_axes([0.85, 0.125, 0.05, 0.3])
fig.colorbar(c2, cax=cbar_ax2)

plt.show()