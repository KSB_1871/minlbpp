"""
Master class
"""
import cplex
import json
import numpy as np
import pandas as pd

class master():

    def __init__(self,input_file):

        print("\n")
        print("Mixed Integer Bilevel Non-Linear Problem Optimisation")
        print("\n")

        self.prob = cplex.Cplex()
        self.prob.objective.set_sense(self.prob.objective.sense.maximize)
        self.prob.parameters.timelimit.set(3600.0)

        # Read JSON
        self.read_json(input_file)

        # Fix seed
        np.random.seed(self.seed)

        # Generate random constants
        self.random_constants()

    # Functions

    def read_json(self,input_file):

        print("Reading Input File:",input_file)

        with open(input_file) as json_file:
            data = json.load(json_file)
            for key in data.keys():
                setattr(self,key,data[key])

    def random_constants(self):

        # Supply
        if self.single_supply == True:
            self.supply = np.ones((self.n))
        else:
            if hasattr(self,"supply_max"):
                self.supply = np.random.randint(low=1,high=self.supply_max+1,size=self.n)
            elif hasattr(self,"supply_equal"):
                self.supply = np.array([int(self.supply_equal) for j in range(self.n)])  
            elif hasattr(self,"supply_coef"):
                bnd = 2**(np.floor(np.log2(self.d+1)))-1
                s = np.floor(self.supply_coef * bnd)
                self.supply = np.array([int(s) for j in range(self.n)])          
            else:
                self.supply = np.random.randint(low=1,high=self.n,size=self.n)

        # C
        self.c = [float(np.random.randint(low=1,high=self.n)) for j in range(self.n)]

        # d
        self.d = [self.d for i in range(self.i)]

        # A matrix
        self.A = [np.zeros((0,self.n)) for i in range(self.i)]
        self.b = [np.zeros((0,1)) for i in range(self.i)]

        # ksb
        self.ksb_sols = []
        self.ksb_sols_index = []

        self.k_card_constraints()
        self.knapsack_constraints()
        self.partition_matroid_constraints()

        for i in range(self.i):
            self.A[i] = self.A[i].astype(float)
            self.b[i] = self.b[i].astype(float)

    def k_card_constraints(self):

        for i in range(self.i):
            self.A[i] = np.append(self.A[i],np.ones((1,self.n)),axis=0)
            self.b[i] = np.append(self.b[i],np.array([[self.k_card_rhs]]))

    def knapsack_constraints(self):

        for i in range(self.i):
            self.A[i] = np.append(self.A[i],np.array([self.c]),axis=0)
            self.b[i] = np.append(self.b[i],np.array([[self.knapsack_rhs]]))

    def partition_matroid_constraints(self):

        primes = []
        number = 1000
        for num in range(2,number + 1):
            if len(primes) == self.partition_matroid_no:
                break

            if num > 1:
                for i in range(2,num):
                    if (num % i) == 0:
                        break
                else:
                    primes += [num]

        if len(primes) > 0:
            for i in range(self.i):
                self.A[i] = np.append(self.A[i],np.array([[1 if j%prime == 0 else 0 for j in range(self.n)] for prime in primes]),axis=0)
                self.b[i] = np.append(self.b[i],np.ones((self.partition_matroid_no))*self.partition_matroid_rhs)

    def save_results(self,results):

        # Instance_descr        
        if hasattr(self,"supply_max"):

            inst_descr = [["seed",            self.seed],
                            ["single_supply",   self.single_supply],
                            ["Method",          self.solution_method],
                            ["KSB_no_sols",     self.ksb_no_sols],
                            ["n",               self.n],
                            ["i",               self.i],
                            ["d_sum",           np.sum(self.d)],
                            ["k_card_rhs",      self.k_card_rhs],
                            ["knapsack_rhs",    self.knapsack_rhs],
                            ["par_mat_no",      self.partition_matroid_no],
                            ["par_mat_rhs",     self.partition_matroid_rhs],
                            ["M",               self.M],
                            ["B",               self.B],
                            ["Supply_max",      self.supply_max]]

        elif hasattr(self,"supply_equal"):

            inst_descr = [["seed",            self.seed],
                            ["single_supply",   self.single_supply],
                            ["Method",          self.solution_method],
                            ["KSB_no_sols",     self.ksb_no_sols],
                            ["n",               self.n],
                            ["i",               self.i],
                            ["d_sum",           np.sum(self.d)],
                            ["k_card_rhs",      self.k_card_rhs],
                            ["knapsack_rhs",    self.knapsack_rhs],
                            ["par_mat_no",      self.partition_matroid_no],
                            ["par_mat_rhs",     self.partition_matroid_rhs],
                            ["M",               self.M],
                            ["B",               self.B],
                            ["Supply_equal",    self.supply_equal]]

        elif hasattr(self,"supply_coef"):

            inst_descr = [["seed",            self.seed],
                            ["single_supply",   self.single_supply],
                            ["Method",          self.solution_method],
                            ["KSB_no_sols",     self.ksb_no_sols],
                            ["n",               self.n],
                            ["i",               self.i],
                            ["d_sum",           np.sum(self.d)],
                            ["k_card_rhs",      self.k_card_rhs],
                            ["knapsack_rhs",    self.knapsack_rhs],
                            ["par_mat_no",      self.partition_matroid_no],
                            ["par_mat_rhs",     self.partition_matroid_rhs],
                            ["M",               self.M],
                            ["B",               self.B],
                            ["Supply_coef",     self.supply_coef]]


        time_result = [["Time",     self.end_time - self.start_time]]

        ksb_opt = -1

        """Status Codes
        101     Optimal
        102     Tolerance optimal
        103     Infeasible
        104     Solution limit
        105     Node Limit Feasible
        106     Node limit infeasible
        107     Time limit feasible
        108     Time limit infeasible
        109     Error but solution exists
        110     Error and no solution exists
        111     Memory limit but feasible
        112     Memory limit and infeasible
        113     Aborted but integer solution exists
        114     Aborted no integer solution exists
        115     Problem optimal with unscaled infeasibilities
        116     Out of memory, no tree, integer sol exists
        117     Out of memory, no tree, no int sol
        118     Problem unbounded
        119     Problem infeasible or unbounded
        120     


        """
        print("\n")
        print("RESULTS")
        # print(inst_descr + results)

        results_df = pd.DataFrame(inst_descr + results + time_result,).T

        new_header = results_df.iloc[0] 

        results_df = results_df[1:] 

        results_df.columns = new_header

        results_name = "results/"
        for i in range(len(inst_descr)-1):
            results_name += str(inst_descr[i][1]) + "_"

        results_name += str(inst_descr[-1][1]) + ".txt"

        results_df.to_csv(results_name,sep=";")

        print("RESULTS NAME:",results_name)
        print("RESULTS DATA:")
        print(results_df)

        # if self.prob.solution.get_status() == 101:

        #     ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

        #     if self.ksb_method == True:
        #         print("\n")
        #         print("YSUM   = ",[j for j in range(self.n) if ysum[j] > 0.5])
        #         for i in range(len(self.branch_callback.ksb_sols)):
        #             self.branch_callback.ksb_sols_index[i].sort()
        #             print("Y ",i,"  = ",self.branch_callback.ksb_sols_index[i])

        #         if self.branch_callback.branch_node_count == 0:
        #             ksb_opt = -2

        #         if [j for j in range(self.n) if ysum[j] > 0.5] in self.branch_callback.ksb_sols_index:
        #             ksb_opt = self.branch_callback.ksb_sols_index.index([j for j in range(self.n) if ysum[j] > 0.5])

        # results_data = [["seed",            self.seed],
        #                 ["single_supply",   self.single_supply],
        #                 ["method",          self.solution_method],
        #                 ["n",               self.n],
        #                 ["i",               self.i],
        #                 ["d_sum",           np.sum(self.d)],
        #                 ["k_card_rhs",      self.k_card_rhs],
        #                 ["knapsack_rhs",    self.knapsack_rhs],
        #                 ["par_mat_no",      self.partition_matroid_no],
        #                 ["par_mat_rhs",     self.partition_matroid_rhs],
        #                 ["No_nodes",        self.node_callback.node_count],
        #                 ["No_branch_nodes", self.branch_callback.branch_node_count],
        #                 ["No_lazy_calls",   self.lazy_callback.no_lazy_callbacks],
        #                 ["No_lazy_cuts",    self.lazy_callback.no_lazy_cuts],
        #                 ["Obj",             obj],
        #                 ["KSB_Sol",         ksb_opt],
        #                 ["Time",            np.round(self.end_time - self.start_time,decimals=4)],
        #                 ["KSB_no_sols",     self.ksb_no_sols],
        #                 ["Status",          self.prob.solution.get_status()]]

        # results_df = pd.DataFrame(results_data).T

        # print(results_df.T)

        # saved_name = str(self.seed) + "_" + \
        #              str(self.single_supply) + "_" + \
        #              str(self.ksb_no_sols) + "_" + \
        #              str(self.ksb_method) + "_" + \
        #              str(self.n) + "_" + \
        #              str(self.i) + "_" + \
        #              str(np.sum(self.d)) + "_" + \
        #              str(self.k_card_rhs) + "_" + \
        #              str(self.knapsack_rhs) + "_" + \
        #              str(self.partition_matroid_no) + "_" + \
        #              str(self.partition_matroid_rhs)

        # results_df.to_csv(saved_name + '.txt',sep=";",header=False,index=False) 
