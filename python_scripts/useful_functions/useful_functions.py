"""
Useful functions used in all problems
"""
import cplex

def mccormick_constraints(prob,x1,x2,x3):

    """
    prob is the cplex problem

    x1, x2 are tuples (name, lower, upper)
    x3 is name

    McCormick constraints for 

    x3 = x1*x2
    """

    prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[x1[0],x2[0],x3],
                                                                val=[x2[1],x1[1],-1])],
                                senses   = ["L"],
                                rhs      = [float(x1[1]*x2[1])])

    prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[x1[0],x2[0],x3],
                                                                val=[x2[2],x1[2],-1])],
                                senses   = ["L"],
                                rhs      = [float(x1[2]*x2[2])])

    prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[x1[0],x2[0],x3],
                                                                val=[-x2[1],-x1[2],1])],
                                senses   = ["L"],
                                rhs      = [-float(x1[2]*x2[1])])

    prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[x1[0],x2[0],x3],
                                                                val=[-x2[2],-x1[1],1])],
                                senses   = ["L"],
                                rhs      = [-float(x1[1]*x2[2])])

def binary_expansion_constraint(prob,x1,x2):

    """
    prob is the cplex problem

    x1 is a list of variable names
    x2 is the list of binary variable names

    x1 = \sum_{l=0}^{log_floor_value} 2**l x2[l]
    """

    prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=x1 + x2,
                                                             val=[-1]* len(x1) + [2**l for l in range(len(x2))])],
                                senses   = ["E"],
                                rhs      = [0])
