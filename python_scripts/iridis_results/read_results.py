import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("/home/ksb1871/Documents/KSB/PhD/IRIDIS_results/csv_results.csv",sep=";")


"""Total commodities with time"""
# with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
#     print(df.groupby(["Total_commods","Method"])["Time"].mean())

# df[df["Method"].isin([1,2,3,5,6,7,9,10,11])].groupby(["Total_commods","Method"])["Time"].mean().unstack().plot(kind="line")
# plt.show()

"""Over time limit objectives"""


# Geometric Times and Nodes

geomeans = pd.DataFrame()

products = df[df["Method"].isin([1,2,3,5,6,7,9,10,11])].groupby(["n","Method"])["Time","Geometric Nodes"].prod()
counts   = df[df["Method"].isin([1,2,3,5,6,7,9,10,11])].groupby(["n","Method"])["Time","Geometric Nodes"].count()
geomeans = counts

for i in range(len(counts)):
    for j in range(2):
        geomeans.iloc[i,j] = products.iloc[i,j]**(1/counts.iloc[i,j])

# with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
#     print(geomeans)

# geomeans["Time"].unstack().plot(kind = "line")
# plt.show()



# Under the Time limit
parameters = ["n","M","B","Supply_max"]
under_count_pre = ["" for i in range(len(parameters))]

for idx,param in enumerate(parameters):

    n_method_count = df.groupby([param,"Method"])["Time"].count()

    under_count_pre = df[df["Time Limit"].isin(["UNDER"])].groupby([param,"Method"])["Time"].count()
    under_count = under_count_pre.divide(n_method_count)

    under_count.unstack().plot(kind  = "line",
                            title = param)
    plt.show()

with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
    print(n_method_count)
    print(under_count_pre)
    print(under_count)