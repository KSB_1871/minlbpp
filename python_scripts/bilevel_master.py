"""
Bilevel master file
"""
import sys
import time
from master_class import master 

# from solution_methods.single_supply.cutting_plane.cutting_plane_method import cutting_plane_method 
from solution_methods.single_supply.cutting_plane.cutting_plane_method_2 import cutting_plane_method 
from solution_methods.single_supply.lambda_method.lambda_method import lambda_method

from solution_methods.multiple_supply.ybar.ybar_formulation import ybar
from solution_methods.multiple_supply.gamma.gamma_formulation import gamma
from solution_methods.multiple_supply.Lambda.lambda_formulation import lambda_method as ms_lambda_method

"""
Depending on the type of problem, we have various solution methods available to use:

Single Supply:

    1   Standard cutting plane approach
    2   Standard cutting plane with Known-Solution-Branching (KSB)
    3   Lambda Formulation with value function, followed by standard cutting plane approach
    4   Lambda Formulation with KKT, followed by standard 
    6   Lambda Formulation with value function, followed by standard cutting plane with Known-Solution-Branching (KSB)
    7   Lambda Formulation with KKT, followed by standard cutting plane with Known-Solution-Branching (KSB)

    1   Direct MC + cutting plane
    2   Direct MC + cutting plane with KSB
    3   Symmetric MC + cutting plane
    4   Symmetric MC + cutting plane with KSB
    5   Lambda Formulation with value function + best of 1-4
    6   Lambda Formulation with KKT + best of 1-4

Multiple Supply:

    TODO
    1   Ybar + direct MC + FCG
    2   Ybar + direct MC + DCG
    3   Ybar + direct MC + DB
    4   Ybar + direct MC + DB + KSB
    5   Ybar + (BE + MC) + FCG
    6   Ybar + (BE + MC) + DCG
    7   Ybar + (BE + MC) + DB
    8   Ybar + (BE + MC) + DB + KSB
    9   Gamma + FCG
    10  Gamma + DCG
    11  Gamma + DB
    12  Gamma + DB + KSB

    13  Ybar + direct MC + DB+
    14  Ybar + direct MC + DB+ + KSB
    15  Ybar + (BE + MC) + DB+
    16  Ybar + (BE + MC) + DB+ + KSB
    17  Gamma + DB+
    18  Gamma + DB+ + KSB

    19  Lambda KKT + Gamma DB+ 17
    20  Lambda VF + Gamma DB+ 17

"""

master = master(sys.argv[1])
master.start_time = time.time()

if master.single_supply == True:

    print("\n")
    print("Supply Type    : Single")

    if master.solution_method == 1:

        print("Solution Method: Direct McCormick Standard cutting plane approach")
        sol_method = cutting_plane_method(master)
        results = sol_method.solve(master)

    elif master.solution_method == 2:

        print("Solution Method: Direct McCormick Standard cutting plane approach with KSB")
        sol_method = cutting_plane_method(master)
        results = sol_method.solve(master)

    elif master.solution_method == 3:

        print("Solution Method: Symmetric McCormick Standard cutting plane approach")
        sol_method = cutting_plane_method(master)
        results = sol_method.solve(master)

    elif master.solution_method == 4:

        print("Solution Method: Symmetric McCormick Standard cutting plane approach with KSB")
        sol_method = cutting_plane_method(master)
        results = sol_method.solve(master)

    elif master.solution_method == 5:

        print("Solution Method: Lambda Formulation, with Value function and SMC CP")
        lambda_problem = lambda_method(master)
        results = lambda_problem.solve(master)

    elif master.solution_method == 6:

        print("Solution Method: Lambda Formulation, with KKT conditions and SMC CP")
        lambda_problem = lambda_method(master)
        results = lambda_problem.solve(master)

elif master.single_supply == False:

    print("\n")
    print("Multiple Supply")

    if master.solution_method == 1:
        
        print("Solution Method: YBar formulation, with direct Mccormick and fixed column generation")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 2:
        
        print("Solution Method: YBar formulation, with direct McCormick and dynamic column generation")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 3:

        print("Solution Method: YBar formulation, with direct Mccormick and delta branching")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 4:
        
        print("Solution Method: YBar formulation, with direct McCormick and delta branching with KSB")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 5:
        
        print("Solution Method: YBar formulation, with binary expansion then Mccormick and fixed column generation")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 6:
        
        print("Solution Method: YBar formulation, with binary expansion then Mccormick and dynamic column generation")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 7:

        print("Solution Method: YBar formulation, with binary expansion then Mccormick and delta branching")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 8:

        print("Solution Method: YBar formulation, with binary expansion then Mccormick and delta branching with KSB")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 9:

        print("Solution Method: Gamma formulation, with fixed column generation")
        gamma_problem = gamma(master)
        results = gamma_problem.solve(master)

    elif master.solution_method == 10:

        print("Solution Method: Gamma formulation, with dynamic column generation")
        gamma_problem = gamma(master)
        results = gamma_problem.solve(master)

    elif master.solution_method == 11:

        print("Solution Method: Gamma formulation, with delta branching")
        gamma_problem = gamma(master)
        results = gamma_problem.solve(master)

    elif master.solution_method == 12:

        print("Solution Method: Gamma formulation with delta branching and KSB")
        gamma_problem = gamma(master)
        results = gamma_problem.solve(master)

    elif master.solution_method == 13:

        print("Solution Method: YBar with direct McCormick and delta branching +")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 14:

        print("Solution Method: YBar formulation, with direct McCormick and delta branching+ with KSB")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 15:

        print("Solution Method: YBar formulation, with binary expansion then Mccormick and delta branching+")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 16:

        print("Solution Method: YBar formulation, with binary expansion then Mccormick and delta branching+ with KSB")
        ybar_problem = ybar(master)
        results = ybar_problem.solve(master)

    elif master.solution_method == 17:

        print("Solution Method: Gamma formulation, with delta branching+")
        gamma_problem = gamma(master)
        results = gamma_problem.solve(master)

    elif master.solution_method == 18:

        print("Solution Method: Gamma formulation with delta branching+ and KSB")
        gamma_problem = gamma(master)
        results = gamma_problem.solve(master)

    elif master.solution_method == 19:

        print("Solution Method: Lambda formulation with KKT")
        lambda_problem = ms_lambda_method(master)
        results = lambda_problem.solve(master)

    elif master.solution_method == 20:

        print("Solution Method: Lambda formulation with VF")
        lambda_problem = ms_lambda_method(master)
        results = lambda_problem.solve(master)


master.end_time = time.time()
master.save_results(results)