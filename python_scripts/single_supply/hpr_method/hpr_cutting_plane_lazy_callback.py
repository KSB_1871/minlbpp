"""
Single Supply HPR cutting plane

Lazy Callback
"""

from cplex.callbacks import  LazyConstraintCallback
from hpr_cutting_plane_followers_problem import FOL_PROB
import cplex
import numpy as np
import pandas as pd

# The class BendersLazyConsCallback 
# allows to add Benders' cuts as lazy constraints.
# 
class LazyConsCallback(LazyConstraintCallback):
        
    def __call__(self):    

        # print("Lazy Callback")

        # Add to number of callbacks
        # print("\n")
        self.no_lazy_callbacks += 1

        fol_prob = FOL_PROB(self.get_values(self.x_names),0)
        fol_prob.x_names = self.x_names
        fol_prob.xbar_names = self.xbar_names
        fol_prob.i = self.i
        fol_prob.d = self.d
        fol_prob.n = self.n
        fol_prob.c = self.c
        fol_prob.supply = self.supply
        fol_prob.A = self.A
        fol_prob.b = self.b

        fol_obj, fol_sol = fol_prob.solve_followers_problem()

        # print("Y",[[self.get_values(self.y_names[i][k]) for k in range(self.d[i])] for i in range(self.i)])
        # print("Y_sum",[np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)])

        curr_obj = np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)])*np.array([self.get_values(self.x_names[j]) + self.c[j] for j in range(self.n)]))

        # curr_obj2 = np.sum([self.get_values(self.z_names)]) + np.sum()

        # print("\n")
        # vstack = np.vstack((self.supply,
        #                     self.get_values(self.xbar_names),
        #                     self.get_values(self.x_names),
        #                     self.get_values(self.z_names),
        #                     self.c,
        #                     [np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
        #                     )).T
        
        # df = pd.DataFrame(vstack,columns=["S","XBAR","X","Z","C","YSUM"])
        # print("\n")
        # print(df)

        # print("CURR FOL OBJ = ",curr_obj)
        # print("OPTI FOL OBJ = ",fol_obj)

        if curr_obj >= fol_obj + 0.5:

            # print("ADD CUTTING PLANE")
            self.no_lazy_cuts += 1

            IND = [self.z_names[j] for j in range(self.n)]
            VAL = [1 for j in range(self.n)]

            IND += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.x_names[j] for j in range(self.n)]
            VAL += [-np.sum([fol_sol[i][k][j] for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

            RHS = np.sum([np.sum([fol_sol[i][k][j] for i in range(self.i) for k in range(self.d[i])])*self.c[j] for j in range(self.n)])

            # print(IND)
            # print([self.get_values(ind) for ind in IND])
            # print(VAL)
            # print(np.sum(np.array([self.get_values(ind) for ind in IND])*np.array(VAL)),RHS,np.sum(np.array([self.get_values(ind) for ind in IND])*np.array(VAL)) >= RHS)


            self.add(constraint=cplex.SparsePair(ind=IND,
                                                 val=VAL),
                     sense     ="L",
                     rhs       =RHS)

        # print("\n")
        # print("# Callbacks:",self.no_lazy_callbacks)
        # print("# Cuts     :",self.no_lazy_cuts)
