"""
Single Supply HPR cutting plane main
"""
import numpy as np
import time
import sys

from hpr_cutting_plane_leader import Prob

print("INPUT FILE:",sys.argv[1])

Prob = Prob()
Prob.read_json(sys.argv[1])
Prob.settings_after_json()

# Prob.seed = 0
# np.random.seed(Prob.seed)
# Prob.n = 80
# Prob.i = 2
# Prob.d = [2,2]
# Prob.A_constraint_type = 'knapsack' #[k_card,knapsack,]
# Prob.k_card_rhs = 5
# Prob.knapsack_rhs = 20
# Prob.A_coef_min = 0
# Prob.A_coef_max = 20
# Prob.b_coef_min = 3
# Prob.b_coef_max = 5
# Prob.c_min = 1
# Prob.c_max = 100

# Prob.c = [float(np.random.randint(low=Prob.c_min,high=Prob.c_max)) for j in range(Prob.n)]#[2,3,4,5,6]
# Prob.M = 10
# Prob.B = 100
# Prob.show_lazy_callback = 0
# Prob.solution_display = 2
# Prob.ksb_method = False#bool(sys.argv[1])
# Prob.ksb_no_sols = 10
# Prob.single_supply = True

# if Prob.single_supply == True:
#     Prob.supply = np.ones((Prob.n))

Prob.start_time = time.time()
Prob.solve_problem()