"""
Single Supply HPR cutting plane

KSB Solutions
"""
import cplex
import numpy as np
import pandas as pd
from hpr_cutting_plane_followers_problem import FOL_PROB


class KSB_solutions(FOL_PROB):

    def generate_solutions(self,no_sols=5):

        fol_prob = FOL_PROB([0]*self.n,0)
        fol_prob.x_names = self.x_names
        fol_prob.xbar_names = self.xbar_names
        fol_prob.i = self.i
        fol_prob.d = self.d
        fol_prob.n = self.n
        fol_prob.c = self.c
        fol_prob.supply = self.supply
        fol_prob.A = self.A
        fol_prob.b = self.b

        ksb_sols = fol_prob.ksb_solution_generation(no_sols)

        return ksb_sols

    def generate_solutions_on_fly(self):

        fol_prob = FOL_PROB([0]*self.n,0)
        fol_prob.x_names = self.x_names
        fol_prob.xbar_names = self.xbar_names
        fol_prob.i = self.i
        fol_prob.d = self.d
        fol_prob.n = self.n
        fol_prob.c = self.c
        fol_prob.supply = self.supply
        fol_prob.A = self.A
        fol_prob.b = self.b

        fol_prob.add_y_variables()
        fol_prob.group_constraints()
        fol_prob.fol_supply_constraints()
        fol_prob.fol_prob_settings()

        for idx,sol in enumerate(self.ksb_sols):

            fol_prob.fol_prob.linear_constraints.add(lin_expr= [cplex.SparsePair(ind=[fol_prob.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if j in self.ksb_sols_index[idx]],
                                                                                 val=[1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if j in self.ksb_sols_index[idx]])],
                                                     senses  = ["L"],
                                                     rhs     = [float(len(self.ksb_sols_index[idx])-1)])


        fol_prob.fol_prob.solve()

        IND = [fol_prob.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if fol_prob.fol_prob.solution.get_values(fol_prob.fol_y_names[i][k][j]) >= 0.5]
        IDX = [j for j in range(self.n) if np.sum([fol_prob.fol_prob.solution.get_values(fol_prob.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) >= 0.5]
        self.ksb_sols.append(IND)

        self.ksb_sols_index.append(IDX)
        # self.ksb_sols_index = [[int(self.y_df[self.y_df[0] == sol[i]].index[0])  for i in range(len(sol))] for sol in self.ksb_sols]
        # print("SOLS",self.ksb_sols)
        # print("IDX ",self.ksb_sols_index)
        # # print(fol_prob.fol_prob.solution.get_objective_value())

        # print("\n")
        # print("ksb_sols")
        # for i in range(len(self.ksb_sols)):
        #     print(self.ksb_sols)

        # if len(self.ksb_sols) == 3:
        #     print(hi)

        return IND,IDX




