"""
Single Supply HPR cutting plane

KSB Branch Callback
"""

from cplex.callbacks import  BranchCallback
from hpr_cutting_plane_followers_problem import FOL_PROB
import cplex as CPX
import numpy as np
import pandas as pd
from math import floor, fabs

# The class BendersLazyConsCallback 
# allows to add Benders' cuts as lazy constraints.
# 
class KSB_branch_callback(BranchCallback):
        
    def __call__(self):    

        if self.ksb_method == True:

            if self.get_node_data() != None:
                print("NODE DATA:",self.get_node_data())

            # print("branch",self.branch_count)
            # print("data  ",self.get_node_data()[-1])
            # print("Sols  ",)

            if self.branch_count == 0 or (self.sol_count <= self.ksb_no_sols and self.get_node_data()[-1] == "R" + str(self.sol_count-1)):

                # Generate the next follower solution
                IND, IDX = self.generate_solutions_on_fly()
                self.ksb_sols.append(IND)
                self.ksb_sols_index.append(IDX)

                self.make_branch(objective_estimate=self.get_objective_value(),
                                    constraints=[([IDX,[1]*len(IDX)],"E",len(IDX))],
                                    node_data=[([IDX,[1]*len(IDX)],"E",len(IDX))])

                self.make_branch(objective_estimate=self.get_objective_value(),
                                    constraints=[([IDX,[1]*len(IDX)],"L",len(IDX)-1)],
                                    node_data=[([IDX,[1]*len(IDX)],"L",len(IDX)-1),"R" + str(self.sol_count)])

                self.sol_count += 1

        self.branch_count += 1
        self.branch_node_count += self.get_num_branches()


            # if self.branch_count == 0:

            #     print(self.ksb_no_sols)
            #     print(self.ksb_sols)
                
            #     IND, IDX = self.generate_solutions_on_fly()
            #     # self.ksb_sols = self.generate_solutions(self.ksb_no_sols)
            #     # self.ksb_sols_index = [[int(self.y_df[self.y_df[0] == sol[i]].index[0])  for i in range(len(sol))] for sol in self.ksb_sols]
            #     print("INDEX",self.ksb_sols_index)

            #     self.make_branch(objective_estimate=self.get_objective_value(),
            #                         constraints=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"E",len(self.ksb_sols_index[self.sol_count]))],
            #                         node_data=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"E",len(self.ksb_sols_index[self.sol_count]))])
            #     print("HI")

            #     self.make_branch(objective_estimate=self.get_objective_value(),
            #                         constraints=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"L",len(self.ksb_sols_index[self.sol_count])-1)],
            #                         node_data=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"L",len(self.ksb_sols_index[self.sol_count])-1),"R" + str(self.sol_count)])

            #     self.sol_count += 1



            # elif self.sol_count < len(self.ksb_sols) and (self.get_node_data() != None and  self.get_node_data()[-1] == "R" + str(self.sol_count-1)):

            #     self.make_branch(objective_estimate=self.get_objective_value(),
            #                         constraints=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"E",len(self.ksb_sols_index[self.sol_count])-1)],
            #                         node_data=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"E",len(self.ksb_sols_index[self.sol_count])-1)])

            #     self.make_branch(objective_estimate=self.get_objective_value(),
            #                         constraints=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"L",len(self.ksb_sols_index[self.sol_count])-1)],
            #                         node_data=[([self.ksb_sols_index[self.sol_count],[1]*len(self.ksb_sols_index[self.sol_count])],"L",len(self.ksb_sols_index[self.sol_count])-1),"R" + str(self.sol_count)])

            #     self.sol_count += 1


