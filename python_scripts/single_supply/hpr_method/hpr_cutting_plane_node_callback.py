"""
Single Supply HPR cutting plane

Node Callback
"""
import cplex
from cplex.callbacks import  NodeCallback
from cplex.callbacks import  BranchCallback

# The class BendersLazyConsCallback 
# allows to add Benders' cuts as lazy constraints.
# 
class Node_callback(NodeCallback):

    def __call__(self):

        # print("NODE CALLBACK")

        # if self.node_count == 0:
        #     self.set_node_data(0,["ROOT"])

        # if self.get_node_data(0) != None:
        #     print(self.get_node_data(0))
            # print(self.get_values())

        #     print(self.get_objective_value(0))

        #     print(self.xbar_names[0])

        #     # self.make_branch(objective_estimate=self.get_objective_value(0),
        #     #                  constraints=[([[i for i in range(100)],[1.0 for i in range(100)]],"L",0.0)])

        #     # self.make_branch(objective_estimate=self.get_objective_value(0),
        #     #                  constraints=[([[i for i in range(100)],[1.0 for i in range(100)]],"L",1.0)])


        # print("****************",self.get_num_branches())
        self.node_count += 1
