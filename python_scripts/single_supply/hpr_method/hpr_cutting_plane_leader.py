"""
Single Supply HPR cutting plane

Leaders Class
"""

import cplex
import numpy as np
import pandas as pd
import time
import json

from hpr_cutting_plane_lazy_callback import LazyConsCallback
from hpr_cutting_plane_node_callback import Node_callback
from hpr_cutting_plane_KSB_branch_callback import KSB_branch_callback
from hpr_cutting_plane_ksb_solutions import KSB_solutions

class Prob(KSB_solutions):

    def __init__(self):

        print("Prob class")
        self.prob = cplex.Cplex()
        self.prob.objective.set_sense(self.prob.objective.sense.maximize)
        self.default_settings()

    def default_settings(self):

        print("defulat settings")
        self.solution_display = 1
        
    def read_json(self,input_file):

        with open(input_file) as json_file:
            data = json.load(json_file)
            for key in data.keys():
                setattr(self,key,data[key])

    def settings_after_json(self):

        np.random.seed(self.seed)

        self.c = [float(np.random.randint(low=1,high=self.n)) for j in range(self.n)]
        self.M = 0.5*self.n
        self.B = self.n
        self.d = [self.d for i in range(self.i)]

        if self.single_supply == True:
            self.supply = np.ones((self.n))
        else:
            self.supply = np.random.randint(self.n,size=self.n)

    def solve_problem(self):


        self.add_vars_to_prob()
        self.add_constraints_to_prob()
        self.display_settings()
        self.solver_settings()

        if self.ksb_method == True:
            self.ksb_sols = []
            self.ksb_sols_index = []

        self.register_ksb_branching_callback()

        self.register_lazy_callback()
        self.register_node_callback()
        self.prob.solve()

        self.end_time = time.time()

        if self.show_solution != 0:
            self.show_solution()

        self.save_results()

    def solver_settings(self):

        self.prob.parameters.preprocessing.linear.set(0)
        self.prob.parameters.mip.strategy.search.set(self.prob.parameters.mip.strategy.search.values.traditional)
        self.prob.parameters.timelimit.set(3600)

    def display_settings(self):

        print("DISPLAY SETTINGS")

        # self.prob.parameters.mip.interval.set(10)

        # self.prob.set_log_stream(None)
        # self.prob.set_error_stream(None)
        # self.prob.set_warning_stream(None)
        # self.prob.set_results_stream(None)
        # self.prob.parameters.mip.display.set(0)

    def register_lazy_callback(self):

        self.lazy_callback = self.prob.register_callback(LazyConsCallback)

        self.lazy_callback.x_names = self.x_names
        self.lazy_callback.xbar_names = self.xbar_names
        self.lazy_callback.i = self.i
        self.lazy_callback.d = self.d
        self.lazy_callback.n = self.n
        self.lazy_callback.c = self.c
        self.lazy_callback.supply = self.supply
        self.lazy_callback.A = self.A
        self.lazy_callback.b = self.b
        self.lazy_callback.y_names = self.y_names
        self.lazy_callback.z_names = self.z_names
        self.lazy_callback.no_lazy_callbacks = 0
        self.lazy_callback.no_lazy_cuts = 0

    def register_node_callback(self):

        self.node_callback = self.prob.register_callback(Node_callback)
        self.node_callback.node_count = 0
        self.node_callback.xbar_names = self.xbar_names

    def register_ksb_branching_callback(self):
        self.branch_callback = self.prob.register_callback(KSB_branch_callback)
        self.branch_callback.branch_count = 0
        self.branch_callback.branch_node_count = 0
        self.branch_callback.ksb_method = self.ksb_method
        self.branch_callback.sol_count = 0
        self.branch_callback.generate_solutions = self.generate_solutions
        self.branch_callback.generate_solutions_on_fly = self.generate_solutions_on_fly
        self.branch_callback.ksb_no_sols = self.ksb_no_sols
        self.branch_callback.y_df = self.y_df
        self.branch_callback.ksb_sols = []
        self.branch_callback.ksb_sols_index = []

        #self.prob.parameters.preprocessing.reformulations.set(self.prob.parameters.preprocessing.reformulations.values.none)

        # if self.ksb_method == True:
        #     self.branch_callback.ksb_sols = self.ksb_sols
        #     self.branch_callback.ksb_sols_index = self.ksb_sols_index

    def add_vars_to_prob(self):

        # Add variables
        self.create_y_vars()

        self.create_x_vars()
        self.create_xbar_vars()
        self.create_z_vars()
        self.create_u_vars()

    def add_constraints_to_prob(self):

        self.budget_constraint()
        self.x_xbar_big_M_constraint()
        self.group_constraints()
        self.follower_supply_constraint()
        self.z_mccormick_constraints()
        self.u_mccormick_constraints()

    def create_x_vars(self):

        self.x_names = ["x_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [0] * self.n,
                                lb = [0] * self.n,
                                ub = [self.M] * self.n,
                                types = "C" * self.n,
                                names = self.x_names)

    def create_xbar_vars(self):

        self.xbar_names = ["xbar_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [-float(self.c[j]) for j in range(self.n)],
                                lb = [0] * self.n,
                                ub = self.supply,
                                types = "I" * self.n,
                                names = self.xbar_names)

    def create_y_vars(self):

        self.y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        self.y_df = pd.DataFrame()

        # # Add variables
        for i in range(self.i):
            for j in range(self.d[i]):
                self.y_df = self.y_df.append(self.y_names[i][j])
                self.prob.variables.add(obj= [0] * len(self.y_names[i][j]),
                                lb = [0] * len(self.y_names[i][j]),
                                ub = [1] * len(self.y_names[i][j]),
                                types = "I" * len(self.y_names[i][j]),
                                names = self.y_names[i][j])

    def create_z_vars(self):

        self.z_names = ["z_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [1] * self.n,
                                lb = [0] * self.n,
                                ub = [self.M] * self.n,
                                types = "C" * self.n,
                                names = self.z_names)

    def create_u_vars(self):

        self.u_names = ["u_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [float(self.c[j]) for j in range(self.n)],
                                lb = [0] * self.n,
                                ub = [1] * self.n,
                                types = "B" * self.n,
                                names = self.u_names)

    def budget_constraint(self):

        self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = self.c)],
                                         senses = ["L"],
                                         rhs = [self.B],
                                         names = ['Budget'])

    def x_xbar_big_M_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-self.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def k_card_constraints(self):

        for i in range(self.i):
            self.A[i] = np.append(self.A[i],np.ones((1,self.n)),axis=0)
            self.b[i] = np.append(self.b[i],np.array([[self.k_card_rhs]]))

    def knapsack_constraints(self):

        for i in range(self.i):
            self.A[i] = np.append(self.A[i],np.array([self.c]),axis=0)
            self.b[i] = np.append(self.b[i],np.array([[self.knapsack_rhs]]))

    def partition_matroid_constraints(self):

        primes = []
        number = 1000
        for num in range(2,number + 1):
            if len(primes) == self.partition_matroid_no:
                break

            if num > 1:
                for i in range(2,num):
                    if (num % i) == 0:
                        break
                else:
                    primes += [num]

        if len(primes) > 0:
            for i in range(self.i):
                self.A[i] = np.append(self.A[i],np.array([[1 if j%prime == 0 else 0 for j in range(self.n)] for prime in primes]),axis=0)
                self.b[i] = np.append(self.b[i],np.ones((self.partition_matroid_no))*self.partition_matroid_rhs)

    def group_constraints(self):

        self.A = [np.zeros((0,self.n)) for i in range(self.i)]
        self.b = [np.zeros((0,1)) for i in range(self.i)]

        self.k_card_constraints()
        self.knapsack_constraints()
        self.partition_matroid_constraints()

        for i in range(self.i):
            self.A[i] = self.A[i].astype(float)
            self.b[i] = self.b[i].astype(float)

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(np.shape(self.A[i])[0]):
                    self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                                      val=list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = [self.b[i][j]])

    def follower_supply_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses = ["L"],
                                             rhs = [self.supply[j]],
                                             names = ["Fol_supply_" + str(j)])

    def z_mccormick_constraints(self):

        """
        z_j = (\sum_{i} \sum_{k} y^{ik}_j) x_j
        """

        for j in range(self.n):

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j],self.x_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[-1,1] + [self.M for i in range(self.i) for k in range(self.d[i])])],
                                             senses  =["L"],
                                             rhs     =[self.M])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1] + [-self.M for i in range(self.i) for k in range(self.d[i])])],
                                             senses  =["L"],
                                             rhs     =[0])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j],self.x_names[j]],
                                                                        val=[1,-1])],
                                             senses  =["L"],
                                             rhs     =[0])

    def u_mccormick_constraints(self):

        """
        u_j = (\sum_{i} \sum_{k} y^{ik}_j) xbar_j
        """

        for j in range(self.n):

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.xbar_names[j],self.u_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1,-1] + [1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses  =["L"],
                                             rhs     =[1])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1] + [-1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses  =["L"],
                                             rhs     =[0])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[j],self.xbar_names[j]],
                                                                        val=[1,-1])],
                                             senses  =["L"],
                                             rhs     =[0])

    def show_solution(self):

        print(self.prob.solution.get_status())

        if self.prob.solution.get_status() == 101:

            if self.solution_display == 1:

                print("OBJ    = ",np.round(self.prob.solution.get_objective_value(),decimals=2))
                print("INC    = ",np.round(np.sum([(self.prob.solution.get_values(self.z_names[j]) + self.prob.solution.get_values(self.u_names[j])*self.c[j]) for j in range(self.n)]),decimals=2))
                print("EXP    = ",np.round(np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]),decimals=2))

                print("F.COST = ",np.round(np.sum([self.prob.solution.get_values(self.z_names[j]) for j in range(self.n)]) + np.sum([np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])])*self.c[j] for j in range(self.n)]),decimals=2))

            elif self.solution_display == 2:
                print("OBJ    = ",self.prob.solution.get_objective_value())
                print("INC    = ",np.sum([(self.prob.solution.get_values(self.z_names[j]) + self.prob.solution.get_values(self.u_names[j])*self.c[j]) for j in range(self.n)]))
                print("EXP    = ",np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]))

                print("F.COST = ",np.sum([self.prob.solution.get_values(self.z_names[j]) for j in range(self.n)]) + np.sum([np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])])*self.c[j] for j in range(self.n)]))


                # Create VStack

                vstack = np.vstack((self.supply,
                                    self.prob.solution.get_values(self.xbar_names),
                                    self.prob.solution.get_values(self.x_names),
                                    self.prob.solution.get_values(self.z_names),
                                    self.c,
                                    [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    self.prob.solution.get_values(self.u_names))).T

                df = pd.DataFrame(vstack,columns=["S","XBAR","X","Z","C","YSUM","U"])
                print("\n")
                print(df)

            # ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
            # print("YSUM = ",[i for i in range(self.n) if ysum[i] > 0.5])
            # print("XBAR = ",[i for i in range(self.n) if self.prob.solution.get_values(self.xbar_names[i]) > 0.5])

    def save_results(self):
        

        ksb_opt = -1

        try:
            obj = np.around(self.prob.solution.get_objective_value(),decimals=2)
        except:
            obj = -1

        """Status Codes
        101     Optimal
        102     Tolerance optimal
        103     Infeasible
        104     Solution limit
        105     Node Limit Feasible
        106     Node limit infeasible
        107     Time limit feasible
        108     Time limit infeasible
        109     Error but solution exists
        110     Error and no solution exists
        111     Memory limit but feasible
        112     Memory limit and infeasible
        113     Aborted but integer solution exists
        114     Aborted no integer solution exists
        115     Problem optimal with unscaled infeasibilities
        116     Out of memory, no tree, integer sol exists
        117     Out of memory, no tree, no int sol
        118     Problem unbounded
        119     Problem infeasible or unbounded
        120     


        """

        if self.prob.solution.get_status() == 101:

            ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

            if self.ksb_method == True:
                print("\n")
                print("YSUM   = ",[j for j in range(self.n) if ysum[j] > 0.5])
                for i in range(len(self.branch_callback.ksb_sols)):
                    self.branch_callback.ksb_sols_index[i].sort()
                    print("Y ",i,"  = ",self.branch_callback.ksb_sols_index[i])

                if self.branch_callback.branch_node_count == 0:
                    ksb_opt = -2

                if [j for j in range(self.n) if ysum[j] > 0.5] in self.branch_callback.ksb_sols_index:
                    ksb_opt = self.branch_callback.ksb_sols_index.index([j for j in range(self.n) if ysum[j] > 0.5])

        results_data = [["seed",            self.seed],
                        ["single_supply",   self.single_supply],
                        ["KSB_method",      self.ksb_method],
                        ["KSB_no_sols",     self.ksb_no_sols],
                        ["n",               self.n],
                        ["i",               self.i],
                        ["d_sum",           np.sum(self.d)],
                        ["k_card_rhs",      self.k_card_rhs],
                        ["knapsack_rhs",    self.knapsack_rhs],
                        ["par_mat_no",      self.partition_matroid_no],
                        ["par_mat_rhs",     self.partition_matroid_rhs],
                        ["No_nodes",        self.node_callback.node_count],
                        ["No_branch_nodes", self.branch_callback.branch_node_count],
                        ["No_lazy_calls",   self.lazy_callback.no_lazy_callbacks],
                        ["No_lazy_cuts",    self.lazy_callback.no_lazy_cuts],
                        ["Obj",             obj],
                        ["KSB_Sol",         ksb_opt],
                        ["Time",            np.round(self.end_time - self.start_time,decimals=4)],
                        ["Status",          self.prob.solution.get_status()]]

        results_df = pd.DataFrame(results_data).T

        print(results_df.T)

        saved_name = str(self.seed) + "_" + \
                     str(self.single_supply) + "_" + \
                     str(self.ksb_no_sols) + "_" + \
                     str(self.ksb_method) + "_" + \
                     str(self.n) + "_" + \
                     str(self.i) + "_" + \
                     str(np.sum(self.d)) + "_" + \
                     str(self.k_card_rhs) + "_" + \
                     str(self.knapsack_rhs) + "_" + \
                     str(self.partition_matroid_no) + "_" + \
                     str(self.partition_matroid_rhs)

        results_df.to_csv(saved_name + '.txt',sep=";",header=False,index=False)







"""
waste
"""
       # for i in range(self.i):

        #     print("no_rows",i,self.A_rows[i])
        #     # count = self.A_rows[i]
        #     number = 100
        #     primes = []
        #     for num in range(2,number + 1):
        #         print(len(primes))
        #         print(self.A_rows[i])
        #         if len(primes) == self.A_rows[i]:
        #             break
                
        #         for k in range(2,num):
        #             if (num % k) == 0:
        #                 break
        #         else:
        #             primes += [num]

        #     print(primes)
        #     print([1 if i==2 else 0 for i in range(10)])
        #     A = [[1 if i%k==0 else 0 for i in range(self.n)] for k in primes]
        #     print(A)

        #     self.A[i] = np.array(([[1 for j in range(self.n)] for prime in primes]))
        #     print(self.A[i])

            # # Create Constraints matrix
            # self.A[i] = np.random.randint(  low=self.A_coef_min,
            #                         high=self.A_coef_max,
            #                         size=(self.A_rows[i],self.n))

            # print(self.A[i])

            # self.b[i] = np.random.randint(  low=self.b_coef_min,
            #                         high=self.b_coef_max,
            #                         size=(self.A_rows[i],1))

            # self.A[i] = self.A[i].astype(float)
            # self.b[i] = self.b[i].astype(float)