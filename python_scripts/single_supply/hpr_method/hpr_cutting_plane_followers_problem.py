"""
Single Supply HPR cutting plane

Followers Class
"""

import cplex
import numpy as np
import pandas as pd

class FOL_PROB():

    def __init__(self,x,show_sol=0):

        self.fol_prob = cplex.Cplex()
        self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)

        self.x = x
        self.show_sol = show_sol

    def ksb_solution_generation(self,no_sols):

        ksb_sols = []

        self.add_y_variables()
        self.group_constraints()
        self.fol_supply_constraints()
        self.fol_prob_settings()
        
        for sols in range(no_sols):
            
            self.fol_prob.solve()

            IND = [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) >= 0.5]
            IDX = [j for j in range(self.n) if np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) >= 0.5]
            ksb_sols.append(IND)
            print("IDX = ",IDX)
            print("OBJ = ",self.fol_prob.solution.get_objective_value())

            for kdx,ksb_sol in enumerate(ksb_sols):        
                # print("kdx",kdx)
                # print("sol",np.sum([self.fol_prob.solution.get_values([ksb_sol[j]]) for j in range(len(ksb_sol))]))


                self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if j in IDX],
                                                                                val=[1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if j in IDX])],
                                                    senses  =["L"],
                                                    rhs     =[float(len(IDX)-1)])

        return ksb_sols

    def ksb_solution_generation_fly(self):

        self.add_y_variables()
        self.group_constraints()
        self.fol_supply_constraints()
        self.fol_prob_settings()

        for sols in self.ksb_sols:

            IND = [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) >= 0.5]
            IDX = [j for j in range(self.n) if np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) >= 0.5]
 
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if j in IDX],
                                                                                val=[1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if j in IDX])],
                                                    senses  =["L"],
                                                    rhs     =[float(len(IDX)-1)])

        self.fol_prob.solve()
        print(self.fol_prob.solution.get_values())


    def solve_followers_problem(self):

        self.add_y_variables()
        self.group_constraints()
        self.fol_supply_constraints()
        self.fol_prob_settings()
        self.fol_prob.solve()

        if self.show_sol == 1:
            self.fol_show_solutions()


        return self.fol_prob.solution.get_objective_value(),[[self.fol_prob.solution.get_values(self.fol_y_names[i][k]) for k in range(self.d[i])] for i in range(self.i)]
        # [[self.fol_prob.solution.get_values(self.fol_y_names[i][k])] for i in range(self.i) for k in range(self.d[i])]

    def fol_prob_settings(self):
        self.fol_prob.set_log_stream(None)
        self.fol_prob.set_error_stream(None)
        self.fol_prob.set_warning_stream(None)
        self.fol_prob.set_results_stream(None)
        self.fol_prob.parameters.mip.display.set(0)

    def add_y_variables(self):

        self.fol_y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        # # Add variables
        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj= [self.c[j] + self.x[j] for j in range(self.n)],
                                lb = [0] * len(self.fol_y_names[i][k]),
                                ub = [1] * len(self.fol_y_names[i][k]),
                                types = "I" * len(self.fol_y_names[i][k]),
                                names = self.fol_y_names[i][k])

    def group_constraints(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(np.shape(self.A[i])[0]):
                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.fol_y_names[i][k],
                                                                                      val=list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = [self.b[i][j]])

    def fol_supply_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                            val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                                 senses  =["L"],
                                                 rhs     =[float(self.supply[j])])

    def fol_show_solutions(self):

        print("\n")
        print("FOL PROB OBJ:",self.fol_prob.solution.get_objective_value())

        vstack = np.vstack((self.supply,
                            self.x,
                            self.c,
                            [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                            )).T
        df = pd.DataFrame(vstack,columns=["S","X","C","YSUM"])

        print(df)
