"""
Single supply lambda method main
"""

import numpy as np
import time
import sys

from lambda_leader import Prob

print("INPUT FILE:",sys.argv[1])

Prob = Prob()
Prob.read_json(sys.argv[1])
Prob.settings_after_json()
Prob.start_time = time.time()
Prob.solve_problem()