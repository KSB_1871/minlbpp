"""
Lambda leader
"""
import cplex
import numpy as np
import pandas as pd
import time
import json

class Prob():

    def __init__(self):

        print("Prob Class")
        self.prob = cplex.Cplex()
        self.prob.objective.set_sense(self.prob.objective.sense.maximize)
        self.default_settings()

    def default_settings(self):

        print("defulat settings")
        self.solution_display = 1

    def read_json(self,input_file):

        with open(input_file) as json_file:
            data = json.load(json_file)
            for key in data.keys():
                setattr(self,key,data[key])

    def settings_after_json(self):

        np.random.seed(self.seed)

        self.c = [float(np.random.randint(low=1,high=self.n)) for j in range(self.n)]
        self.M = 0.5*self.n
        self.B = self.n*10
        self.d = [self.d for i in range(self.i)]
        self.initial_lambda_size = 1

        if self.single_supply == True:
            self.supply = np.ones((self.n))
        else:
            self.supply = np.random.randint(low=1,high=np.sum(self.d)+1,size=self.n)
            self.supply.astype(float)

    def solve_problem(self):

        print("SOLVING")

        print("Generating Lambda")
        self.generate_lambda_set()



        self.add_vars_to_prob()
        # self.add_constraints_to_prob()
        # self.display_settings()
        # self.solver_settings()

        self.prob.solve()

    def add_vars_to_prob(self):

        # Add variables
        self.create_xbar_vars()
        self.create_x_vars()
        self.create_initial_lambda_variables(self)

    def create_x_vars(self):

        self.x_names = ["x_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [0] * self.n,
                                lb = [0] * self.n,
                                ub = [self.M] * self.n,
                                types = "C" * self.n,
                                names = self.x_names)

    def create_xbar_vars(self):

        self.xbar_names = ["xbar_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [-float(self.c[j]) for j in range(self.n)],
                                lb = [0] * self.n,
                                ub = [float(self.supply[i]) for i in range(self.n)],
                                types = "I" * self.n,
                                names = self.xbar_names)

    def create_initial_lambda_variables(self):

        self.lambda_names = ["lambda_" + str(l) for l in range(self.initial_lambda_size)]

        self.prob.variables.add(obj   = [],
                                lb    = [0] * self.initial_lambda_size,
                                ub    = [1] * self.initial_lambda_size,
                                types = "B" * self.initial_lambda_size,
                                names = self.lambda_names)


    # POST PROCESSING

    def show_solution(self):

        print("STATUS :",self.prob.solution.get_status())

        if self.prob.solution.get_status() == 101:

            self.solution_display = 2
            print("DISPLAY:",self.solution_display)


            if self.solution_display == 1:


                # for i in range(self.i):
                #     for k in range(self.d[i]):
                #         print(self.prob.solution.get_values(self.z_names[i][k]))

                z_sum       = [np.sum(self.prob.solution.get_values(self.z_names[i][k])) for i in range(self.i) for k in range(self.d[i])]
                ybar_sum    = [np.sum([self.prob.solution.get_values(self.ybar_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]
                y_sum       = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]

                print("OBJ    = ",np.round(self.prob.solution.get_objective_value(),decimals=2))
                print("INC    = ",np.round(np.sum(z_sum)+np.sum(ybar_sum),decimals=2))
                print("EXP    = ",np.round(np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]),decimals=2))
                print("F.COST = ",np.round(np.sum(z_sum)+np.sum(ybar_sum)+np.sum(y_sum),decimals=2))

            elif self.solution_display == 2:

                z_sum       = [np.sum(self.prob.solution.get_values(self.z_names[i][k])) for i in range(self.i) for k in range(self.d[i])]
                ybar_sum    = [np.sum([self.prob.solution.get_values(self.ybar_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]
                y_sum       = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]

                print("OBJ    = ",np.round(self.prob.solution.get_objective_value(),decimals=2))
                print("INC    = ",np.round(np.sum(z_sum)+np.sum(ybar_sum),decimals=2))
                print("EXP    = ",np.round(np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]),decimals=2))
                print("F.COST = ",np.round(np.sum(z_sum)+np.sum(ybar_sum)+np.sum(y_sum),decimals=2))


                # Create VStack

                vstack = np.vstack((self.supply,
                                    self.prob.solution.get_values(self.xbar_names),
                                    self.prob.solution.get_values(self.x_names),
                                    [np.sum([self.prob.solution.get_values(self.z_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    self.c,
                                    [np.sum([self.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    )).T

                # vstack = np.vstack((self.supply,
                #                     self.prob.solution.get_values(self.xbar_names),
                #                     self.prob.solution.get_values(self.x_names),
                #                     self.prob.solution.get_values(self.z_names),
                #                     self.c,
                #                     [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                #                     self.prob.solution.get_values(self.u_names))).T

                df = pd.DataFrame(vstack,columns=["S","XBAR","X","Z","C","YBARSUM","YSUM"])#,"U"])
                print("\n")
                print(df)

            # ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
            # print("YSUM = ",[i for i in range(self.n) if ysum[i] > 0.5])
            # print("XBAR = ",[i for i in range(self.n) if self.prob.solution.get_values(self.xbar_names[i]) > 0.5])

    def save_results(self):
        

        ksb_opt = -1

        try:
            obj = np.around(self.prob.solution.get_objective_value(),decimals=2)
        except:
            obj = -1

        if self.prob.solution.get_status() == 101:

            ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

            if self.ksb_method == True:
                print("\n")
                print("YSUM   = ",[j for j in range(self.n) if ysum[j] > 0.5])
                for i in range(len(self.branch_callback.ksb_sols)):
                    self.branch_callback.ksb_sols_index[i].sort()
                    print("Y ",i,"  = ",self.branch_callback.ksb_sols_index[i])

                if self.branch_callback.branch_node_count == 0:
                    ksb_opt = -2

                if [j for j in range(self.n) if ysum[j] > 0.5] in self.branch_callback.ksb_sols_index:
                    ksb_opt = self.branch_callback.ksb_sols_index.index([j for j in range(self.n) if ysum[j] > 0.5])

        results_data = [["seed",            self.seed],
                        ["single_supply",   self.single_supply],
                        ["KSB_method",      self.ksb_method],
                        ["KSB_no_sols",     self.ksb_no_sols],
                        ["n",               self.n],
                        ["i",               self.i],
                        ["d_sum",           np.sum(self.d)],
                        ["k_card_rhs",      self.k_card_rhs],
                        ["knapsack_rhs",    self.knapsack_rhs],
                        ["par_mat_no",      self.partition_matroid_no],
                        ["par_mat_rhs",     self.partition_matroid_rhs],
                        ["No_nodes",        self.node_callback.node_count],
                        ["No_branch_nodes", self.branch_callback.branch_node_count],
                        ["No_lazy_calls",   self.lazy_callback.no_lazy_callbacks],
                        ["No_lazy_cuts",    self.lazy_callback.no_lazy_cuts],
                        ["Obj",             obj],
                        ["KSB_Sol",         ksb_opt],
                        ["Time",            np.round(self.end_time - self.start_time,decimals=4)],
                        ["Status",          self.prob.solution.get_status()]]

        results_df = pd.DataFrame(results_data).T

        print(results_df.T)

        saved_name = str(self.seed) + "_" + \
                     str(self.single_supply) + "_" + \
                     str(self.ksb_no_sols) + "_" + \
                     str(self.ksb_method) + "_" + \
                     str(self.n) + "_" + \
                     str(self.i) + "_" + \
                     str(np.sum(self.d)) + "_" + \
                     str(self.k_card_rhs) + "_" + \
                     str(self.knapsack_rhs) + "_" + \
                     str(self.partition_matroid_no) + "_" + \
                     str(self.partition_matroid_rhs)

        results_df.to_csv(saved_name + '.txt',sep=";",header=False,index=False)

