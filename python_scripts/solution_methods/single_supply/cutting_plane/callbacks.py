"""
Single Supply Callbacks
"""
import numpy as np
import cplex

from cplex.callbacks import  LazyConstraintCallback, NodeCallback, BranchCallback
from solution_methods.single_supply.followers_problem import FOL_PROB

class standard_branch_callback(BranchCallback):

    def __call__(self):

        self.branch_count += 1
        self.node_count += self.get_num_branches()

class KSB_branch_callback(BranchCallback):

    def __call__(self):

        if self.get_node_data() != None:
            print(self.get_node_data())

        if self.branch_count == 0:
            # At the root node

            LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

            self.Y_init.append(sorted(LB_idx))

            self.ksb_sols_index.append((RB_idx,len(LB_idx)))

            self.make_branch(objective_estimate = self.get_objective_value(),
                             constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                             node_data          = [(("KSB",self.sol_count,"L"),None,None)])

            self.make_branch(objective_estimate = self.get_objective_value(),
                             constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                             node_data          = [(("KSB",self.sol_count,"R"),None,None)])

            self.sol_count += 1
            self.fol_prob_nodes += nodes

        elif self.sol_count < self.Y_init_size and \
                self.get_node_data() != None and \
                self.get_node_data()[0][0] == "KSB":
                # On a KSB node

                if self.get_node_data()[0][2] == "L":

                    # Carry on as normal
                    for i in range(self.get_num_branches()):
                        self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

                elif self.get_node_data()[0][2] == "R":

                    # Need to produce a new solution
                    LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                    self.Y_init.append(sorted(LB_idx))

                    self.ksb_sols_index.append((RB_idx,len(LB_idx)))

                    self.make_branch(objective_estimate = self.get_objective_value(),
                                    constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                    node_data          = [(("KSB",self.sol_count,"L"),None,None)])

                    self.make_branch(objective_estimate = self.get_objective_value(),
                                    constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                    node_data          = [(("KSB",self.sol_count,"R"),None,None)])

                    self.sol_count += 1
                    self.fol_prob_nodes += nodes

        else:

            # Carry on as normal
            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        
        
        self.branch_count += 1
        self.node_count += self.get_num_branches()


    def generate_solutions_on_fly(self):

        fol_prob = FOL_PROB(self.n,self.i,self.d,self.A,self.b,self.c,self.supply,0)

        LB_names, RB_names, nodes = fol_prob.ksb_solve_followers_problem(self.ksb_sols_index)

        self.no_fol_prob_solves += 1

        LB_idx = [self.var_list.index(LB_name) for LB_name in LB_names]
        RB_idx = [self.var_list.index(RB_name) for RB_name in RB_names]

        return LB_idx, RB_idx, nodes

class standard_node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1

class direct_lazy_constraint_callback(LazyConstraintCallback):

    def __call__(self):

        self.no_lazy_callbacks += 1

        fol_prob = FOL_PROB(self.n,
                            self.i,
                            self.d,
                            self.A,
                            self.b,
                            [float(self.get_values(self.x_names[j]) + self.c[j]) for j in range(self.n)],
                            self.supply,
                            show_sol=0)

        fol_obj, fol_ysum, fol_prob_nodes,LB_names,RB_names = fol_prob.solve_followers_problem(sol_pool_sol=True)

        self.no_fol_prob_solves += 1

        self.fol_prob_nodes += fol_prob_nodes
        
        curr_obj = np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) \
                                                                                   for k in range(self.d[i])]) for j in range(self.n)])\
                                    *np.array([self.get_values(self.x_names[j]) + self.c[j] for j in range(self.n)]))

        if fol_obj <= curr_obj - 0.1:

            self.add(constraint = cplex.SparsePair(ind=[self.p_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                       [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                       self.x_names,
                                                   val=[1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                       [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                       [float(-fol_ysum[j]) for j in range(self.n)]),
                     sense      = "L",
                     rhs        = float(np.sum([fol_ysum[j]*self.c[j] for j in range(self.n)])))

            self.no_lazy_cuts += 1

        else:
            print("Bilevel Feasible")

class symmetric_lazy_constraint_callback(LazyConstraintCallback):

    def __call__(self):

        self.no_lazy_callbacks += 1

        fol_prob = FOL_PROB(self.n,
                            self.i,
                            self.d,
                            self.A,
                            self.b,
                            [float(self.get_values(self.x_names[j]) + self.c[j]) for j in range(self.n)],
                            self.supply,
                            show_sol=0)

        fol_obj, fol_ysum, fol_prob_nodes,LB_names,RB_names = fol_prob.solve_followers_problem(sol_pool_sol=True)

        self.no_fol_prob_solves += 1

        self.fol_prob_nodes += fol_prob_nodes
        
        curr_obj = np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) \
                                                                                   for k in range(self.d[i])]) for j in range(self.n)])\
                                    *np.array([self.get_values(self.x_names[j]) + self.c[j] for j in range(self.n)]))

        if fol_obj <= curr_obj - 0.1:

            self.LB_names.append(LB_names)
            self.RB_names.append(RB_names)

            self.add(constraint = cplex.SparsePair(ind=self.p_names + \
                                                       [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                       self.x_names,
                                                   val=[1 for j in range(self.n)] + \
                                                       [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                       [float(-fol_ysum[j]) for j in range(self.n)]),
                     sense      = "L",
                     rhs        = float(np.sum([fol_ysum[j]*self.c[j] for j in range(self.n)])))

            self.no_lazy_cuts += 1

        else:
            print("Bilevel Feasible")





