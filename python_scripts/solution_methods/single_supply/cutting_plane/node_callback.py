"""
Single Supply Cutting plane Node Callback
"""
from cplex.callbacks import  NodeCallback
 
class Node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1