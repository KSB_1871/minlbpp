"""
Single Supply cutting plane method
"""
import cplex
import numpy as np
import pandas as pd
import time

from solution_methods.single_supply.cutting_plane.node_callback import Node_callback
from solution_methods.single_supply.cutting_plane.lazy_callback import Lazy_callback
from solution_methods.single_supply.cutting_plane.branch_callback import standard_branch_callback
from solution_methods.single_supply.cutting_plane.branch_callback import KSB_branch_callback

class cutting_plane_method():

    def __init__(self,master):

        master.prob = cplex.Cplex()
        master.prob.objective.set_sense(master.prob.objective.sense.maximize)
        master.prob.parameters.timelimit.set(np.max([0,3600 - (time.time() - master.start_time)]))

        self.add_vars(master)
        self.add_constraints(master)
        self.register_callbacks(master)


    def solve(self,master):

        print("\n")
        master.prob.solve()

        # self.display_solution(master)

        try:
            sol = master.prob.solution.get_objective_value()
            bi_feas = True
        except:
            sol = -1
            bi_feas = False

        try:
            mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
        except:
            mip_gap = -1

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             sol],
                ["Bi_Feas",         bi_feas],
                ["MIP_Gap",         mip_gap],
                ["No_Lazy_Calls",   self.lazy_callback.no_lazy_callbacks],
                ["No_Lazy_Cuts",    self.lazy_callback.no_lazy_cuts],
                ["No_Nodes",        self.node_callback.node_count],
                ["No_Branch_Nodes", self.branch_callback.node_count],
                ["Fol_Node_Count",  self.lazy_callback.fol_prob_nodes+self.branch_callback.fol_prob_nodes],
                ["No_KSB_Sols",     len(self.branch_callback.ksb_sols_index)],
                ["No_Lambda_Sols",  0]]
    
    def add_vars(self,master):

        self.add_y_vars(master)
        self.add_x_vars(master)
        self.add_xbar_vars(master)
        self.add_z_vars(master)
        self.add_u_vars(master)

    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.group_constraints(master)
        self.follower_supply_constraint(master)
        self.z_mccormick_constraints(master)
        self.u_mccormick_constraints(master)
        self.minimum_objective(master,0)

    def register_callbacks(self,master):

        self.register_node_callback(master)
        self.register_lazy_callback(master)
        self.register_branch_callback(master)

    # Callbacks

    def register_node_callback(self,master):

        self.node_callback = master.prob.register_callback(Node_callback)
        self.node_callback.node_count = 0

    def register_branch_callback(self,master):

        print("ksb_sols_index length",master.ksb_sols_index,len(master.ksb_sols_index))
        print(master.solution_method)

        if master.solution_method in [2,5,6]:

            self.branch_callback = master.prob.register_callback(KSB_branch_callback)

            self.branch_callback.node_count           = 0
            self.branch_callback.branch_count         = 0
            self.branch_callback.fol_prob_nodes       = 0
            self.branch_callback.ksb_sols             = master.ksb_sols
            self.branch_callback.ksb_sols_index       = master.ksb_sols_index
            self.branch_callback.i                    = master.i
            self.branch_callback.d                    = master.d
            self.branch_callback.n                    = master.n
            self.branch_callback.c                    = master.c
            self.branch_callback.supply               = master.supply
            self.branch_callback.A                    = master.A
            self.branch_callback.b                    = master.b
            self.branch_callback.y_names_flat         = self.y_names_flat
            self.branch_callback.sol_count            = 0
            self.branch_callback.LB_names             = self.lazy_callback.LB_names
            self.branch_callback.RB_names             = self.lazy_callback.RB_names
            self.branch_callback.Y                    = self.lazy_callback.Y
            self.branch_callback.Y_init               = []
            self.branch_callback.Y_init_size          = master.ksb_no_sols
            self.branch_callback.Y_idx                = 0

        else:

            self.branch_callback = master.prob.register_callback(standard_branch_callback)

            self.branch_callback.node_count           = 0
            self.branch_callback.branch_count         = 0
            self.branch_callback.fol_prob_nodes       = 0
            self.branch_callback.ksb_sols_index       = master.ksb_sols_index


    def register_lazy_callback(self,master):

        self.lazy_callback = master.prob.register_callback(Lazy_callback)

        self.lazy_callback.x_names              = self.x_names
        self.lazy_callback.xbar_names           = self.xbar_names
        self.lazy_callback.y_names              = self.y_names
        self.lazy_callback.z_names              = self.z_names
        self.lazy_callback.i                    = master.i
        self.lazy_callback.d                    = master.d
        self.lazy_callback.n                    = master.n
        self.lazy_callback.c                    = master.c
        self.lazy_callback.supply               = master.supply
        self.lazy_callback.A                    = master.A
        self.lazy_callback.b                    = master.b
        self.lazy_callback.no_lazy_callbacks    = 0
        self.lazy_callback.no_lazy_cuts         = 0
        self.lazy_callback.fol_prob_nodes       = 0
        self.lazy_callback.LB_names             = []
        self.lazy_callback.RB_names             = []
        self.lazy_callback.Y                    = []

    # Variables

    def add_y_vars(self,master):

        self.y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(master.n)]\
                                                                      for j in range(master.d[i])]\
                                                                      for i in range(master.i)]

        self.y_df = pd.DataFrame()
        self.y_names_flat = []

        # # Add variables
        for i in range(master.i):
            for k in range(master.d[i]):
                self.y_names_flat += self.y_names[i][k]
                master.prob.variables.add(obj   = [0] * len(self.y_names[i][k]),
                                        lb    = [0] * len(self.y_names[i][k]),
                                        ub    = [1] * len(self.y_names[i][k]),
                                        types = "I" * len(self.y_names[i][k]),
                                        names = self.y_names[i][k])

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(i) for i in range(master.n)]

        # Add variables
        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(i) for i in range(master.n)]

        # Add variables
        master.prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [1] * master.n,
                                  types = "B" * master.n,
                                  names = self.xbar_names)

    def add_z_vars(self,master):

        self.z_names = ["z_" + str(i) for i in range(master.n)]

        # Add variables
        master.prob.variables.add(obj= [1] * master.n,
                                lb = [0] * master.n,
                                ub = [master.M] * master.n,
                                types = "C" * master.n,
                                names = self.z_names)

    def add_u_vars(self,master):

        self.u_names = ["u_" + str(i) for i in range(master.n)]

        # Add variables
        master.prob.variables.add(obj= [float(master.c[j]) for j in range(master.n)],
                                lb = [0] * master.n,
                                ub = [1] * master.n,
                                types = "B" * master.n,
                                names = self.u_names)

    # Constraints
    
    def not_in_Y_constraints(self,master,Y):

        for l in range(len(Y)):

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(master.i)\
                                                                                                         for k in range(master.d[i])\
                                                                                                         for j in range(master.n)\
                                                                                                         if Y[l][j] >= 0.5],
                                                                            val = [1                     for i in range(master.i)\
                                                                                                         for k in range(master.d[i])\
                                                                                                         for j in range(master.n)\
                                                                                                         if Y[l][j] >= 0.5])],
                                               senses   = ["L"],
                                               rhs      = [float(np.sum(Y[l])-1)])

    def minimum_objective(self,master,value):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.z_names[j] for j in range(master.n)] + \
                                                                            [self.u_names[j] for j in range(master.n)] + \
                                                                            [self.xbar_names[j] for j in range(master.n)],
                                                                        val=[1 for j in range(master.n)] + \
                                                                            [float(master.c[j]) for j in range(master.n)] + \
                                                                            [-float(master.c[j]) for j in range(master.n)])],
                                           senses   = ["G"],
                                           rhs      = [value])

    def budget_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                           senses   = ["L"],
                                           rhs      = [master.B],
                                           names    = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def group_constraints(self,master):

        for i in range(master.i):
            for k in range(master.d[i]):
                for j in range(np.shape(master.A[i])[0]):
                    master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                                      val=list(master.A[i][j,:]))],
                                                       senses   = ["G"],
                                                       rhs      = [master.b[i][j]])

    def follower_supply_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(master.i)\
                                                                                                     for k in range(master.d[i])],
                                                                        val=[1 for i in range(master.i)\
                                                                               for k in range(master.d[i])])],
                                               senses   = ["L"],
                                               rhs      = [master.supply[j]],
                                               names    = ["Fol_supply_" + str(j)])

    def z_mccormick_constraints(self,master):

        """
        z_j = (\sum_{i} \sum_{k} y^{ik}_j) x_j
        """

        for j in range(master.n):

            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j],self.x_names[j]] + \
                                                                              [self.y_names[i][k][j] for i in range(master.i) \
                                                                                                     for k in range(master.d[i])],
                                                                          val=[-1,1] + \
                                                                              [master.M for i in range(master.i)\
                                                                                        for k in range(master.d[i])])],
                                             senses  =["L"],
                                             rhs     =[master.M])

            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j]] + \
                                                                              [self.y_names[i][k][j] for i in range(master.i) \
                                                                                                     for k in range(master.d[i])],
                                                                          val=[1] + \
                                                                              [-master.M for i in range(master.i) \
                                                                                         for k in range(master.d[i])])],
                                             senses  =["L"],
                                             rhs     =[0])

            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j],self.x_names[j]],
                                                                        val=[1,-1])],
                                             senses  =["L"],
                                             rhs     =[0])

    def u_mccormick_constraints(self,master):

        """
        u_j = (\sum_{i} \sum_{k} y^{ik}_j) xbar_j
        """

        for j in range(master.n):

            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.xbar_names[j],self.u_names[j]] + [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                          val=[1,-1] + [1 for i in range(master.i) for k in range(master.d[i])])],
                                             senses  =["L"],
                                             rhs     =[1])

            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[j]] + [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                          val=[1] + [-1 for i in range(master.i) for k in range(master.d[i])])],
                                             senses  =["L"],
                                             rhs     =[0])

            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[j],self.xbar_names[j]],
                                                                        val=[1,-1])],
                                             senses  =["L"],
                                             rhs     =[0])

    # Post Processing

    def display_solution(self,master):

        vstack = np.vstack((master.supply,
                    master.prob.solution.get_values(self.xbar_names),
                    master.prob.solution.get_values(self.x_names),
                    master.prob.solution.get_values(self.z_names),
                    master.c,
                    [np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i)\
                                                                                    for k in range(master.d[i])])\
                                                                                    for j in range(master.n)],
                    master.prob.solution.get_values(self.u_names))).T

        df = pd.DataFrame(vstack,columns=["S","XBAR","X","Z","C","YSUM","U"])
        print("\n")
        print("Solution")
        print("Objective:",master.prob.solution.get_objective_value())
        print("INC      :",np.sum(np.array([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i)\
                                                                                    for k in range(master.d[i])])\
                                                                                    for j in range(master.n)]) * \
                            np.array(master.prob.solution.get_values(self.xbar_names)) * \
                            np.array([master.prob.solution.get_values(self.x_names[j]) + master.c[j] for j in range(master.n)])))
        print("EXP      :",np.sum([master.c[j] * master.prob.solution.get_values(self.xbar_names[j]) for j in range(master.n)]))
        print("Fol Obj  :",np.sum(np.array([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i)\
                                                                                    for k in range(master.d[i])])\
                                                                                    for j in range(master.n)]) * \
                            np.array([master.prob.solution.get_values(self.x_names[j]) + master.c[j] for j in range(master.n)])))
        print(df)

