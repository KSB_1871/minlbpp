"""
Single Supply Cutting Plane Branch Callback
"""

from cplex.callbacks import BranchCallback
from solution_methods.single_supply.followers_problem import FOL_PROB


class standard_branch_callback(BranchCallback):

    def __call__(self):

        self.branch_count += 1
        self.node_count += self.get_num_branches()

class KSB_branch_callback(BranchCallback):

    def __call__(self):

        # print(self.sol_pool)

        # if self.get_node_data() != None:
        #     print(self.get_node_data())
        #     print(self.get_node_data()[-1][-1])
        #     print(self.sol_count)

        if self.branch_count == 0 or \
            self.sol_count < self.Y_init_size and self.get_node_data() != None and self.get_node_data()[-1][-1] == "R" + str(self.sol_count-1):

            # Find initial set Y
            # Generate the next follower solution

            LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

            self.Y_init.append(sorted(LB_idx))

            self.ksb_sols_index.append((RB_idx,len(LB_idx)))

            self.make_branch(objective_estimate = self.get_objective_value(),
                             constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                             node_data          = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))])

            self.make_branch(objective_estimate = self.get_objective_value(),
                             constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                             node_data          = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1,"R" + str(self.sol_count))])

            self.sol_count += 1
            self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and self.get_node_data()[-1][-1] == "R" + str(self.sol_count-1):

            if self.Y_idx == len(self.LB_names) - 1:
                """Done all the solutions we have found"""
                pass

            else:                    

                LB_idx = sorted([self.y_names_flat.index(self.LB_names[self.Y_idx][j]) for j in range(len(self.LB_names[self.Y_idx]))])
                RB_idx = sorted([self.y_names_flat.index(self.RB_names[self.Y_idx][j]) for j in range(len(self.RB_names[self.Y_idx]))])

                self.ksb_sols_index.append((RB_idx,len(LB_idx)))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))])

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1,"R" + str(self.sol_count))])

                self.Y_idx += 1
                self.sol_count += 1

        self.branch_count += 1
        self.node_count += self.get_num_branches()


    def generate_solutions_on_fly(self):

        fol_prob = FOL_PROB(self.n,self.i,self.d,self.A,self.b,self.c,self.supply,0)

        LB_names, RB_names, nodes = fol_prob.ksb_solve_followers_problem(self.ksb_sols_index)

        LB_idx = [self.y_names_flat.index(LB_name) for LB_name in LB_names]
        RB_idx = [self.y_names_flat.index(RB_name) for RB_name in RB_names]

        return LB_idx, RB_idx, nodes

