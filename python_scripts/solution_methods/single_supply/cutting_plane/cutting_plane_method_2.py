import cplex
import numpy as np
import pandas as pd
import time

from solution_methods.single_supply.cutting_plane.callbacks import *

from useful_functions.useful_functions import *

from solution_methods.single_supply.followers_problem import FOL_PROB


class cutting_plane_method():

    def __init__(self,master):

        self.var_list = []
        self.no_fol_prob_solves = 0

        master.prob = cplex.Cplex()
        master.prob.objective.set_sense(master.prob.objective.sense.maximize)
        master.prob.parameters.timelimit.set(np.max([0,3600 - (time.time() - master.start_time)]))

    def create_instance(self,master):

        self.add_vars(master)
        self.add_constraints(master)
        self.register_callbacks(master)

    def solve(self,master,Y=[],min_obj=[]):

        self.Y          = Y
        self.min_obj    = min_obj
        self.create_instance(master)

        master.prob.solve()

        try:
            sol = master.prob.solution.get_objective_value()
            bi_feas = True
        except:
            sol = -1
            bi_feas = False

        try:
            mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
        except:
            mip_gap = -1

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             sol],
                ["Bi_Feas",         bi_feas],
                ["MIP_Gap",         mip_gap],
                ["No_Lazy_Calls",   self.lazy_callback.no_lazy_callbacks],
                ["No_Lazy_Cuts",    self.lazy_callback.no_lazy_cuts],
                ["No_Nodes",        self.node_callback.node_count],
                ["No_Branch_Nodes", self.branch_callback.node_count],
                ["Fol_Node_Count",  self.lazy_callback.fol_prob_nodes+self.branch_callback.fol_prob_nodes],
                ["No_KSB_Sols",     len(self.branch_callback.ksb_sols_index)],
                ["No_Lambda_Sols",  0],
                ["No_Fol_Prob_Solves", self.no_fol_prob_solves + self.lazy_callback.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

    def add_vars(self,master):

        self.add_y_vars(master)
        self.add_x_vars(master)
        self.add_xbar_vars(master)
        self.add_p_vars(master)
        self.add_q_vars(master)

        if master.solution_method in [3,4,5,6]:

            self.add_r_vars(master)
        
    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.group_constraints(master)
        self.follower_supply_constraint(master)

        if master.solution_method in [1,2]:

            for j in range(master.n):
                for i in range(master.i):
                    for k in range(master.d[i]):
                        mccormick_constraints(master.prob,
                                                (self.y_names[i][k][j],0,1),
                                                (self.x_names[j],0,master.M),
                                                self.p_names[i][k][j])

            for j in range(master.n):
                for i in range(master.i):
                    for k in range(master.d[i]):
                        mccormick_constraints(master.prob,
                                                (self.y_names[i][k][j],0,1),
                                                (self.xbar_names[j],0,1),
                                                self.q_names[i][k][j])

        elif master.solution_method in [3,4,5,6]:

            self.r_y_sum_constraints(master)

            for j in range(master.n):
                mccormick_constraints(master.prob,
                        (self.r_names[j],0,1),
                        (self.x_names[j],0,master.M),
                        self.p_names[j])

                mccormick_constraints(master.prob,
                                        (self.r_names[j],0,1),
                                        (self.xbar_names[j],0,1),
                                        self.q_names[j])

        if master.solution_method in [5,6]:

            # Minimum objective value
            self.minimum_objective(master)

            # Solution not in Y
            self.not_in_Y_constraints(master)

    def register_callbacks(self,master):

        self.node_callback = master.prob.register_callback(standard_node_callback)
        
        self.node_callback.node_count = 0

        # Cutting Plane Callback
        if master.solution_method in [1,2]:
            # Direct MC Cutting Plane method
            self.lazy_callback = master.prob.register_callback(direct_lazy_constraint_callback)
            
            self.lazy_callback.x_names              = self.x_names
            self.lazy_callback.xbar_names           = self.xbar_names
            self.lazy_callback.y_names              = self.y_names
            self.lazy_callback.p_names              = self.p_names
            self.lazy_callback.i                    = master.i
            self.lazy_callback.d                    = master.d
            self.lazy_callback.n                    = master.n
            self.lazy_callback.c                    = master.c
            self.lazy_callback.supply               = master.supply
            self.lazy_callback.A                    = master.A
            self.lazy_callback.b                    = master.b

            self.lazy_callback.no_lazy_callbacks = 0
            self.lazy_callback.fol_prob_nodes    = 0
            self.lazy_callback.no_lazy_cuts      = 0
            self.lazy_callback.Y                 = []
            self.lazy_callback.LB_names          = []
            self.lazy_callback.RB_names          = []
            self.lazy_callback.no_fol_prob_solves = 0

        elif master.solution_method in [3,4,5,6]:
            # Symmetric MC + cutting plane
            self.lazy_callback = master.prob.register_callback(symmetric_lazy_constraint_callback)
            
            self.lazy_callback.x_names              = self.x_names
            self.lazy_callback.xbar_names           = self.xbar_names
            self.lazy_callback.y_names              = self.y_names
            self.lazy_callback.p_names              = self.p_names
            self.lazy_callback.i                    = master.i
            self.lazy_callback.d                    = master.d
            self.lazy_callback.n                    = master.n
            self.lazy_callback.c                    = master.c
            self.lazy_callback.supply               = master.supply
            self.lazy_callback.A                    = master.A
            self.lazy_callback.b                    = master.b

            self.lazy_callback.no_lazy_callbacks = 0
            self.lazy_callback.fol_prob_nodes    = 0
            self.lazy_callback.no_lazy_cuts      = 0
            self.lazy_callback.Y                 = []
            self.lazy_callback.LB_names          = []
            self.lazy_callback.RB_names          = []
            self.lazy_callback.no_fol_prob_solves = 0


        # Branch Callback
        if master.solution_method in [1,3,5,6]:
            # Standard branch callback
            self.branch_callback = master.prob.register_callback(standard_branch_callback)
        
            self.branch_callback.node_count = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [2,4]:
            # KSB method
            self.branch_callback = master.prob.register_callback(KSB_branch_callback)
        
            self.branch_callback.i                    = master.i
            self.branch_callback.d                    = master.d
            self.branch_callback.n                    = master.n
            self.branch_callback.c                    = master.c
            self.branch_callback.supply               = master.supply
            self.branch_callback.A                    = master.A
            self.branch_callback.b                    = master.b

            self.branch_callback.node_count           = 0
            self.branch_callback.branch_count         = 0
            self.branch_callback.fol_prob_nodes       = 0
            self.branch_callback.ksb_sols_index       = []
            self.branch_callback.Y_init_size          = master.ksb_no_sols
            self.branch_callback.Y_init               = []
            self.branch_callback.sol_count            = 0
            self.branch_callback.LB_names             = self.lazy_callback.LB_names
            self.branch_callback.RB_names             = self.lazy_callback.RB_names
            self.branch_callback.var_list             = self.var_list
            self.branch_callback.no_fol_prob_solves   = 0

    # Variables
    def add_y_vars(self,master):

        self.y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(master.n)]\
                                                                      for j in range(master.d[i])]\
                                                                      for i in range(master.i)]

        self.y_df = pd.DataFrame()
        self.y_names_flat = []

        # # Add variables
        for i in range(master.i):
            for k in range(master.d[i]):
                self.var_list += self.y_names[i][k]
                master.prob.variables.add(obj   = [0] * master.n,
                                        lb    = [0] * master.n,
                                        ub    = [1] * master.n,
                                        types = "I" * master.n,
                                        names = self.y_names[i][k])

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(i) for i in range(master.n)]

        self.var_list += self.x_names

        # Add variables
        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(i) for i in range(master.n)]

        self.var_list += self.xbar_names

        # Add variables
        master.prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [1] * master.n,
                                  types = "B" * master.n,
                                  names = self.xbar_names)

    def add_p_vars(self,master):

        """
        p = y * x
        """

        if master.solution_method in [1,2]:

            self.p_names = [[["p_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(master.n)]\
                                                                        for j in range(master.d[i])]\
                                                                        for i in range(master.i)]

            # # Add variables
            for i in range(master.i):
                for k in range(master.d[i]):
                    self.var_list += self.p_names[i][k]
                    master.prob.variables.add(obj   = [1] * master.n,
                                            lb    = [0] * master.n,
                                            ub    = [master.M] * master.n,
                                            types = "C" * master.n,
                                            names = self.p_names[i][k])

        elif master.solution_method in [3,4,5,6]:

            self.p_names = ["p_" + str(j) for j in range(master.n)]

            self.var_list += self.p_names

            master.prob.variables.add(obj   = [1] * master.n,
                                            lb    = [0] * master.n,
                                            ub    = [master.M] * master.n,
                                            types = "C" * master.n,
                                            names = self.p_names)

    def add_q_vars(self,master):

        """
        q = y * xbar
        """

        if master.solution_method in [1,2]:

            self.q_names = [[["q_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(master.n)]\
                                                                        for j in range(master.d[i])]\
                                                                        for i in range(master.i)]

            # # Add variables
            for i in range(master.i):
                for k in range(master.d[i]):
                    self.var_list += self.q_names[i][k]
                    master.prob.variables.add(obj   = [float(master.c[j]) for j in range(master.n)],
                                            lb    = [0] * master.n,
                                            ub    = [1] * master.n,
                                            types = "B" * master.n,
                                            names = self.q_names[i][k])


        elif master.solution_method in [3,4,5,6]:

            self.q_names = ["q_" + str(j) for j in range(master.n)]

            self.var_list += self.q_names

            master.prob.variables.add(obj   = [float(master.c[j]) for j in range(master.n)],
                                            lb    = [0] * master.n,
                                            ub    = [1] * master.n,
                                            types = "B" * master.n,
                                            names = self.q_names)

    def add_r_vars(self,master):

        """
        r = sum i sum k yik_j
        """

        self.r_names = ["r_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [0] * master.n,
                                    lb  = [0] * master.n,
                                    ub  = [1] * master.n,
                                    types   = "B" * master.n,
                                    names   = self.r_names)

    # Constraints
    def budget_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                           senses   = ["L"],
                                           rhs      = [master.B],
                                           names    = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def group_constraints(self,master):

        for i in range(master.i):
            for k in range(master.d[i]):
                for j in range(np.shape(master.A[i])[0]):
                    master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                                      val=list(master.A[i][j,:]))],
                                                       senses   = ["G"],
                                                       rhs      = [master.b[i][j]])

    def follower_supply_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(master.i)\
                                                                                                     for k in range(master.d[i])],
                                                                        val=[1 for i in range(master.i)\
                                                                               for k in range(master.d[i])])],
                                               senses   = ["L"],
                                               rhs      = [master.supply[j]],
                                               names    = ["Fol_supply_" + str(j)])

    def r_y_sum_constraints(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(master.i)\
                                                                                                         for k in range(master.d[i])] + \
                                                                                  [self.r_names[j]],
                                                                            val = [1 for i in range(master.i)\
                                                                                     for k in range(master.d[i])] + \
                                                                                  [-1])],
                                               senses   = ["E"],
                                               rhs      = [0])

    def not_in_Y_constraints(self,master):

        for l in range(len(self.Y)):

            index_list = []

            for j in range(master.n):

                if self.Y[l][j] >= 0.5:

                    index_list.append(j)

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in index_list],
                                                                           val = [1 for i in range(master.i) for k in range(master.d[i]) for j in index_list])],
                                               senses   = ["L"],
                                               rhs      = [float(len(index_list) - 1)])

    def minimum_objective(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names + \
                                                                              self.p_names + \
                                                                              self.q_names,
                                                                        val = [-float(master.c[j]) for j in range(master.n)] + \
                                                                              [1 for j in range(master.n)] + \
                                                                              [float(master.c[j]) for j in range(master.n)])],
                                            senses   = ["G"],
                                            rhs      = [float(self.min_obj)])
