"""
Single Supply Cutting plane Lazy Callback
"""
import numpy as np
import cplex

from cplex.callbacks import  LazyConstraintCallback
from solution_methods.single_supply.followers_problem import FOL_PROB

class ksb_testing:

    def __init__(self):

        self.sols = []

    def add_to_sols(self,sol):
        print("HI")
        self.sols.append(sol)

class Lazy_callback(LazyConstraintCallback):

    def __call__(self):


        self.no_lazy_callbacks += 1

        fol_prob = FOL_PROB(self.n,
                            self.i,
                            self.d,
                            self.A,
                            self.b,
                            [float(self.get_values(self.x_names[j]) + self.c[j]) for j in range(self.n)],
                            self.supply,
                            show_sol=0)

        fol_obj, fol_ysum, fol_prob_nodes,LB_names,RB_names = fol_prob.solve_followers_problem(sol_pool_sol=True)

        self.fol_prob_nodes += fol_prob_nodes

        curr_obj = np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) \
                                                                                   for k in range(self.d[i])]) for j in range(self.n)])\
                                    *np.array([self.get_values(self.x_names[j]) + self.c[j] for j in range(self.n)]))


        if fol_obj <= curr_obj - 0.1:

            if fol_ysum in self.Y:
                print("New solution already discovered")
            else:
                self.Y.append([i for i in range(self.n) if fol_ysum[i] >= 0.5])
                self.LB_names.append(LB_names)
                self.RB_names.append(RB_names)

            IND = self.z_names + \
                                                       [self.y_names[i][k][j] for i in range(self.i) \
                                                                              for k in range(self.d[i]) \
                                                                              for j in range(self.n)] + \
                                                       self.x_names

            VAL = [1 for j in range(self.n)] + \
                                                       [self.c[j] for i in range(self.i) \
                                                                  for k in range(self.d[i]) \
                                                                  for j in range(self.n)] + \
                                                       [float(-fol_ysum[j]) for j in range(self.n)]

            RHS = float(np.sum([fol_ysum[j]*self.c[j] for j in range(self.n)]))

            self.add(constraint = cplex.SparsePair(ind=self.z_names + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + self.x_names,
                                                   val=[1 for j in range(self.n)] + [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + [float(-fol_ysum[j]) for j in range(self.n)]),
                     sense      = "L",
                     rhs        = float(np.sum([fol_ysum[j]*self.c[j] for j in range(self.n)])))

            self.no_lazy_cuts += 1
        # else:
            # print("Bilevel Feasible")
            # print("Cur Follower objective:",curr_obj)
            # print("Opt Follower objective:",fol_obj)
