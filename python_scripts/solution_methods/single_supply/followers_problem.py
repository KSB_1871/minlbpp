"""
Single Supply Followers Problem
"""

import cplex
import numpy as np
import pandas as pd

from cplex.callbacks import NodeCallback

class follower_node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1

class FOL_PROB():

    def __init__(self,n,i,d,A,b,c,supply,show_sol=0):

        """
        Inputs:

        n   Number of commodities
        i   Number of follower groups
        d   Number of followers in each group
        A   Constraints matrix for all followers
        b   RHS of constraints for all followers
        c   Cost assigned to every commodity (x+c if in callback)
        """
        self.n,self.i,self.d,self.A,self.b,self.c,self.supply = n,i,d,A,b,c,supply

        self.fol_prob = cplex.Cplex()
        self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)

        self.fol_prob_settings()

        self.add_y_variables()
        self.group_constraints()
        self.fol_supply_constraints()

        self.node_callback = self.fol_prob.register_callback(follower_node_callback)
        self.node_callback.node_count = 0

    def fol_prob_settings(self):
        
        self.fol_prob.set_log_stream(None)
        self.fol_prob.set_error_stream(None)
        self.fol_prob.set_warning_stream(None)
        self.fol_prob.set_results_stream(None)
        self.fol_prob.parameters.mip.display.set(0)

    def ksb_solve_followers_problem(self,ksb_sols):

        # print("Follower problem")

        # Add any constraints for solutions already found
        for ksb_sol in ksb_sols:

            self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = ksb_sol[0],
                                                                              val = [1] * len(ksb_sol[0]))],
                                                 senses   = ["L"],
                                                 rhs      = [float(ksb_sol[1] - 1)])

        # Solve the problem
        self.fol_prob.solve()

        LB_names = []
        RB_names = []

        for j in range(self.n):
            for i in range(self.i):
                for k in range(self.d[i]):
                    if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) >= 0.5:
                        LB_names += [self.fol_y_names[i][k][j]]
                        RB_names += [self.fol_y_names[ii][kk][j] for ii in range(self.i) \
                                                               for kk in range(self.d[i])]

        return LB_names, RB_names, self.node_callback.node_count

    def lambda_solve_followers_problem(self,lam_sols_names, lam_neq_names):

        for idx,lam_neq_sol in enumerate(lam_neq_names):

            print("idx",idx)
            
            print("L",lam_neq_sol)
            self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = lam_neq_sol,
                                                                              val = [1] * len(lam_neq_sol))],
                                                 senses   = ["L"],
                                                 rhs      = [float(len(lam_sols_names[idx]) - 1)])

        # Solve the problem
        self.fol_prob.solve()
        print("FOL OBJ:",self.fol_prob.solution.get_objective_value())

        lam_sol_val = [[self.fol_prob.solution.get_values(self.fol_y_names[i][k]) for k in range(self.d[i])] for i in range(self.i)]
        lam_sol_names = [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) >= 0.5]

        lam_neq_vals = []
        lam_neq_names = []

        for j in range(self.n):
            for i in range(self.i):
                for k in range(self.d[i]):
                    if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) >= 0.5:
                        lam_neq_names += [self.fol_y_names[ii][kk][j] for ii in range(self.i) for kk in range(self.d[i])]

        return self.fol_prob.solution.get_objective_value(),\
               lam_sol_val,\
               lam_sol_names,\
               lam_neq_vals,\
               lam_neq_names,\
               self.node_callback.node_count

    def solve_followers_problem(self,sol_pool_sol=False):

        # print("\n")
        # print("Follower problem")
        # print("Cost",self.c)
        self.fol_prob.solve()

        if sol_pool_sol == False:

            return self.fol_prob.solution.get_objective_value(),\
                    [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) \
                                                                                        for k in range(self.d[i])]) \
                                                                                        for j in range(self.n)],\
                    self.node_callback.node_count

        else:

            LB_names = []
            RB_names = []

            for j in range(self.n):
                for i in range(self.i):
                    for k in range(self.d[i]):
                        if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) >= 0.5:
                            LB_names += [self.fol_y_names[i][k][j]]
                            RB_names += [self.fol_y_names[ii][kk][j] for ii in range(self.i) \
                                                                for kk in range(self.d[i])]

            return self.fol_prob.solution.get_objective_value(),\
                    [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) \
                                                                                        for k in range(self.d[i])]) \
                                                                                        for j in range(self.n)],\
                    self.node_callback.node_count,\
                    LB_names,\
                    RB_names




    def add_y_variables(self):

        self.fol_y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        # # Add variables
        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj= [self.c[j] for j in range(self.n)],
                                lb = [0] * len(self.fol_y_names[i][k]),
                                ub = [1] * len(self.fol_y_names[i][k]),
                                types = "I" * len(self.fol_y_names[i][k]),
                                names = self.fol_y_names[i][k])

    def group_constraints(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(np.shape(self.A[i])[0]):
                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.fol_y_names[i][k],
                                                                                      val=list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = [self.b[i][j]])

    def fol_supply_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                            val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                                 senses  =["L"],
                                                 rhs     =[float(self.supply[j])])