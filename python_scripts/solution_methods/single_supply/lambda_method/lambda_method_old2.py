"""
Single Supply

Lambda Method
"""

import cplex
import numpy as np
import pandas as pd
import time

from solution_methods.single_supply.followers_problem import FOL_PROB
from solution_methods.single_supply.cutting_plane.cutting_plane_method import cutting_plane_method 

from useful_functions.useful_functions import mccormick_constraints
from cplex.callbacks import BranchCallback
from cplex.callbacks import  NodeCallback

class standard_branch_callback(BranchCallback):
    def __call__(self):

        self.branch_count += 1
        self.node_count += self.get_num_branches()

class Node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1

class lambda_method:

    def __init__(self,master):

        print("\n")
        print("Lambda Method")

        self.lam_solution = 0
        self.mip_gap = -1
        self.fol_node_count = 0
        self.node_node_count = 0
        self.branch_node_count = 0
        self.branch_branch_count = 0
        self.bi_feas = False
        self.Y = []
        self.lambda_time_flag = False

        # Get initial Followers Solution
        fol_prob = FOL_PROB(master.n,master.i,master.d,master.A,master.b,master.c,master.supply,0)
        fol_obj, fol_sol, node_count = fol_prob.solve_followers_problem()

        self.Y.append(fol_sol)
        self.fol_node_count += node_count
        self.no_lambda_sols = len(self.Y)

    def create_problem(self,master):

        self.lam_prob = cplex.Cplex()
        self.lam_prob.objective.set_sense(self.lam_prob.objective.sense.maximize)
        self.lam_prob.parameters.timelimit.set(np.max([0,3600 - (time.time() - master.start_time)]))

        self.lam_prob.set_log_stream(None)
        self.lam_prob.set_error_stream(None)
        self.lam_prob.set_warning_stream(None)
        self.lam_prob.set_results_stream(None)
        self.lam_prob.parameters.mip.display.set(0)

        self.add_vars(master)
        self.add_constraints(master)
        self.add_callbacks(master)

    def add_vars(self,master):

        self.add_x_vars(master)
        self.add_xbar_vars(master)
        self.add_lambda_vars(master)
        self.add_p_vars(master)
        self.add_q_vars(master)

        if master.solution_method in [4,6]:

            self.add_mu_vars(master)
            self.add_nu_vars(master)     

    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.sum_lambda_constraint(master)


        for l in range(len(self.Y)):
            for j in range(master.n):

                mccormick_constraints(self.lam_prob,\
                                        (self.lambda_names[l],0,1),\
                                           (self.x_names[j],0,master.M),\
                                           self.p_names[l][j])

                mccormick_constraints(self.lam_prob,\
                                           (self.lambda_names[l],0,1),\
                                           (self.xbar_names[j],0,1),\
                                           self.q_names[l][j])

        if master.solution_method in [3,5]:

            self.value_func_constraint(master)

        elif master.solution_method in [4,6]:

            self.KKT_stationary_constraints(master)
            self.KKT_comp_slack_constraints(master)

    def add_callbacks(self,master):

        self.branch_callback = self.lam_prob.register_callback(standard_branch_callback)
        self.branch_callback.node_count = self.branch_node_count
        self.branch_callback.branch_count = self.branch_branch_count

        self.node_callback = self.lam_prob.register_callback(Node_callback)
        self.node_callback.node_count = self.node_node_count

    def solve(self,master):

        while True:
            print("\n")
            print("Lambda Size:",len(self.Y))

            self.create_problem(master)
            self.lam_prob.solve()

            try:
                self.lam_solution = self.lam_prob.solution.get_objective_value()
            except:
                self.lam_solution = -1
            
            try:
                self.mip_gap      = self.lam_prob.solution.MIP.get_mip_relative_gap()
            except:
                self.mip_gap      = -1

            try:
                print("Lam",self.lam_prob.solution.get_values(self.lambda_names))

                # Solve followers problem
                fol_prob = FOL_PROB(master.n,master.i,master.d,master.A,master.b,np.array(self.lam_prob.solution.get_values(self.x_names)) + np.array(master.c),master.supply,0)
                fol_obj, fol_sol, node_count = fol_prob.solve_followers_problem()
                cur_obj = np.array(self.lam_prob.solution.get_values(self.lambda_names)) * \
                            np.array([np.sum(np.array(self.Y[o])*(np.array(self.lam_prob.solution.get_values(self.x_names)) + np.array(master.c))) for o in range(len(self.Y))])

                # Append necessary parameters
                self.fol_node_count += node_count

                print("Current Fol Objective:",cur_obj)
                print("New Fol Objective    :",fol_obj)

                IND = [self.p_names[l][j] for l in range(len(self.Y)) for j in range(master.n)]
                VAL = [self.Y[l][j]  for l in range(len(self.Y)) for j in range(master.n)]

                IND += [self.lambda_names[l] for l in range(len(self.Y))]
                VAL += [np.sum(np.array(self.Y[l])*np.array(master.c)) for l in range(len(self.Y))]

                if np.abs(max(cur_obj) - fol_obj) >= 0.1:
                    print("Bilevel Infeasible")
                    self.get_results(master)
                    self.Y.append(fol_sol)

                else:
                    print("Bilevel Feasible")
                    self.bi_feas = True
                    self.get_results(master)
                    break
            except:
                print("Out of Time with Lambda")
                self.get_results(master)
                self.lambda_time_flag = True
                break

        if self.lambda_time_flag == False:
            print("Objective:",self.lam_prob.solution.get_objective_value())

            # TODO
            cpm = cutting_plane_method(master)

            cpm.not_in_Y_constraints(master,self.Y)
            cpm.minimum_objective(master,self.lam_prob.solution.get_objective_value())
            cpm_results = cpm.solve(master)

            self.merge_cpm_results(master,cpm_results)

        

        return self.results

    # Variables
    def add_x_vars(self,master):

        self.x_names = ["x_" + str(i) for i in range(master.n)]

        # Add variables
        self.lam_prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(i) for i in range(master.n)]

        # Add variables
        self.lam_prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [1] * master.n,
                                  types = "B" * master.n,
                                  names = self.xbar_names)

    def add_lambda_vars(self,master):

        if master.solution_method in [3,5]:

            self.lambda_names = ["lam_" + str(l) for l in range(len(self.Y))]

            for l in range(len(self.Y)):
                self.lam_prob.variables.add(obj   = [0],#[np.sum(np.array(self.Y[l])*np.array(master.c))],
                                        lb    = [0],
                                        ub    = [1],
                                        types = "B",
                                        names = [self.lambda_names[l]])

        elif master.solution_method in [4,6]:

            self.lambda_names = ["lam_" + str(l) for l in range(len(self.Y))]

            for l in range(len(self.Y)):
                self.lam_prob.variables.add(obj   = [0],#[np.sum(np.array(self.Y[l])*np.array(master.c))],
                                        lb    = [0],
                                        ub    = [1],
                                        types = "B",
                                        names = [self.lambda_names[l]])

    def add_p_vars(self,master):

        """
        p_{lj} = \lambda_l * x_j
        """
        self.p_names = [["p_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(len(self.Y))]

        for l in range(len(self.Y)):
            self.lam_prob.variables.add(obj   = self.Y[l],
                                      lb    = [0] * master.n,
                                      ub    = [master.M] * master.n,
                                      types = "C" * master.n,
                                      names = self.p_names[l])

    def add_q_vars(self,master):

        """
        q_{lj} = \lambda_l * \bar{x}_j
        """
        self.q_names = [["q_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(len(self.Y))]

        for l in range(len(self.Y)):
            self.lam_prob.variables.add(obj   = [float(self.Y[l][j]*master.c[j]) for j in range(master.n)],
                                      lb    = [0] * master.n,
                                      ub    = [master.M] * master.n,
                                      types = "C" * master.n,
                                      names = self.q_names[l])
    
    def add_mu_vars(self,master):

        self.mu_names = ["mu_" + str(sol) for sol in range(len(self.Y))]

        self.lam_prob.variables.add(obj   = [0] * len(self.Y),
                                    lb    = [0] * len(self.Y),
                                    ub    = [np.inf] * len(self.Y),
                                    types = "C" * len(self.Y),
                                    names = self.mu_names)

    def add_nu_vars(self,master):

        self.nu_names = ["nu_0"]

        self.lam_prob.variables.add(obj   = [0],
                                    lb    = [-np.inf],
                                    ub    = [np.inf],
                                    types = "C",
                                    names = self.nu_names)

    # Constraints
    def budget_constraint(self,master):

        self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                           senses   = ["L"],
                                           rhs      = [master.B],
                                           names    = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def sum_lambda_constraint(self,master):

        self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.lambda_names,
                                                                          val = [1] * len(self.lambda_names))],
                                             senses   = ["E"],
                                             rhs      = [1])

    def value_func_constraint(self,master):

        for sol in range(len(self.Y)):

            self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.p_names[o][j] for o in range(len(self.Y)) for j in range(master.n)] + \
                                                                                    [self.lambda_names[o] for o in range(len(self.Y))] + \
                                                                                    self.x_names,
                                                                             val = [self.Y[o][j] for o in range(len(self.Y)) for j in range(master.n)] + \
                                                                                    [np.sum(np.array(self.Y[o])*np.array(master.c)) for o in range(len(self.Y))] + \
                                                                                    [-self.Y[sol][j] for j in range(master.n)])],
                                                 senses   = ["L"],
                                                 rhs      = [np.sum(np.array(self.Y[sol]) * np.array(master.c))])
        
    def KKT_stationary_constraints(self,master):

        for sol in range(len(self.Y)):

            self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.x_names + \
                                                                                    [self.mu_names[sol],self.nu_names[0]],
                                                                              val = self.Y[sol] + \
                                                                                    [-1,1])],
                                                 senses   = ["E"],
                                                 rhs      = [-float(np.sum(np.array(self.Y[sol])*np.array(master.c)))])

    def KKT_comp_slack_constraints(self,master):

        for sol in range(len(self.Y)):
            
            self.lam_prob.SOS.add(type = "1",
                                  SOS  = [[self.mu_names[sol],self.lambda_names[sol]],[1,2]])

    # Post processing
    def get_results(self,master):

        self.results = [["Status",          self.lam_prob.solution.get_status()],
                    ["Obj",             self.lam_solution],
                    ["Bi_Feas",         self.bi_feas],
                    ["MIP_Gap",         self.mip_gap],
                    ["No_Lazy_Calls",   0],
                    ["No_Lazy_Cuts",    0],
                    ["No_Nodes",        self.node_callback.node_count],
                    ["No_Branch_Nodes", self.branch_callback.node_count],
                    ["Fol_Node_Count",  self.fol_node_count],
                    ["No_KSB_Sols",     0],
                    ["No_Lambda_Sols",  len(self.Y)]]

    def merge_cpm_results(self,master,cpm_results):

        if cpm_results[2][1] == True:
            # Bilevel feasible solution found in CPM
            self.results = [["Status",          cpm_results[0][1]],
                            ["Obj",             cpm_results[1][1]],
                            ["Bi_Feas",         cpm_results[2][1]],
                            ["MIP_Gap",         cpm_results[3][1]],
                            ["No_Lazy_Calls",   cpm_results[4][1]],
                            ["No_Lazy_Cuts",    cpm_results[5][1]],
                            ["No_Nodes",        cpm_results[6][1] + self.node_callback.node_count],
                            ["No_Branch_Nodes", cpm_results[7][1] + self.branch_callback.node_count],
                            ["Fol_Node_Count",  cpm_results[8][1] + self.fol_node_count],
                            ["No_KSB_Sols",     cpm_results[9][1]],
                            ["No_Lambda_Sols",  len(self.Y)]]

        else:
            # No Bilevel Feasible solution found in CPM
            self.results = [["Status",          cpm_results[0][1]],
                            ["Obj",             self.lam_solution],
                            ["Bi_Feas",         self.bi_feas],
                            ["MIP_Gap",         cpm_results[3][1]],
                            ["No_Lazy_Calls",   cpm_results[4][1]],
                            ["No_Lazy_Cuts",    cpm_results[5][1]],
                            ["No_Nodes",        cpm_results[6][1] + self.node_callback.node_count],
                            ["No_Branch_Nodes", cpm_results[7][1] + self.branch_callback.node_count],
                            ["Fol_Node_Count",  cpm_results[8][1] + self.fol_node_count],
                            ["No_KSB_Sols",     cpm_results[9][1]],
                            ["No_Lambda_Sols",  len(self.Y)]]
           
















