"""
Lambda Branch Callback
"""
from cplex.callbacks import BranchCallback

class standard_branch_callback(BranchCallback):
    def __call__(self):

        self.branch_count += 1
        self.node_count += self.get_num_branches()