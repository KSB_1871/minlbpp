"""
Single Supply Lambda method
"""
import cplex
import numpy as np
import pandas as pd

from solution_methods.single_supply.followers_problem import FOL_PROB
from solution_methods.single_supply.cutting_plane.cutting_plane_method import cutting_plane_method 
from solution_methods.single_supply.lambda_method.branch_callback import standard_branch_callback
from solution_methods.single_supply.lambda_method.node_callback import Node_callback

from useful_functions.useful_functions import mccormick_constraints

class lambda_method():

    def __init__(self,master):

        print("\n")
        print("Lambda Method")

        self.sol = 0
        self.mip_gap = -1
        self.fol_node_count = 0
        self.node_node_count = 0
        self.branch_node_count = 0
        self.bi_feas = False

        self.Y = []

        # Get initial Followers Solution
        fol_prob = FOL_PROB(master.n,master.i,master.d,master.A,master.b,master.c,master.supply,0)
        fol_obj, fol_sol, node_count = fol_prob.solve_followers_problem()

        self.Y.append(fol_sol)

        self.fol_node_count += node_count
        self.no_lambda_sols = len(self.Y)

    def create_problem(self,master):

        self.lam_prob = cplex.Cplex()
        self.lam_prob.objective.set_sense(self.lam_prob.objective.sense.maximize)
        self.lam_prob.parameters.timelimit.set(3600.0)

        self.lam_prob.set_log_stream(None)
        self.lam_prob.set_error_stream(None)
        self.lam_prob.set_warning_stream(None)
        self.lam_prob.set_results_stream(None)
        self.lam_prob.parameters.mip.display.set(0)

        self.add_vars(master)
        self.add_constraints(master)
        self.add_callbacks(master)

    def add_vars(self,master):

        self.add_x_vars(master)
        self.add_xbar_vars(master)
        self.add_lambda(master)
        self.add_p_vars(master)
        self.add_q_vars(master)

    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)

        if master.solution_method in [3,6]:
            """ Value function Constraints """
            
            self.lambda_sum_constraints(master)
            self.value_func_constraint(master)

        elif master.solution_method in [4,7]:
            """ KKT Constraints """

            self.KKT_constraints(master)

        for l in range(len(self.Y)):
            for j in range(master.n):

                mccormick_constraints(self.lam_prob,\
                                        (self.lambda_names[l],0,1),\
                                           (self.x_names[j],0,master.M),\
                                           self.p_names[l][j])

                mccormick_constraints(self.lam_prob,\
                                           (self.lambda_names[l],0,1),\
                                           (self.xbar_names[j],0,1),\
                                           self.q_names[l][j])

    def add_callbacks(self,master):

        self.register_node_callback(master)
        self.register_branch_callback(master)

    def register_node_callback(self,master):

        self.node_callback = self.lam_prob.register_callback(Node_callback)

        self.node_callback.node_count = 0

    def register_branch_callback(self,master):

        self.branch_callback = self.lam_prob.register_callback(standard_branch_callback)

        self.branch_callback.node_count = 0
        self.branch_callback.branch_count = 0

    def solve(self,master):

        self.create_problem(master)

        while True:
            print("\n")
            print("Lambda_Size:",len(self.Y))

            self.lam_prob.solve()

            print("Alpha  :",self.lam_prob.solution.get_values(self.alpha_names))
            print("DLV    :",self.lam_prob.solution.get_values(self.dummy_lambda_sum_names))

            self.sol = self.lam_prob.solution.get_objective_value()

            self.node_node_count    += self.node_callback.node_count
            self.branch_node_count  += self.branch_callback.node_count

            curr_obj = 0

            for l in range(len(self.Y)):
                if self.lam_prob.solution.get_values(self.lambda_names[l]) >= 0.5:
                    opt_l = l
                    curr_obj = np.sum(np.array(self.Y[l]) * (np.array(master.c) + np.array(self.lam_prob.solution.get_values(self.x_names))))
            
            fol_prob = FOL_PROB(master.n,
                                master.i,
                                master.d,
                                master.A,
                                master.b,
                                np.array(self.lam_prob.solution.get_values(self.x_names)) + np.array(master.c),
                                master.supply,
                                0)

            fol_obj, fol_sol, fol_nodes = fol_prob.solve_followers_problem()

            self.fol_node_count     += fol_nodes


            print("\n")
            print("Leader Objective       :",self.lam_prob.solution.get_objective_value())
            print("Follower Objective     :",curr_obj)
            print("New Follower Objective :",fol_obj)
            print("Fol Nodes              :",fol_nodes)

            if fol_obj <= curr_obj - 0.5:
                print("Bilevel Infeasible")

                if fol_sol in self.Y:
                    print("Repeated solution")
                    print(self.Y.index(fol_sol))
                    print(hi)

                self.Y.append(fol_sol)

                self.create_problem(master)
                

            else:
                self.bi_feas = True
                

            results = [["Status",          self.lam_prob.solution.get_status()],
                    ["Obj",             self.sol],
                    ["Bi_Feas",         self.bi_feas],
                    ["MIP_Gap",         0],#mip_gap],
                    ["No_Lazy_Calls",   0],#self.lazy_callback.no_lazy_callbacks],
                    ["No_Lazy_Cuts",    0],#self.lazy_callback.no_lazy_cuts],
                    ["No_Nodes",        self.node_node_count],
                    ["No_Branch_Nodes", self.branch_node_count],
                    ["Fol_Node_Count",  self.fol_node_count],
                    ["No_KSB_Sols",     0]]#len(self.branch_callback.ksb_sols_index)]])

            print(results)

            if self.bi_feas == True:
                break


        self.display_solution(master)

        print("\n")
        print("Lambda Finished")
        print("Objective:",self.lam_prob.solution.get_objective_value())



        # Now we must solve the not in Y problem
        if master.solution_method == 3:
            """Cutting plane method"""
            cpm = cutting_plane_method(master)
            cpm.minimum_objective(master,self.lam_prob.solution.get_objective_value())
            cpm.not_in_Y_constraints(master,self.Y)
            results2 = cpm.solve(master)
        else:
            """Cutting plane method with KSB"""
            print(self.Y)

            for y in self.Y:
                y_list = []
                y_len = np.sum(y)
                for j in range(master.n):
                    if y[j] == 1:
                        
                        counter = 0

                        for i in range(master.i):
                            for k in range(master.d[i]):
                                y_list += [j + counter]
                                counter += master.n

                y_tuple = (y_list,y_len)
                master.ksb_sols_index.append(y_tuple)

            # print(master.ksb_sols_index)
            # print(hi)

            cpm = cutting_plane_method(master)
            cpm.minimum_objective(master,self.lam_prob.solution.get_objective_value())
            cpm.not_in_Y_constraints(master,self.Y)
            results2 = cpm.solve(master)
        
        results = [["Status",          self.lam_prob.solution.get_status()],
                ["Obj",             max(self.sol,results2[1][1])],
                ["Bi_Feas",         self.bi_feas],
                ["MIP_Gap",         results2[3][1] if results2[3][1] >= 0 else 0],
                ["No_Lazy_Calls",   results2[4][1]],#self.lazy_callback.no_lazy_callbacks],
                ["No_Lazy_Cuts",    results2[5][1]],#self.lazy_callback.no_lazy_cuts],
                ["No_Nodes",        self.node_node_count+results2[6][1]],
                ["No_Branch_Nodes", self.branch_node_count+results2[7][1]],
                ["Fol_Node_Count",  self.fol_node_count+results2[8][1]],
                ["No_KSB_Sols",     results2[9][1]]]#len(self.branch_callback.ksb_sols_index)]])


        print("3",results)

        return results


    # Variables

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(i) for i in range(master.n)]

        # Add variables
        self.lam_prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(i) for i in range(master.n)]

        # Add variables
        self.lam_prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [1] * master.n,
                                  types = "B" * master.n,
                                  names = self.xbar_names)
 
    def add_lambda(self,master):

        self.lambda_names = ["lam_" + str(l) for l in range(len(self.Y))]

        for l in range(len(self.Y)):
            self.lam_prob.variables.add(obj   = [0],#[np.sum(np.array(self.Y[l])*np.array(master.c))],
                                    lb    = [0],
                                    ub    = [1],
                                    types = "B",
                                    names = [self.lambda_names[l]])

    def add_p_vars(self,master):

        """
        p_{lj} = \lambda_l * x_j
        """
        self.p_names = [["p_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(len(self.Y))]

        for l in range(len(self.Y)):
            self.lam_prob.variables.add(obj   = self.Y[l],
                                      lb    = [0] * master.n,
                                      ub    = [master.M] * master.n,
                                      types = "C" * master.n,
                                      names = self.p_names[l])

    def add_q_vars(self,master):

        """
        q_{lj} = \lambda_l * \bar{x}_j
        """
        self.q_names = [["q_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(len(self.Y))]

        for l in range(len(self.Y)):
            self.lam_prob.variables.add(obj   = [float(self.Y[l][j]*master.c[j]) for j in range(master.n)],
                                      lb    = [0] * master.n,
                                      ub    = [master.M] * master.n,
                                      types = "C" * master.n,
                                      names = self.q_names[l])

    def add_alpha_vars(self,master):

        self.alpha_names = ["alpha"]

        self.lam_prob.variables.add(obj   = [0],
                                    lb    = [-np.inf],
                                    ub    = [np.inf],
                                    types = "C",
                                    names = self.alpha_names)

    def add_dummy_lambda_sum_variable(self,master):

        self.dummy_lambda_sum_names = ["dls"]

        self.lam_prob.variables.add(obj   = [0],
                                    lb    = [0],
                                    ub    = [float(len(self.Y))],
                                    types = "C",
                                    names = self.dummy_lambda_sum_names)

    # Constraints

    def budget_constraint(self,master):

        self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                           senses   = ["L"],
                                           rhs      = [master.B],
                                           names    = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def lambda_sum_constraints(self,master):

        self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.lambda_names,
                                                                        val=[1] * len(self.lambda_names))],
                                           senses   = ["E"],
                                           rhs      = [1],
                                           names    = ["Lambda_sum"])

    def value_func_constraint(self,master):

        for o in range(len(self.Y)):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.p_names[l][j] for l in range(len(self.Y)) for j in range(master.n)]
            VAL += [self.Y[l][j]  for l in range(len(self.Y)) for j in range(master.n)]

            IND += [self.lambda_names[l] for l in range(len(self.Y))]
            VAL += [np.sum(np.array(self.Y[l])*np.array(master.c)) for l in range(len(self.Y))]

            IND += self.x_names
            VAL += [-self.Y[o][j] for j in range(master.n)]

            RHS = float(np.sum(np.array(self.Y[o])*np.array(master.c)))

            self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=IND,
                                                                            val=VAL)],
                                               senses   = ["L"],
                                               rhs      = [RHS],
                                               names    = ['VF_' + str(o)])

    def dummy_lambda_constraints(self,master):

        self.lam_prob.variables.add(lin_expr = [cplex.SparsePair(ind = self.lambda_names + self.dummy_lambda_sum_names,
                                                                 val = [1 for i in range(len(self.lambda_names))] + [-1])],
                                    senses   = ["E"],
                                    rhs      = [1])

    def KKT_constraints(self,master):

        self.add_alpha_vars(master)
        self.add_dummy_lambda_sum_variable(master)

        self.lam_prob.solve()

        for o in range(len(self.Y)):
            self.lam_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.x_names + self.alpha_names,
                                                                              val = self.Y[o] + [1])],
                                                 senses   = ["E"],
                                                 rhs      = [-np.sum(np.array(self.Y[o])*np.array(master.c))])

            self.lam_prob.SOS.add(type="1",SOS=[self.alpha_names + self.dummy_lambda_sum_names,[1,2]])


    def display_solution(self,master):

        # print(np.array([master.prob.solution.get_values(self.z_names[l]) for l in range(len(self.Y))]))

        vstack = np.vstack((master.supply,
                            self.lam_prob.solution.get_values(self.xbar_names),
                            self.lam_prob.solution.get_values(self.x_names),
                            np.array([self.lam_prob.solution.get_values(self.p_names[l]) for l in range(len(self.Y))]))).T
            # master.prob.solution.get_values(self.z_names),
            # master.c,
            # [np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i)\
            #                                                                 for k in range(master.d[i])])\
            #                                                                 for j in range(master.n)],
            # master.prob.solution.get_values(self.u_names))).T

        df = pd.DataFrame(vstack,columns=["S","XBAR","X",] + ["Z" + str(l) for l in range(len(self.Y))])#"Z","C","YSUM","U"])
        print("\n")
        print("Solution")
        print("Objective Value:",self.lam_prob.solution.get_objective_value())
        print("Lambda Values  :",self.lam_prob.solution.get_values(self.lambda_names))
        print("Lambda Costs   :",[np.sum(np.array(self.Y[l])*(np.array(self.lam_prob.solution.get_values(self.x_names)) + np.array(master.c))) for l in range(len(self.Y))])
        print(df)

    