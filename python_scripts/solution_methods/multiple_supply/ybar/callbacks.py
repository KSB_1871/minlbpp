"""
Incumbent Callback
"""
import numpy as np
import time
import pandas as pd
from cplex.callbacks import  IncumbentCallback,NodeCallback,BranchCallback
from solution_methods.multiple_supply.followers_problem import MS_FOL_PROB

class IncCallback(IncumbentCallback):

    def __call__(self):

        if self.get_node_data() != None \
            and self.get_node_data()[0][2] == 2:

            self.reject()

        elif self.get_node_data() == None \
            or self.get_node_data()[0][2] == 1 \
            or self.get_node_data()[-1][0] == "R":

            fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,
                                    self.get_values(self.x_names),
                                    self.get_values(self.xbar_names),
                                    self.c,
                                    self.supply)

            self.no_fol_prob_solves += 1

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()

            cur_obj = np.sum(np.array([np.sum([self.get_values(self.ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * (np.array(self.get_values(self.x_names)) + np.array(self.c))) + \
                        np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * np.array(self.c))

            if fol_obj <= cur_obj - 0.1:
                print("Bilevel Infeasible",fol_obj,"<=",cur_obj)
                # print(np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)])*np.array(self.c)))
                self.set_node_data((("S",0,2,""),
                                    np.array(fol_ybar_sol),
                                    np.array(fol_y_sol)))
                self.reject()

                vstack = np.vstack((fol_ybar_sol,fol_y_sol)).T
                df = pd.DataFrame(vstack,columns=["YBAR","Y"])

                # print(df)

            else:
                print("Bilevel Feasible")
                self.bi_feas = True

class ksb_incumbent_callback(IncumbentCallback):

    def __call__(self):

        # print("Incumbent",self.get_node_data())

        if self.get_node_data() == None:
            # Either at the root node or an uninteresting node.
            # Solve followers problem and envoke delta branching if necessary

            self.feasibility_check()

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 1):
            # We are on the left side of a delta branching node
            # So we check for bilevel feasibility

            self.feasibility_check()
    

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 2):
            # We are on the right side of a delta branching node, so ignore the incumbent

            self.reject()

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "L"):
            # Left KSB branch. Check feasibility

            self.feasibility_check()
        
        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R"):
            # Left KSB branch. Check feasibility

            self.feasibility_check()

    def feasibility_check(self):

        fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,
                                self.get_values(self.x_names),
                                self.get_values(self.xbar_names),
                                self.c,
                                self.supply)

        self.no_fol_prob_solves += 1

        fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()

        cur_obj = np.sum(np.array([np.sum([self.get_values(self.ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * (np.array(self.get_values(self.x_names)) + np.array(self.c))) + \
                    np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * np.array(self.c))

        if fol_obj <= cur_obj - 0.1:
            print("Bilevel Infeasible",fol_obj,"<=",cur_obj)
            # print(np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)])*np.array(self.c)))
            if self.get_node_data() != None and \
                (self.get_node_data()[0][2] == "R"):

                self.reject()

            else:

                self.set_node_data((("S",0,2,""),
                                    np.array(fol_ybar_sol),
                                    np.array(fol_y_sol)))
                
                self.reject()

            vstack = np.vstack((fol_ybar_sol,fol_y_sol)).T
            df = pd.DataFrame(vstack,columns=["YBAR","Y"])

            # print(df)

        else:
            print("Bilevel Feasible at ",self.get_node_data())
            self.bi_feas = True

class node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1

class branch_callback(BranchCallback):

    def __call__(self):

        self.node_count += self.get_num_branches()

class delta_ybar_dmcc_branch_callback(BranchCallback):

    def __call__(self):

        if self.get_node_data() == None \
            or self.get_node_data()[0][2] == 1:

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i,node_data=self.get_node_data())

        elif self.get_node_data()[0][1] <= (self.n - 1):

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = None)
                                                    # ("S",self.get_node_data()[0][1]+1,1),
                                                    # self.get_node_data()[1],
                                                    # self.get_node_data()[2],
                                                    # ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] <= (2*self.n - 2):

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] == (2*self.n - 1):
                            
            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        self.node_count += self.get_num_branches()

class delta_ybar_dmcc_branch_callback_plus(BranchCallback):

    def __call__(self):

        # Fast forward to commodity which is being purchased
        hunting = True

        if self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 2):
        
            while hunting == True:

                if self.get_node_data()[0][1] <= (self.n - 1):

                    if self.get_node_data()[1][self.get_node_data()[0][1]] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        # Break
                        if np.max(self.get_node_data()[2]) <= 0.5:

                            max_after = 0

                            for idx in range(self.get_node_data()[0][1],self.n-1):

                                if self.get_node_data()[2][idx] > max_after:

                                    max_after = self.get_node_data()[2][idx-self.n]

                            if max_after >= 0.5:

                                pass

                            else:

                                node_data = list(self.get_node_data())
                                node_data[0] = (self.get_node_data()[0][0],
                                                            self.get_node_data()[0][1],
                                                            self.get_node_data()[0][2],
                                                            "CP")

                                self.set_node_data(tuple(node_data))

                        hunting = False

                elif self.get_node_data()[0][1] <= (2*self.n - 1):

                    if self.get_node_data()[2][self.get_node_data()[0][1]-self.n] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        max_after = 0

                        for idx in range(self.get_node_data()[0][1]+1,2*self.n-1):
                            
                            if self.get_node_data()[2][idx-self.n] > max_after:

                                max_after = self.get_node_data()[2][idx-self.n]

                        if max_after >= 0.5:

                            pass

                        else:

                            node_data = list(self.get_node_data())
                            node_data[0] = (self.get_node_data()[0][0],
                                                        self.get_node_data()[0][1],
                                                        self.get_node_data()[0][2],
                                                        "CP")

                            self.set_node_data(tuple(node_data))

                        # Break                      
                        hunting = False  

        if self.get_node_data() == None \
            or self.get_node_data()[0][2] == 1:

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i,node_data=self.get_node_data())

        elif self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == "":


            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = None)
                                                    # ("S",self.get_node_data()[0][1]+1,1),
                                                    # self.get_node_data()[1],
                                                    # self.get_node_data()[2],
                                                    # ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == "CP":

            print(hi)

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1])],[1]],"L",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])])],
                            node_data          = None)

        elif self.get_node_data()[0][1] <= (2*self.n - 2) and self.get_node_data()[0][3] == "":

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] <= (2*self.n - 2) and self.get_node_data()[0][3] == "CP":

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        elif self.get_node_data()[0][1] == (2*self.n - 1):
                            
            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        self.node_count += self.get_num_branches()

class delta_ybar_dmcc_ksb_branch_callback(BranchCallback):

    def __call__(self):

        if self.node_count == 0:
            # We are at the root node and must KSB
            # print("Branching:1")
            if self.Y_init_size == 1:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = None)

                self.sol_count += 1
                self.fol_prob_nodes += nodes


            else:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = (("KSB",self.sol_count,"R"),None,None))

                self.sol_count += 1
                self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "L"):
            # We are on a left KSB branch. Branch as normal
            # print("Branching:2")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count < self.Y_init_size):
            # We are on a right KSB branch and have not reached the Y_init_size. Create new KSB branch
            LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

            self.Y_init.append(sorted(LB_idx))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                            node_data          = (("KSB",self.sol_count,"L"),None,None))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                            node_data          = (("KSB",self.sol_count,"R"),None,None))

            self.sol_count += 1
            self.fol_prob_nodes += nodes


        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count == self.Y_init_size):
            # We are on a right KSB branch and have reached the Y_init_size. Carry on as normal
            # print("Branching:4")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())


        elif self.get_node_data() == None or \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 1):
            # At a normal branch, so carry on
            # print("Branching:5")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1)):
            # First half of delta branching and on rhs, so carry on with delta
            # print("Branching:6")

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = None)
                                                    # ("S",self.get_node_data()[0][1]+1,1),
                                                    # self.get_node_data()[1],
                                                    # self.get_node_data()[2],
                                                    # ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 2)):
            # Second half of delta branching and on rhs, so carry on with delta
            # print("Branching:7")
            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] == (2*self.n - 1)):
            # Last node of delta branching, so include the value function constraint
            # print("Branching:8")
            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)


        else:
            print("Branching:9")

            print(self.get_node_data())
            print(bye)

        self.branch_count += 1
        self.node_count += self.get_num_branches()

    def generate_solutions_on_fly(self):

        fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,[0] * self.n,[0] * self.n,self.c,self.supply,0)

        LB_names, RB_names, nodes, u_values = fol_prob.ksb_solve_followers_problem(self.u_values,self.supply)

        self.ksb_sols_index.append((RB_names,len(LB_names)))
        self.no_fol_prob_solves += 1


        self.u_values.append(u_values)

        LB_idx = [self.var_list.index(LB_name) for LB_name in LB_names]
        RB_idx = [self.var_list.index(RB_name) for RB_name in RB_names]


        return LB_idx, RB_idx, nodes

class delta_ybar_dmcc_ksb_branch_callback_plus(BranchCallback):

    def __call__(self):

        # Fast forward to commodity which is being purchased
        hunting = True

        if self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 2):
        
            while hunting == True:

                if self.get_node_data()[0][1] <= (self.n - 1):

                    if self.get_node_data()[1][self.get_node_data()[0][1]] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        # Break
                        if np.max(self.get_node_data()[2]) <= 0.5:

                            max_after = 0

                            for idx in range(self.get_node_data()[0][1],self.n-1):

                                if self.get_node_data()[2][idx] > max_after:

                                    max_after = self.get_node_data()[2][idx-self.n]

                            if max_after >= 0.5:

                                pass

                            else:

                                node_data = list(self.get_node_data())
                                node_data[0] = (self.get_node_data()[0][0],
                                                            self.get_node_data()[0][1],
                                                            self.get_node_data()[0][2],
                                                            "CP")

                                self.set_node_data(tuple(node_data))

                        hunting = False

                elif self.get_node_data()[0][1] <= (2*self.n - 1):

                    if self.get_node_data()[2][self.get_node_data()[0][1]-self.n] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        max_after = 0

                        for idx in range(self.get_node_data()[0][1]+1,2*self.n-1):
                            
                            if self.get_node_data()[2][idx-self.n] > max_after:

                                max_after = self.get_node_data()[2][idx-self.n]

                        if max_after >= 0.5:

                            pass

                        else:

                            node_data = list(self.get_node_data())
                            node_data[0] = (self.get_node_data()[0][0],
                                                        self.get_node_data()[0][1],
                                                        self.get_node_data()[0][2],
                                                        "CP")

                            self.set_node_data(tuple(node_data))

                        # Break                      
                        hunting = False  

        if self.node_count == 0:
            # We are at the root node and must KSB
            # print("Branching:1")
            if self.Y_init_size == 1:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = None)

                self.sol_count += 1
                self.fol_prob_nodes += nodes


            else:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = (("KSB",self.sol_count,"R"),None,None))

                self.sol_count += 1
                self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "L"):
            # We are on a left KSB branch. Branch as normal
            # print("Branching:2")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count < self.Y_init_size):
            # We are on a right KSB branch and have not reached the Y_init_size. Create new KSB branch
            print("Branching:3")
            LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()
            print("HI")

            self.Y_init.append(sorted(LB_idx))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                            node_data          = (("KSB",self.sol_count,"L"),None,None))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                            node_data          = (("KSB",self.sol_count,"R"),None,None))

            self.sol_count += 1
            self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count == self.Y_init_size):
            # We are on a right KSB branch and have reached the Y_init_size. Carry on as normal
            # print("Branching:4")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() == None or \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 1):
            # At a normal branch, so carry on
            # print("Branching:5")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == ""):
            # First half of delta branching and on rhs, so carry on with delta
            # print("Branching:6")

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = None)
                                                    # ("S",self.get_node_data()[0][1]+1,1),
                                                    # self.get_node_data()[1],
                                                    # self.get_node_data()[2],
                                                    # ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == "CP"):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1])],[1]],"L",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])])],
                            node_data          = None)


        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 1) and self.get_node_data()[0][3] == ""):
            # Second half of delta branching and on rhs, so carry on with delta
            # print("Branching:7")
            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))
        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 1) and self.get_node_data()[0][3] == "CP"):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        # elif self.get_node_data() != None and \
        #     (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] == (2*self.n - 1)):
        #     # Last node of delta branching, so include the value function constraint
        #     # print("Branching:8")
        #     IND = []
        #     VAL = []
        #     RHS = 0

        #     IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
        #     VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

        #     RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
        #     RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
        #                     node_data          = None)
        #                     # (("S",self.get_node_data()[0][1]+1,1),
        #                     #                         self.get_node_data()[1],
        #                     #                         self.get_node_data()[2],
        #                     #                         ))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([IND,VAL],"L",RHS),
        #                                             ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
        #                     node_data          = None)


        else:
            print("Branching:9")

            print(self.get_node_data())
            print(bye)

        self.branch_count += 1
        self.node_count += self.get_num_branches()

    def generate_solutions_on_fly(self):

        fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,[0] * self.n,[0] * self.n,self.c,self.supply,0)

        LB_names, RB_names, nodes, u_values = fol_prob.ksb_solve_followers_problem(self.u_values,self.supply)

        self.ksb_sols_index.append((RB_names,len(LB_names)))
        self.no_fol_prob_solves += 1
        self.u_values.append(u_values)

        LB_idx = [self.var_list.index(LB_name) for LB_name in LB_names]
        RB_idx = [self.var_list.index(RB_name) for RB_name in RB_names]


        return LB_idx, RB_idx, nodes

class delta_ybar_be_mcc_branching_callback(BranchCallback):

    def __call__(self):

        if self.get_node_data() == None \
            or self.get_node_data()[0][2] == 1:

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i,node_data=self.get_node_data())

        elif self.get_node_data()[0][1] <= (self.n - 1):

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] <= (2*self.n - 2):

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] == (2*self.n - 1):
                            
            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        self.node_count += self.get_num_branches()

class delta_ybar_be_mcc_branching_callback_plus(BranchCallback):

    def __call__(self):

        # Fast forward to commodity which is being purchased
        hunting = True

        if self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 2):
        
            while hunting == True:

                if self.get_node_data()[0][1] <= (self.n - 1):

                    if self.get_node_data()[1][self.get_node_data()[0][1]] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        # Break
                        if np.max(self.get_node_data()[2]) <= 0.5:

                            max_after = 0

                            for idx in range(self.get_node_data()[0][1],self.n-1):

                                if self.get_node_data()[2][idx] > max_after:

                                    max_after = self.get_node_data()[2][idx-self.n]

                            if max_after >= 0.5:

                                pass

                            else:

                                node_data = list(self.get_node_data())
                                node_data[0] = (self.get_node_data()[0][0],
                                                            self.get_node_data()[0][1],
                                                            self.get_node_data()[0][2],
                                                            "CP")

                                self.set_node_data(tuple(node_data))

                        hunting = False

                elif self.get_node_data()[0][1] <= (2*self.n - 1):

                    if self.get_node_data()[2][self.get_node_data()[0][1]-self.n] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        max_after = 0

                        for idx in range(self.get_node_data()[0][1]+1,2*self.n-1):
                            
                            if self.get_node_data()[2][idx-self.n] > max_after:

                                max_after = self.get_node_data()[2][idx-self.n]

                        if max_after >= 0.5:

                            pass

                        else:

                            node_data = list(self.get_node_data())
                            node_data[0] = (self.get_node_data()[0][0],
                                                        self.get_node_data()[0][1],
                                                        self.get_node_data()[0][2],
                                                        "CP")

                            self.set_node_data(tuple(node_data))

                        # Break                      
                        hunting = False  

                else:

                    print(hi)

        if self.get_node_data() == None \
            or self.get_node_data()[0][2] == 1:

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i,node_data=self.get_node_data())

        elif self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == "":

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == "CP":

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1])],[1]],"L",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])])],
                            node_data          = None)

        elif self.get_node_data()[0][1] <= (2*self.n - 1) and self.get_node_data()[0][3] == "":

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data()[0][1] <= (2*self.n - 1) and self.get_node_data()[0][3] == "CP":

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        # elif self.get_node_data()[0][1] == (2*self.n - 1):
                            
        #     IND = []
        #     VAL = []
        #     RHS = 0

        #     IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
        #     VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

        #     IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
        #     VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

        #     RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
        #     RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
        #                     node_data          = (("S",self.get_node_data()[0][1]+1,1),
        #                                             self.get_node_data()[1],
        #                                             self.get_node_data()[2],
        #                                             ))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([IND,VAL],"L",RHS),
        #                                             ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
        #                     node_data          = None)

        self.node_count += self.get_num_branches()

class delta_ybar_be_mcc_ksb_branching_callback(BranchCallback):

    def __call__(self):

        if self.node_count == 0:
            # We are at the root node and must KSB
            # print("Branching:1")
            if self.Y_init_size == 1:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = None)

                self.sol_count += 1
                self.fol_prob_nodes += nodes

            else:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = (("KSB",self.sol_count,"R"),None,None))

                self.sol_count += 1
                self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "L"):
            # We are on a left KSB branch. Branch as normal
            # print("Branching:2")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count < self.Y_init_size):
            # We are on a right KSB branch and have not reached the Y_init_size. Create new KSB branch
            print("Branching:3")
            LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

            self.Y_init.append(sorted(LB_idx))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                            node_data          = (("KSB",self.sol_count,"L"),None,None))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                            node_data          = (("KSB",self.sol_count,"R"),None,None))

            self.sol_count += 1
            self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count == self.Y_init_size):
            # We are on a right KSB branch and have reached the Y_init_size. Carry on as normal
            # print("Branching:4")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() == None or \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 1):
            # At a normal branch, so carry on
            # print("Branching:5")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1)):
            # First half of delta branching and on rhs, so carry on with delta
            # print("Branching:6")

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = None)
                                                    # ("S",self.get_node_data()[0][1]+1,1),
                                                    # self.get_node_data()[1],
                                                    # self.get_node_data()[2],
                                                    # ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 2)):
            # Second half of delta branching and on rhs, so carry on with delta
            # print("Branching:7")
            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] == (2*self.n - 1)):
                            
            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)

        else:

            print(hi)

        self.branch_count += 1
        self.node_count += self.get_num_branches()

    def generate_solutions_on_fly(self):

        fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,[0] * self.n,[0] * self.n,self.c,self.supply,0)

        LB_names, RB_names, nodes, u_values = fol_prob.ksb_solve_followers_problem(self.u_values,self.supply)

        self.ksb_sols_index.append((RB_names,len(LB_names)))
        self.no_fol_prob_solves += 1

        if u_values in self.u_values:
            print(hi)

        self.u_values.append(u_values)

        LB_idx = [self.var_list.index(LB_name) for LB_name in LB_names]
        RB_idx = [self.var_list.index(RB_name) for RB_name in RB_names]

        # print(here was last we got before i had to wash up before my parents arrived)

        return LB_idx, RB_idx, nodes

class delta_ybar_be_mcc_ksb_branching_callback_plus(BranchCallback):

    def __call__(self):

        # Fast forward to commodity which is being purchased
        hunting = True

        if self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 2):
        
            while hunting == True:

                # print(self.get_node_data())

                if self.get_node_data()[0][1] <= (self.n - 1):

                    if self.get_node_data()[1][self.get_node_data()[0][1]] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        # Break
                        if np.max(self.get_node_data()[2]) <= 0.5:

                            max_after = 0

                            for idx in range(self.get_node_data()[0][1],self.n-1):

                                if self.get_node_data()[2][idx] > max_after:

                                    max_after = self.get_node_data()[2][idx-self.n]

                            if max_after >= 0.5:

                                pass

                            else:

                                node_data = list(self.get_node_data())
                                node_data[0] = (self.get_node_data()[0][0],
                                                            self.get_node_data()[0][1],
                                                            self.get_node_data()[0][2],
                                                            "CP")

                                self.set_node_data(tuple(node_data))

                        hunting = False

                elif self.get_node_data()[0][1] <= (2*self.n - 1):

                    if self.get_node_data()[2][self.get_node_data()[0][1]-self.n] <= 0.5:

                        # Go to next commodity
                        node_data = list(self.get_node_data())
                        node_data[0] = (self.get_node_data()[0][0],
                                                    self.get_node_data()[0][1]+1,
                                                    self.get_node_data()[0][2],
                                                    self.get_node_data()[0][3])

                        self.set_node_data(tuple(node_data))

                    else:

                        max_after = 0

                        for idx in range(self.get_node_data()[0][1]+1,2*self.n-1):
                            
                            if self.get_node_data()[2][idx-self.n] > max_after:

                                max_after = self.get_node_data()[2][idx-self.n]

                        # print(max_after)

                        if max_after >= 0.5:

                            pass

                        else:

                            node_data = list(self.get_node_data())
                            node_data[0] = (self.get_node_data()[0][0],
                                                        self.get_node_data()[0][1],
                                                        self.get_node_data()[0][2],
                                                        "CP")

                            self.set_node_data(tuple(node_data))

                        # Break                      
                        hunting = False  

                else:

                    print(hi)

        # print(self.get_node_data())

        # if self.get_node_data() != None and \
        #     self.get_node_data()[0][2] == 1:

        #     print(hi2)

        if self.node_count == 0:
            # We are at the root node and must KSB
            # print("Branching:1")
            if self.Y_init_size == 1:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = None)

                self.sol_count += 1
                self.fol_prob_nodes += nodes

            else:

                LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

                self.Y_init.append(sorted(LB_idx))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                                node_data          = (("KSB",self.sol_count,"L"),None,None))

                self.make_branch(objective_estimate = self.get_objective_value(),
                                constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                                node_data          = (("KSB",self.sol_count,"R"),None,None))

                self.sol_count += 1
                self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "L"):
            # We are on a left KSB branch. Branch as normal
            # print("Branching:2")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count < self.Y_init_size):
            # We are on a right KSB branch and have not reached the Y_init_size. Create new KSB branch
            print("Branching:3")
            LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

            self.Y_init.append(sorted(LB_idx))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                            node_data          = (("KSB",self.sol_count,"L"),None,None))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                            node_data          = (("KSB",self.sol_count,"R"),None,None))

            self.sol_count += 1
            self.fol_prob_nodes += nodes

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R" and self.sol_count == self.Y_init_size):
            # We are on a right KSB branch and have reached the Y_init_size. Carry on as normal
            # print("Branching:4")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() == None or \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][2] == 1):
            # At a normal branch, so carry on
            # print("Branching:5")

            for i in range(self.get_num_branches()):
                self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == ""):
            # First half of delta branching and on rhs, so carry on with delta
            # print("Branching:6")
            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
                            node_data          = None)
                                                    # ("S",self.get_node_data()[0][1]+1,1),
                                                    # self.get_node_data()[1],
                                                    # self.get_node_data()[2],
                                                    # ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1) and self.get_node_data()[0][3] == "CP"):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1])],[1]],"L",self.supply[int(self.get_node_data()[0][1])]-self.get_node_data()[2][int(self.get_node_data()[0][1])])],
                            node_data          = None)

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 1) and self.get_node_data()[0][3] == ""):
            # Second half of delta branching and on rhs, so carry on with delta
            # print("Branching:7")
            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = None)
                            # (("S",self.get_node_data()[0][1]+1,1),
                            #                         self.get_node_data()[1],
                            #                         self.get_node_data()[2],
                            #                         ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = (("S",self.get_node_data()[0][1]+1,2,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

        elif self.get_node_data() != None and \
            (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 1) and self.get_node_data()[0][3] == "CP"):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

            IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
            VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

            IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
            VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

            RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
            RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
                            node_data          = (("S",self.get_node_data()[0][1]+1,1,""),
                                                    self.get_node_data()[1],
                                                    self.get_node_data()[2],
                                                    ))

            self.make_branch(objective_estimate = self.get_objective_value(),
                            constraints        = [([IND,VAL],"L",RHS),
                                                    ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
                            node_data          = None)


        # elif self.get_node_data() != None and \
        #     (self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] == (2*self.n - 1)):
                            
        #     IND = []
        #     VAL = []
        #     RHS = 0

        #     IND += [self.var_list.index(self.p_names[j][l]) for j in range(self.n) for l in range(1+self.log_values[j])]
        #     VAL += [2**l for j in range(self.n) for l in range(1+self.log_values[j])]

        #     IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
        #     VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

        #     RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
        #     RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
        #                     node_data          = (("S",self.get_node_data()[0][1]+1,1),
        #                                             self.get_node_data()[1],
        #                                             self.get_node_data()[2],
        #                                             ))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([IND,VAL],"L",RHS),
        #                                             ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
        #                     node_data          = None)

        else:

            print(hi)

        self.branch_count += 1
        self.node_count += self.get_num_branches()


    def generate_solutions_on_fly(self):

        fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,[0] * self.n,[0] * self.n,self.c,self.supply,0)

        LB_names, RB_names, nodes, u_values = fol_prob.ksb_solve_followers_problem(self.u_values,self.supply)

        self.ksb_sols_index.append((RB_names,len(LB_names)))
        self.no_fol_prob_solves += 1

        if u_values in self.u_values:
            print(hi)

        self.u_values.append(u_values)

        LB_idx = [self.var_list.index(LB_name) for LB_name in LB_names]
        RB_idx = [self.var_list.index(RB_name) for RB_name in RB_names]

        # print(here was last we got before i had to wash up before my parents arrived)

        return LB_idx, RB_idx, nodes

"""
        # print("NODE COUNT:",self.node_count)
        # print("SOL COUNT :",self.sol_count)
        # print("NODE DATA :",self.get_node_data())

        # if self.node_count >= 50:
        #     print(hi)

        # if self.node_count == 0 or \
        #     (self.sol_count < self.Y_init_size and self.get_node_data() != None and self.get_node_data()[0][0] == "KSB" and self.get_node_data()[0][2] == "R"):
        #     # At the root node

        #     print("1")
        #     # print(hi)

        #     LB_idx, RB_idx, nodes = self.generate_solutions_on_fly()

        #     self.Y_init.append(sorted(LB_idx))

        #     self.ksb_sols_index.append((RB_idx,len(LB_idx)))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                      constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
        #                      node_data          = [(("KSB",self.sol_count,"L"),None,None)])

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                      constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
        #                      node_data          = [(("KSB",self.sol_count,"R"),None,None)])

        #     self.sol_count += 1
        #     self.fol_prob_nodes += nodes

        # elif self.get_node_data() != None and self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (self.n - 1):

        #     print("2")
        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1])],[1]],"L",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))]-1)],
        #                     node_data          = None)
        #                                             # ("S",self.get_node_data()[0][1]+1,1),
        #                                             # self.get_node_data()[1],
        #                                             # self.get_node_data()[2],
        #                                             # ))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1])],[1]],"G",self.get_node_data()[1][int(int(self.get_node_data()[0][1]))])],
        #                     node_data          = (("S",self.get_node_data()[0][1]+1,2),
        #                                             self.get_node_data()[1],
        #                                             self.get_node_data()[2],
        #                                             ))

        # elif self.get_node_data() != None and self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] <= (2*self.n - 2):


        #     print("3")
        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
        #                     node_data          = None)
        #                     # (("S",self.get_node_data()[0][1]+1,1),
        #                     #                         self.get_node_data()[1],
        #                     #                         self.get_node_data()[2],
        #                     #                         ))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
        #                     node_data          = (("S",self.get_node_data()[0][1]+1,2),
        #                                             self.get_node_data()[1],
        #                                             self.get_node_data()[2],
        #                                             ))

        # elif self.get_node_data() != None and self.get_node_data()[0][0] == "S" and self.get_node_data()[0][1] == (2*self.n - 1):
        #                     #
        #     print("4")
        #     IND = []
        #     VAL = []
        #     RHS = 0

        #     IND += [self.var_list.index(self.p_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.ybar_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.y_names[i][k][j])  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        #     VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]

        #     IND += [self.var_list.index(self.x_names[j]) for j in range(self.n)]
        #     VAL += [-self.get_node_data()[1][j] for j in range(self.n)]

        #     RHS += np.sum(np.array(self.get_node_data()[1])*np.array(self.c))
        #     RHS += np.sum(np.array(self.get_node_data()[2])*np.array(self.c))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([[int(self.get_node_data()[0][1]-self.n)],[1]],"G",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)]+1)],
        #                     node_data          = None)
        #                     # (("S",self.get_node_data()[0][1]+1,1),
        #                     #                         self.get_node_data()[1],
        #                     #                         self.get_node_data()[2],
        #                     #                         ))

        #     self.make_branch(objective_estimate = self.get_objective_value(),
        #                     constraints        = [([IND,VAL],"L",RHS),
        #                                             ([[int(self.get_node_data()[0][1]-self.n)],[1]],"L",self.supply[int(self.get_node_data()[0][1]-self.n)]-self.get_node_data()[2][int(self.get_node_data()[0][1]-self.n)])],
        #                     node_data          = None)
        
        # # elif self.get_node_data() == None \
        # #     or self.get_node_data()[0][2] == 1 \
        # #     or self.get_node_data:
        # else:

        #     print("5")
        #     for i in range(self.get_num_branches()):
        #         self.make_cplex_branch(which_branch=i)#node_data=self.get_node_data())

        # self.branch_count += 1
        # self.node_count += self.get_num_branches()
        # print("NODE COUNT:",self.node_count)


class ksb_branch_callback_ybar_dmcc(BranchCallback):

    def __call__(self):

        if self.branch_count == 0 or \
            self.sol_count < self.Y_init_size and self.get_node_data() != None and self.get_node_data()[-1][-1] == "R" + str(self.sol_count-1):

            # Generate Initial Y

            fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,
                        np.array([float(0) for j in range(self.n)]),
                        np.array([float(0) for j in range(self.n)]),
                        self.c,
                        self.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem_non_comp()

            LB_idx = []
            RB_idx = []
            for i in range(self.i):
                for k in range(self.d[i]):
                    for j in range(self.n):
                        if fol_ybar_sol[i][k][j] >= 0.5:
                            LB_idx += [self.var_list.index(self.ybar_names[i][k][j])]
                            for ii in range(self.i):
                                for kk in range(self.d[i]):
                                    RB_idx += [self.var_list.index(self.ybar_names[ii][kk][j])]
                        elif fol_y_sol[i][k][j] >= 0.5:
                            LB_idx += [self.var_list.index(self.y_names[i][k][j])]
                            for ii in range(self.i):
                                for kk in range(self.d[i]):
                                    RB_idx += [self.var_list.index(self.y_names[ii][kk][j])]


            self.make_branch(objective_estimate = self.get_objective_value(),
                             constraints        = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))],
                             node_data          = [([LB_idx,[1]*len(LB_idx)],"E",len(LB_idx))])

            self.make_branch(objective_estimate = self.get_objective_value(),
                             constraints        = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1)],
                             node_data          = [([RB_idx,[1]*len(RB_idx)],"L",len(LB_idx)-1,"R" + str(self.sol_count))])

            self.sol_count += 1
            self.fol_prob_nodes += fol_nodes
            self.node_count += self.get_num_branches()
            self.branch_count += 1
            print("H")


class ksb_incumbent(IncumbentCallback):

    def __call__(self):

        if self.get_node_data() != None \
            and self.get_node_data()[0][2] == 2:
            # Within Delta branching tree, so dont accept incumbent

            self.reject()

        elif self.get_node_data() == None:

            fol_prob = MS_FOL_PROB(self.n,self.i,self.d,self.A,self.b,
                                    self.get_values(self.x_names),
                                    self.get_values(self.xbar_names),
                                    self.c,
                                    self.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()

            cur_obj = np.sum(np.array([np.sum([self.get_values(self.ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * (np.array(self.get_values(self.x_names)) + np.array(self.c))) + \
                        np.sum(np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * np.array(self.c))

            if fol_obj <= cur_obj - 0.1:
                print("Bilevel Infeasible",fol_obj,"<=",cur_obj)

            else:
                print("Bilevel Feasible")
                self.bi_feas = True
"""