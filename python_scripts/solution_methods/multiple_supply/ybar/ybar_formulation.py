"""
Multiple Supply

yBar formulation
"""
import pandas as pd
import numpy as np
import cplex
import time

from useful_functions.useful_functions import mccormick_constraints,binary_expansion_constraint
from solution_methods.multiple_supply.followers_problem import MS_FOL_PROB
from solution_methods.multiple_supply.ybar.callbacks import *
 
class ybar:

    def __init__(self,master):

        self.delta_names = []
        self.ybartilde_sols = []
        self.ytilde_sols = []
        self.columns = -1
        self.bi_feas = False
        self.sol = -1
        self.fol_node_count = 0
        self.var_list = []
        self.dyn_y_sols = []
        self.mip_gap = 0
        self.no_fol_prob_solves = 0

        self.total_no_nodes = 0
        self.total_branch_nodes = 0
 
    def create_instance(self,master):

        master.prob = cplex.Cplex()
        master.prob.objective.set_sense(master.prob.objective.sense.maximize)
        master.prob.parameters.timelimit.set(np.max([0,3600 - (time.time() - master.start_time)]))

        self.prob_settings(master)

        self.add_vars(master)
        self.add_constraints(master)
        self.register_callbacks(master)

    def prob_settings(self,master):
        
        master.prob.set_log_stream(None)
        master.prob.set_error_stream(None)
        master.prob.set_warning_stream(None)
        master.prob.set_results_stream(None)
        master.prob.parameters.mip.display.set(0)
        pass

    def solve(self,master):

        if master.solution_method in [1,5]:
            # Fixed Column generation
            results = self.solve_fixed_column_generation(master)
        
        elif master.solution_method in [2,6]:
            # Dynamic Column Generation
            results = self.solve_dynamic_column_generation(master)
        
        elif master.solution_method in [3,4,7,8,13,14,15,16]:
            # Delta Branching
            results = self.delta_branching(master)

        else:
            print("Unknown Solution method")

        # self.show_solution(master)

        return results

    def solve_fixed_column_generation(self,master):

        while True:
            self.columns += 1

            print("\n")
            print("Iteration:",self.columns)
            self.create_instance(master)

            master.prob.solve() 

            if master.prob.solution.get_status() not in [101,102]:

                return [["Status",          master.prob.solution.get_status()],
                        ["Obj",             self.sol],
                        ["Bi_Feas",         self.bi_feas],
                        ["MIP_Gap",         self.mip_gap],
                        ["No_Lazy_Calls",   0],
                        ["No_Lazy_Cuts",    0],
                        ["No_Nodes",        self.total_no_nodes],
                        ["No_Branch_Nodes", self.total_branch_nodes],
                        ["Fol_Node_Count",  self.fol_node_count],
                        ["No_KSB_Sols",     0],
                        ["No_Lambda_Sols",  0],
                        ["Stage_2_Feas",    "NAP"],
                        ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]
   

            if np.max([0,3600 - (time.time() - master.start_time)]) <= 0:
                self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
                break

            fol_prob = MS_FOL_PROB(master.n,master.i,master.d,master.A,master.b,
                                    master.prob.solution.get_values(self.x_names),
                                    master.prob.solution.get_values(self.xbar_names),
                                    master.c,
                                    master.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()

            self.fol_node_count += fol_nodes
            self.total_no_nodes += self.node_callback.node_count
            self.total_branch_nodes += self.branch_callback.node_count
            self.no_fol_prob_solves += 1

            cur_obj = np.sum([np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*(master.prob.solution.get_values(self.x_names[j]) + master.c[j]) for j in range(master.n)]) + \
                        np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*master.c[j] for j in range(master.n)])
            print("fol_obj", fol_obj)
            print("cur_obj", cur_obj)

            if fol_obj <= cur_obj - 0.5:
                print("Bilevel Infeasible")
                print("Add Column")
                self.sol = master.prob.solution.get_objective_value()
                self.ybartilde_sols.append(fol_ybar_sol)
                self.ytilde_sols.append(fol_y_sol)
            else:
                print("\n")
                print("Bilevel Feasible")
                self.sol = master.prob.solution.get_objective_value()
                self.bi_feas = True
                self.mip_gap = 0
                break

        self.show_solution(master)

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             self.sol],
                ["Bi_Feas",         self.bi_feas],
                ["MIP_Gap",         self.mip_gap],
                ["No_Lazy_Calls",   0],
                ["No_Lazy_Cuts",    0],
                ["No_Nodes",        self.total_no_nodes],
                ["No_Branch_Nodes", self.total_branch_nodes],
                ["Fol_Node_Count",  self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  0],
                ["Stage_2_Feas",    "NAP"],
                ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

    def solve_dynamic_column_generation(self,master):

        while True:
            self.columns += 1

            print("\n")
            print("Iteration:",self.columns)
            self.create_instance(master)

            master.prob.solve()     

            if master.prob.solution.get_status() not in [101,102]:

                return [["Status",          master.prob.solution.get_status()],
                        ["Obj",             self.sol],
                        ["Bi_Feas",         self.bi_feas],
                        ["MIP_Gap",         self.mip_gap],
                        ["No_Lazy_Calls",   0],
                        ["No_Lazy_Cuts",    0],
                        ["No_Nodes",        self.total_no_nodes],
                        ["No_Branch_Nodes", self.total_branch_nodes],
                        ["Fol_Node_Count",  self.fol_node_count],
                        ["No_KSB_Sols",     0],
                        ["No_Lambda_Sols",  0],
                        ["Stage_2_Feas",    "NAP"],
                        ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

            if np.max([0,3600 - (time.time() - master.start_time)]) <= 0:
                self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
                break

            fol_prob = MS_FOL_PROB(master.n,master.i,master.d,master.A,master.b,
                                    master.prob.solution.get_values(self.x_names),
                                    master.prob.solution.get_values(self.xbar_names),
                                    master.c,
                                    master.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()

            self.fol_node_count += fol_nodes
            self.total_no_nodes += self.node_callback.node_count
            self.total_branch_nodes += self.branch_callback.node_count
            self.no_fol_prob_solves += 1

            cur_obj = np.sum([np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*(master.prob.solution.get_values(self.x_names[j]) + master.c[j]) for j in range(master.n)]) + \
                        np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*master.c[j] for j in range(master.n)])
            print("fol_obj", fol_obj)
            print("cur_obj", cur_obj)

            if fol_obj <= cur_obj - 0.5:
                print("Bilevel Infeasible")
                print("Add Column")
                self.sol = master.prob.solution.get_objective_value()
                # print(list(np.array(fol_ybar_sol) + np.array(fol_y_sol)))
                # print(self.dyn_y_sols)
                if list(np.array(fol_ybar_sol) + np.array(fol_y_sol)) in self.dyn_y_sols:
                    print("already got this")

                self.dyn_y_sols.append(list(np.array(fol_ybar_sol) + np.array(fol_y_sol)))

            else:
                print("\n")
                print("Bilevel Feasible")
                self.sol = master.prob.solution.get_objective_value()
                self.bi_feas = True
                self.mip_gap = 0
                break

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             self.sol],
                ["Bi_Feas",         self.bi_feas],
                ["MIP_Gap",         self.mip_gap],
                ["No_Lazy_Calls",   0],
                ["No_Lazy_Cuts",    0],
                ["No_Nodes",        self.total_no_nodes],
                ["No_Branch_Nodes", self.total_branch_nodes],
                ["Fol_Node_Count",  self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  0],
                ["Stage_2_Feas",    "NAP"],
                ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

    def delta_branching(self,master):

        self.create_instance(master)

        master.prob.parameters.preprocessing.reduce.set(0)

        master.prob.solve()

        try:
            self.sol = master.prob.solution.get_objective_value()
        except:
            self.sol = -1

        try:
            self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
        except:
            self.mip_gap = -1

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             self.sol],
                ["Bi_Feas",         self.inc_callback.bi_feas],
                ["MIP_Gap",         self.mip_gap],
                ["No_Lazy_Calls",   0],
                ["No_Lazy_Cuts",    0],
                ["No_Nodes",        self.node_callback.node_count],
                ["No_Branch_Nodes", self.branch_callback.node_count],
                ["Fol_Node_Count",  self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  0],
                ["Stage_2_Feas",    "NAP"],
                ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves + self.inc_callback.no_fol_prob_solves]]

    def add_vars(self,master):

        self.add_xbar_vars(master)
        self.add_x_vars(master)
        self.add_ybar_vars(master)
        self.add_y_vars(master)

        if master.solution_method in [1,2,3,4,13,14]:
            # Direct McCormick
            self.add_p_vars(master)

        elif master.solution_method in [5,6,7,8,15,16]:
            # Binary expansion
            self.add_alpha_vars(master)
            self.add_p_vars(master)

        if master.solution_method in [1,5]:
            # Fixed Column generation
            self.add_delta_vars(master)

        elif master.solution_method in [2,6]:
            # Dynamic Column Generation
            if master.solution_method == 6:

                self.add_beta_vars(master)

            self.add_ybartilde_vars(master)
            self.add_ytilde_vars(master)
            self.add_q_vars(master)
            self.add_dynamic_delta_vars(master)
            self.add_t_vars(master)


        elif master.solution_method in [3,4,7,8,13,14,15,16]:
            # Delta Branching
            pass

    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.group_constraints(master)
        self.follower_supply_constraint(master)
        self.ybar_xbar_constraint(master)
        self.y_supply_x_constraint(master)
        self.ybar_y_sum_constraints(master)

        if master.solution_method in [1,2,3,4,13,14]:
            # Direct McCormick
            for j in range(master.n):
                for i in range(master.i):
                    for k in range(master.d[i]):
                        mccormick_constraints(master.prob,
                                              (self.ybar_names[i][k][j],0,1),
                                              (self.x_names[j],0,master.M),
                                              self.p_names[i][k][j])

            # Minimum objective value
            self.min_objective_constraint_direct_mc(master)
            
        elif master.solution_method in [5,6,7,8,15,16]:
            # Binary expansion
            for j in range(master.n):
                binary_expansion_constraint(master.prob,
                                [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                self.alpha_names[j])

                for l in range(1+self.log_values[j]):
                    mccormick_constraints(master.prob,
                                            (self.alpha_names[j][l],0,1),
                                            (self.x_names[j],0,master.M),
                                            self.p_names[j][l])    

            # Minimum objective value
            self.min_objective_constraint_be_mc(master)        

        if master.solution_method in [1,5]:
            # Fixed Column Generation
            for sol in range(len(self.ybartilde_sols)):
                for j in range(master.n):
                    mccormick_constraints(master.prob,
                                        (self.deltaybar_names[sol][j],0,1),
                                        (self.deltay_names[sol][j],0,1),
                                        self.delta_names[sol][j])

            self.deltaybar_constraints(master)
            self.deltay_constraints(master)
            self.fixed_value_function_constraint(master)

        elif master.solution_method in [2,6]:
            # Dynamic Column Generation
            if master.solution_method == 2:
                # Direct McCormick of RHS
                for sol in range(len(self.dyn_y_sols)):
                    for i in range(master.i):
                        for k in range(master.d[i]):
                            for j in range(master.n):
                                mccormick_constraints(master.prob,
                                                    (self.ybartilde_names[sol][i][k][j],0,1),
                                                    (self.x_names[j],0,master.M),
                                                    self.q_names[sol][i][k][j])
            
            elif master.solution_method == 6:
                # Binary expansion then McCormick of rhs
                for sol in range(len(self.dyn_y_sols)):
                    for j in range(master.n):
                        binary_expansion_constraint(master.prob,
                                        [self.ybartilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                        self.beta_names[sol][j])

                        for l in range(1+self.beta_log_values[sol][j]):
                            mccormick_constraints(master.prob,
                                                    (self.beta_names[sol][j][l],0,1),
                                                    (self.x_names[j],0,master.M),
                                                    self.q_names[sol][j][l])



            self.dynamic_delta_constraints(master)
            self.ybartilde_xbar_constraints(master)
            self.ytilde_s_xbar_constraints(master)
            self.ybartilde_ytilde_sum_constraints(master)
            self.dynamic_value_function_constraint(master)

            for sol in range(len(self.dyn_y_sols)):
                for j in range(master.n):
                    mccormick_constraints(master.prob,
                                            (self.dynamic_delta_names[sol][j],0,1),
                                            (self.xbar_names[j],0,float(master.supply[j])),
                                            self.t_names[sol][j])


        elif master.solution_method in [3,4,7,8,13,14,15,16]:
            # Delta Branching
            pass

    def register_callbacks(self,master):

        # Node Callbacks
        self.node_callback = master.prob.register_callback(node_callback)
        self.node_callback.node_count = 0

        # Branch Callbacks
        if master.solution_method in [1,2,5,6]:

            self.branch_callback = master.prob.register_callback(branch_callback)
            self.branch_callback.node_count = 0
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [3]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_dmcc_branch_callback)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [4]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_dmcc_ksb_branch_callback)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.Y          = []
            self.branch_callback.Y_init     = []
            self.branch_callback.Y_init_size = master.ksb_no_sols
            self.branch_callback.sol_count  = 0
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.u_values = []
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [7]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_be_mcc_branching_callback)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.log_values = self.log_values
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [8]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_be_mcc_ksb_branching_callback)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.Y          = []
            self.branch_callback.Y_init     = []
            self.branch_callback.Y_init_size = master.ksb_no_sols
            self.branch_callback.sol_count  = 0
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.log_values = self.log_values
            self.branch_callback.u_values = []
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [13]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_dmcc_branch_callback_plus)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [14]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_dmcc_ksb_branch_callback_plus)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.Y          = []
            self.branch_callback.Y_init     = []
            self.branch_callback.Y_init_size = master.ksb_no_sols
            self.branch_callback.sol_count  = 0
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.u_values = []
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [15]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_be_mcc_branching_callback_plus)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.log_values = self.log_values
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [16]:

            self.branch_callback            = master.prob.register_callback(delta_ybar_be_mcc_ksb_branching_callback_plus)
            self.branch_callback.node_count = 0
            self.branch_callback.var_list   = self.var_list
            self.branch_callback.n          = master.n
            self.branch_callback.supply     = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.p_names    = self.p_names
            self.branch_callback.ybar_names = self.ybar_names
            self.branch_callback.y_names    = self.y_names
            self.branch_callback.c          = master.c
            self.branch_callback.x_names    = self.x_names
            self.branch_callback.Y          = []
            self.branch_callback.Y_init     = []
            self.branch_callback.Y_init_size = master.ksb_no_sols
            self.branch_callback.sol_count  = 0
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.log_values = self.log_values
            self.branch_callback.u_values = []
            self.branch_callback.no_fol_prob_solves = 0

        # Incumbent Callbacks
        if master.solution_method in [3,7,13,15]:

            self.inc_callback               = master.prob.register_callback(IncCallback)
            self.inc_callback.n             = master.n
            self.inc_callback.i             = master.i
            self.inc_callback.d             = master.d
            self.inc_callback.A             = master.A
            self.inc_callback.b             = master.b
            self.inc_callback.c             = master.c
            self.inc_callback.supply        = master.supply
            self.inc_callback.x_names       = self.x_names
            self.inc_callback.xbar_names    = self.xbar_names
            self.inc_callback.y_names       = self.y_names
            self.inc_callback.ybar_names    = self.ybar_names
            self.inc_callback.bi_feas       = self.bi_feas
            self.inc_callback.ybar_sols     = []
            self.inc_callback.y_sols        = []
            self.inc_callback.no_fol_prob_solves = 0

        elif master.solution_method in [4,8,14,16]:

            self.inc_callback               = master.prob.register_callback(ksb_incumbent_callback)
            self.inc_callback.n             = master.n
            self.inc_callback.i             = master.i
            self.inc_callback.d             = master.d
            self.inc_callback.A             = master.A
            self.inc_callback.b             = master.b
            self.inc_callback.c             = master.c
            self.inc_callback.supply        = master.supply
            self.inc_callback.x_names       = self.x_names
            self.inc_callback.xbar_names    = self.xbar_names
            self.inc_callback.y_names       = self.y_names
            self.inc_callback.ybar_names    = self.ybar_names
            self.inc_callback.bi_feas       = self.bi_feas
            self.inc_callback.ybar_sols     = []
            self.inc_callback.y_sols        = []
            self.inc_callback.node_count    = self.branch_callback.node_count
            self.inc_callback.Y             = []
            self.inc_callback.Y_init        = []
            self.inc_callback.Y_init_size   = master.ksb_no_sols
            self.inc_callback.ksb_sols_index = []
            self.inc_callback.var_list      = self.var_list
            self.inc_callback.sol_count     = self.branch_callback.sol_count
            self.inc_callback.no_fol_prob_solves = 0


    # Variables

    def add_ybar_vars(self,master):

        self.ybar_names = [[["ybar_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                            for k in range(master.d[i])]\
                                                                            for i in range(master.i)]

        # self.ybar_df = pd.DataFrame()

        # # Add variables
        for i in range(master.i):
            for k in range(master.d[i]):
                self.var_list += self.ybar_names[i][k]
                master.prob.variables.add(obj= [float(master.c[j]) for j in range(master.n)],
                                        lb = [0] * master.n,
                                        ub = [1] * master.n,
                                        types = "B" * master.n,
                                        names = self.ybar_names[i][k])

    def add_y_vars(self,master):

        self.y_names = [[["y_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                            for k in range(master.d[i])]\
                                                                            for i in range(master.i)]

        # self.ybar_df = pd.DataFrame()

        # # Add variables
        for i in range(master.i):
            for k in range(master.d[i]):
                self.var_list += self.y_names[i][k]
                master.prob.variables.add(obj= [0] * master.n,
                                        lb = [0] * master.n,
                                        ub = [1] * master.n,
                                        types = "B" * master.n,
                                        names = self.y_names[i][k])

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(j) for j in range(master.n)]
        self.var_list += self.x_names

        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(j) for j in range(master.n)]
        self.var_list += self.xbar_names

        master.prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [float(master.supply[j]) for j in range(master.n)],
                                  types = "I" * master.n,
                                  names = self.xbar_names)

    def add_alpha_vars(self,master):

        self.log_values = [int(np.floor(np.log2(j))) for j in master.supply]

        self.alpha_names = [["alpha_" + str(j) + "_" + str(l) for l in range(1+self.log_values[j])]\
                                                                            for j in range(master.n)]

        for j in range(master.n):
            self.var_list += self.alpha_names[j]
            master.prob.variables.add(obj   = [0] * (1+self.log_values[j]),
                                        lb    = [0] * (1+self.log_values[j]),
                                        ub    = [1] * (1+self.log_values[j]),
                                        types = "B" * (1+self.log_values[j]),
                                        names = self.alpha_names[j])

    def add_p_vars(self,master):

        if master.solution_method in [1,2,3,4,13,14]:

            self.p_names = [[["p_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                                for k in range(master.d[i])]\
                                                                                for i in range(master.i)]

            # # Add variables
            for i in range(master.i):
                for k in range(master.d[i]):
                    self.var_list += self.p_names[i][k]
                    master.prob.variables.add(obj= [1] * master.n,
                                            lb = [0] * master.n,
                                            ub = [master.M] * master.n,
                                            types = "C" * master.n,
                                            names = self.p_names[i][k])        

        elif master.solution_method in [5,6,7,8,15,16]:

            self.p_names = [["p_" + str(j) + "_" + str(l) for l in range(1+self.log_values[j])]\
                                                                                for j in range(master.n)]
            for j in range(master.n):
                self.var_list += self.p_names[j]
                master.prob.variables.add(obj   = [2**lg for lg in range(1+self.log_values[j])],
                                            lb    = [0] * (1+self.log_values[j]),
                                            ub    = [master.M] * (1+self.log_values[j]),
                                            types = "C" * (1+self.log_values[j]),
                                            names = self.p_names[j])

    def add_delta_vars(self,master):

        self.delta_names = [["delta_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                for sol in range(len(self.ytilde_sols))]

        self.deltaybar_names = [["deltaybar_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                for sol in range(len(self.ytilde_sols))]

        self.deltay_names = [["deltay_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                for sol in range(len(self.ytilde_sols))]

        for sol in range(len(self.ytilde_sols)):
            
            self.var_list += self.deltaybar_names[sol]
            self.var_list += self.deltay_names[sol]
            self.var_list += self.delta_names[sol]

            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = ["B"] * master.n,
                                    names = self.deltaybar_names[sol])
            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = ["B"] * master.n,
                                    names = self.deltay_names[sol])
            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = ["B"] * master.n,
                                    names = self.delta_names[sol])

    def add_ybartilde_vars(self,master):

        self.ybartilde_names = [[[["ybartilde_" + str(sol) + "_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                                                        for k in range(master.d[i])]\
                                                                                                        for i in range(master.i)]\
                                                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for i in range(master.i):
                for k in range(master.d[i]):
                    self.var_list += self.ybartilde_names[sol][i][k]
                    master.prob.variables.add(obj    = [0] * master.n,
                                              lb     = [0] * master.n,
                                              ub     = [1] * master.n,
                                              types  = "B" * master.n,
                                              names  = self.ybartilde_names[sol][i][k])

    def add_ytilde_vars(self,master):

        self.ytilde_names = [[[["ytilde_" + str(sol) + "_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                                                        for k in range(master.d[i])]\
                                                                                                        for i in range(master.i)]\
                                                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for i in range(master.i):
                for k in range(master.d[i]):
                    self.var_list += self.ytilde_names[sol][i][k]
                    master.prob.variables.add(obj    = [0] * master.n,
                                              lb     = [0] * master.n,
                                              ub     = [1] * master.n,
                                              types  = "B" * master.n,
                                              names  = self.ytilde_names[sol][i][k])

    def add_q_vars(self,master):

        if master.solution_method == 2:

            self.q_names = [[[["q_" + str(sol) + "_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                                                            for k in range(master.d[i])]\
                                                                                                            for i in range(master.i)]\
                                                                                                            for sol in range(len(self.dyn_y_sols))]

            for sol in range(len(self.dyn_y_sols)):
                for i in range(master.i):
                    for k in range(master.d[i]):
                        self.var_list += self.q_names[sol][i][k]
                        master.prob.variables.add(obj    = [0] * master.n,
                                                lb     = [0] * master.n,
                                                ub     = [master.M] * master.n,
                                                types  = "C" * master.n,
                                                names  = self.q_names[sol][i][k])

        elif master.solution_method == 6:

            self.q_names = [[["q_" + str(sol) + "_" + str(j) + "_" + str(l) for l in range(1+self.beta_log_values[sol][j])] for j in range(master.n)] for sol in range(len(self.dyn_y_sols))]

            for sol in range(len(self.dyn_y_sols)):
                for j in range(master.n):
                    self.var_list += self.q_names[sol][j]
                    master.prob.variables.add(obj    = [0] * (1+self.beta_log_values[sol][j]),
                                            lb     = [0] * (1+self.beta_log_values[sol][j]),
                                            ub     = [master.M] * (1+self.beta_log_values[sol][j]),
                                            types  = "C" * (1+self.beta_log_values[sol][j]),
                                            names  = self.q_names[sol][j])

    def add_dynamic_delta_vars(self,master):

        self.dynamic_delta_names = [["delta_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [1] * master.n,
                                      types = "B" * master.n,
                                      names = self.dynamic_delta_names[sol])

    def add_t_vars(self,master):

        self.t_names = [["t_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [float(master.supply[j]) for j in range(master.n)],
                                      types = "C" * master.n,
                                      names = self.t_names[sol])
       
    def add_beta_vars(self,master):

        self.beta_log_values = [[0 for j in range(master.n)] for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                if self.dyn_y_sols[sol][j] >= 1:
                    self.beta_log_values[sol][j] = int(np.floor(np.log2(self.dyn_y_sols[sol][j])))
                else:
                    self.beta_log_values[sol][j] = 0

        self.beta_names = [[["beta_" + str(sol) + "_" + str(j) + "_" + str(l) for l in range(1+self.beta_log_values[sol][j])]\
                                                                            for j in range(master.n)]\
                                                                            for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                self.var_list += self.beta_names[sol][j]
                master.prob.variables.add(obj   = [0] * (1+self.beta_log_values[sol][j]),
                                            lb    = [0] * (1+self.beta_log_values[sol][j]),
                                            ub    = [1] * (1+self.beta_log_values[sol][j]),
                                            types = "B" * (1+self.beta_log_values[sol][j]),
                                            names = self.beta_names[sol][j])

    # Constraints

    def budget_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                         senses = ["L"],
                                         rhs = [master.B],
                                         names = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def group_constraints(self,master):

        for i in range(master.i):
            for k in range(master.d[i]):
                for j in range(np.shape(master.A[i])[0]):
                    master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.ybar_names[i][k] + self.y_names[i][k],
                                                                                      val=list(master.A[i][j,:]) + list(master.A[i][j,:]))],
                                                        senses  = ["G"],
                                                        rhs     = [master.b[i][j]])

    def follower_supply_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                            [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                        val=[1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                            [1 for i in range(master.i) for k in range(master.d[i])])],
                                             senses = ["L"],
                                             rhs = [float(master.supply[j])],
                                             names = ["Fol_supply_" + str(j)])

    def ybar_xbar_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + [self.xbar_names[j]],
                                                                        val=[1 for i in range(master.i) for k in range(master.d[i])] + [-1])],
                                             senses  =["L"],
                                             rhs     =[0])

    def y_supply_x_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + [self.xbar_names[j]],
                                                                        val=[1 for i in range(master.i) for k in range(master.d[i])] + [1])],
                                             senses  =["L"],
                                             rhs     =[float(master.supply[j])])

    def ybar_y_sum_constraints(self,master):

        for i in range(master.i):
            for k in range(master.d[i]):
                for j in range(master.n):
                    master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.ybar_names[i][k][j],self.y_names[i][k][j]],
                                                                                    val = [1,1])],
                                                       senses   = ["L"],
                                                       rhs      = [1])

    def deltaybar_constraints(self,master):

        self.M_bar = 2*max(master.supply)

        for sol in range(len(self.ytilde_sols)):

            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltaybar_names[sol][j],self.xbar_names[j]],
                                                                                val = [-float(self.M_bar),1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.ybartilde_sols[sol][j] - 0.5)])     


                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltaybar_names[sol][j],self.xbar_names[j]],
                                                                                val = [float(self.M_bar),-1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.M_bar + 0.5 - self.ybartilde_sols[sol][j])])

    def deltay_constraints(self,master):

        for sol in range(len(self.ytilde_sols)):

            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltay_names[sol][j],self.xbar_names[j]],
                                                                                val = [-float(self.M_bar),-1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.ytilde_sols[sol][j] - 0.5 - master.supply[j])])     


                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltay_names[sol][j],self.xbar_names[j]],
                                                                                val = [float(self.M_bar),1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.M_bar + 0.5 + master.supply[j] - self.ytilde_sols[sol][j])])

    def fixed_value_function_constraint(self,master):

        self.M_delta = master.M * master.n

        if master.solution_method in [1]:

            for sol in range(len(self.ytilde_sols)):

                IND = []
                VAL = []
                RHS = 0

                IND += [self.p_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [1 for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.y_names[i][k][j]  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]            
                VAL += [float(master.c[j])  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += self.x_names
                VAL += [-self.ybartilde_sols[sol][j] for j in range(master.n)]

                IND += self.delta_names[sol]
                VAL += [self.M_delta for j in range(master.n)]

                RHS += np.sum([self.ybartilde_sols[sol][j]*master.c[j] for j in range(master.n)])
                RHS += np.sum([self.ytilde_sols[sol][j]*master.c[j] for j in range(master.n)])
                RHS += np.sum([self.M_delta for j in range(master.n)])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                                    val = VAL)],
                                                        senses   = ["L"],
                                                        rhs      = [float(RHS)],
                                                        names    = ["VFC_" + str(sol)])

        elif master.solution_method in [5]:

            for sol in range(len(self.ytilde_sols)):

                IND = []
                VAL = []
                RHS = 0

                IND += [self.p_names[j][l] for j in range(master.n) for l in range(1+self.log_values[j])]
                VAL += [(2**l) for j in range(master.n) for l in range(1+self.log_values[j])]

                IND += [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.y_names[i][k][j]  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]            
                VAL += [float(master.c[j])  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += self.x_names
                VAL += [-self.ybartilde_sols[sol][j] for j in range(master.n)]

                IND += self.delta_names[sol]
                VAL += [self.M_delta for j in range(master.n)]

                RHS += np.sum([self.ybartilde_sols[sol][j]*master.c[j] for j in range(master.n)])
                RHS += np.sum([self.ytilde_sols[sol][j]*master.c[j] for j in range(master.n)])
                RHS += np.sum([self.M_delta for j in range(master.n)])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                                    val = VAL)],
                                                        senses   = ["L"],
                                                        rhs      = [float(RHS)],
                                                        names    = ["VFC_" + str(sol)])

    def dynamic_delta_constraints(self,master):

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ytilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [self.dynamic_delta_names[sol][j]] + \
                                                                                  [self.xbar_names[j]],
                                                                              val=[-1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [float(master.supply[j])] + \
                                                                                  [-1])],
                                                senses  =["L"],
                                                rhs     =[0])

    def ybartilde_xbar_constraints(self,master):

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybartilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i])] + [self.t_names[sol][j]],
                                                                            val=[1 for i in range(master.i) for k in range(master.d[i])] + [-1])],
                                                senses  =["L"],
                                                rhs     =[0])

    def ytilde_s_xbar_constraints(self,master):

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ytilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i])] + [self.xbar_names[j]],
                                                                            val=[1 for i in range(master.i) for k in range(master.d[i])] + [1])],
                                                senses  =["L"],
                                                rhs     =[float(master.supply[j])])

    def ybartilde_ytilde_sum_constraints(self,master):

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybartilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [self.ytilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                              val=[1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [1 for i in range(master.i) for k in range(master.d[i])])],
                                                senses  =["E"],
                                                rhs     =[float(self.dyn_y_sols[sol][j])])

    def dynamic_value_function_constraint(self,master):

        if master.solution_method in [2]:

            for sol in range(len(self.dyn_y_sols)):

                IND = []
                VAL = []
                RHS = 0

                IND += [self.p_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [1 for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.y_names[i][k][j]  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]            
                VAL += [float(master.c[j])  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.q_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [-1 for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.ybartilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [-float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.ytilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [-float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                                    val = VAL)],
                                                        senses   = ["L"],
                                                        rhs      = [float(RHS)],
                                                        names    = ["VFC_" + str(sol)])

        elif master.solution_method in [6]:

            for sol in range(len(self.dyn_y_sols)):

                IND = []
                VAL = []
                RHS = 0

                IND += [self.p_names[j][l] for j in range(master.n) for l in range(1+self.log_values[j])]
                VAL += [2**l for j in range(master.n) for l in range(1+self.log_values[j])]

                IND += [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.y_names[i][k][j]  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]            
                VAL += [float(master.c[j])  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.q_names[sol][j][l] for j in range(master.n) for l in range(1+self.beta_log_values[sol][j])]
                VAL += [-2**l for j in range(master.n) for l in range(1+self.beta_log_values[sol][j])]

                IND += [self.ybartilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [-float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                IND += [self.ytilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
                VAL += [-float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                                    val = VAL)],
                                                        senses   = ["L"],
                                                        rhs      = [float(RHS)],
                                                        names    = ["VFC_" + str(sol)])

    def min_objective_constraint_direct_mc(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.p_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)] + \
                                                                              [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)] + \
                                                                              [self.xbar_names[j] for j in range(master.n)],
                                                                        val = [1 for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)] + \
                                                                              [float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)] + \
                                                                              [float(-master.c[j]) for j in range(master.n)])],
                                           senses   = ["G"],
                                           rhs      = [0.0])

    def min_objective_constraint_be_mc(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.p_names[j][l] for j in range(master.n) for l in range(1+self.log_values[j])] + \
                                                                              [self.ybar_names[i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)] + \
                                                                              [self.xbar_names[j] for j in range(master.n)],
                                                                        val = [float(2**l) for j in range(master.n) for l in range(1+self.log_values[j])] + \
                                                                              [float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)] + \
                                                                              [float(-master.c[j]) for j in range(master.n)])],
                                           senses   = ["G"],
                                           rhs      = [0.0])


    # Postprocessing

    def show_solution(self,master):

        if master.solution_method in [1,3]:

            print("Objective:",master.prob.solution.get_objective_value())
            print("INC      :",np.sum([np.sum([master.prob.solution.get_values(self.p_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)]) + \
                            np.sum([np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j])*master.c[j] for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)]))
            print("EXP      :",np.sum([master.c[j]*master.prob.solution.get_values(self.xbar_names[j]) for j in range(master.n)]))
            print("Fol Obj  :",np.sum([np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) * (master.c[j] + master.prob.solution.get_values(self.x_names[j])) for j in range(master.n)]) + \
                                np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) * master.c[j] for j in range(master.n)]))

            vstack = np.vstack((master.supply,
                                master.prob.solution.get_values(self.xbar_names),
                                master.prob.solution.get_values(self.x_names),
                                master.c,
                                [np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)],
                                [np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)],
                                )).T

            df = pd.DataFrame(vstack,columns=["S","XBAR","X","C","YSUM","YBARSUM"])#,"U"])
            print("\n")
            print(df)



        elif master.solution_method in [2,4]:
            print("Objective:",master.prob.solution.get_objective_value())
            print("INC      :",np.sum([np.sum([(2**l)*master.prob.solution.get_values(self.p_names[j][l]) for l in range(1+self.log_values[j])]) for j in range(master.n)]) + \
                               np.sum([np.sum([master.c[j]*master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)]) )
            print("EXP      :",np.sum([master.c[j]*master.prob.solution.get_values(self.xbar_names[j]) for j in range(master.n)]))
            print("Fol Obj  :",np.sum([np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) * (master.c[j] + master.prob.solution.get_values(self.x_names[j])) for j in range(master.n)]) + \
                                np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) * master.c[j] for j in range(master.n)]))

            vstack = np.vstack((master.supply,
                                master.prob.solution.get_values(self.xbar_names),
                                master.prob.solution.get_values(self.x_names),
                                master.c,
                                [np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)],
                                [np.sum([master.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)],
                                )).T

            df = pd.DataFrame(vstack,columns=["S","XBAR","X","C","YSUM","YBARSUM"])#,"U"])
            print("\n")
            print(df)























