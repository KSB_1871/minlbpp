"""
Multiple Supply

Gamma formulation
"""
import pandas as pd
import numpy as np
import cplex
import time

from useful_functions.useful_functions import mccormick_constraints,binary_expansion_constraint
from solution_methods.multiple_supply.followers_problem import MS_FOL_PROB
from solution_methods.multiple_supply.gamma.callbacks import *

class gamma:

    def __init__(self,master):

        self.delta_names    = []
        self.ybartilde_sols = []
        self.ytilde_sols    = []
        self.columns        = -1
        self.bi_feas        = False
        self.sol            = -1
        self.fol_node_count = 0
        self.var_list       = []
        self.dyn_y_sols     = []
        self.mip_gap = 0
        self.no_fol_prob_solves = 0

        self.total_no_nodes = 0
        self.total_branch_nodes = 0

        
        # self.create_problem(master)

    def create_problem(self,master):

        master.prob = cplex.Cplex()
        master.prob.objective.set_sense(master.prob.objective.sense.maximize)
        master.prob.parameters.timelimit.set(np.max([0,3600 - (time.time() - master.start_time)]))
        self.prob_settings(master)

        self.add_vars(master)
        self.add_constraints(master)
        self.add_callbacks(master)

    def prob_settings(self,master):
        
        master.prob.set_log_stream(None)
        master.prob.set_error_stream(None)
        master.prob.set_warning_stream(None)
        master.prob.set_results_stream(None)
        master.prob.parameters.mip.display.set(0)

    def add_vars(self,master):

        self.add_xbar_vars(master)
        self.add_x_vars(master)
        self.add_y_vars(master)
        self.add_gamma_vars(master)
        self.add_z_names(master)
        self.add_alpha_vars(master)
        self.add_p_vars(master)

        if master.solution_method == 9:
            # Add delta variables for the fixed column generation
            self.add_delta_vars(master)

        elif master.solution_method in [10]:

            self.add_ztilde_vars(master)
            self.add_beta_vars(master)
            self.add_q_vars(master)
            self.add_dynamic_delta_vars(master)

        if self.Y != []:

            self.add_not_in_Y_vars(master)

    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.group_constraints(master)
        self.follower_supply_constraint(master)
        self.gamma_constraints(master)
        self.z_constraints(master)

        self.min_objective_constraint(master)

        for j in range(master.n):

            binary_expansion_constraint(master.prob,
                                        [self.z_names[j]],
                                        self.alpha_names[j])


            for l in range(1+self.log_values[j]):

                mccormick_constraints(master.prob,
                                        (self.alpha_names[j][l],0,1),
                                        (self.x_names[j],0,master.M),
                                        self.p_names[j][l])

        if master.solution_method in [9]:

            self.deltaybar_constraints(master)
            self.deltay_constraints(master)

            for sol in range(len(self.ybartilde_sols)):
                for j in range(master.n):
                    mccormick_constraints(master.prob,
                                        (self.deltaybar_names[sol][j],0,1),
                                        (self.deltay_names[sol][j],0,1),
                                        self.delta_names[sol][j])

            self.fixed_value_function_constraint(master)

        elif master.solution_method in [10]:

            for sol in range(len(self.dyn_y_sols)):
                for j in range(master.n):
                    binary_expansion_constraint(master.prob,
                                    [self.ztilde_names[sol][j]],
                                    self.beta_names[sol][j])

                    for l in range(1+self.beta_log_values[sol][j]):
                        mccormick_constraints(master.prob,
                                                (self.beta_names[sol][j][l],0,1),
                                                (self.x_names[j],0,master.M),
                                                self.q_names[sol][j][l])


            self.dynamic_delta_constraints(master)
            self.ztilde_constraints(master)
            self.dynamic_value_function_constraint(master)

        if self.Y != []:

            self.not_in_Y_constraints(master)

    def add_callbacks(self,master):

        # Node Callbacks

        if master.solution_method in [9,10,11,12,17,18,19,20]:

            self.node_callback = master.prob.register_callback(node_callback)
            self.node_callback.node_count = 0
            

        # Branch Callbacks

        if master.solution_method in [9,10]:

            self.branch_callback = master.prob.register_callback(branch_callback)
            self.branch_callback.node_count = 0
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [11]:

            self.branch_callback = master.prob.register_callback(DeltaBranching)
            self.branch_callback.node_count = 0
            self.branch_callback.n = master.n
            self.branch_callback.supply = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.c         = master.c
            self.branch_callback.var_list = self.var_list
            self.branch_callback.log_values = self.log_values
            self.branch_callback.p_names = self.p_names
            self.branch_callback.y_names = self.y_names
            self.branch_callback.x_names = self.x_names
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [12]:

            self.branch_callback = master.prob.register_callback(KSBDeltaBranching)
            self.branch_callback.node_count = 0
            self.branch_callback.n = master.n
            self.branch_callback.supply = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.c         = master.c
            self.branch_callback.var_list = self.var_list
            self.branch_callback.log_values = self.log_values
            self.branch_callback.p_names = self.p_names
            self.branch_callback.y_names = self.y_names
            self.branch_callback.x_names = self.x_names
            self.branch_callback.Y_init_size = master.ksb_no_sols
            self.branch_callback.u_values = []
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.Y_init     = []
            self.branch_callback.sol_count  = 0
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.no_fol_prob_solves = 0
 
        elif master.solution_method in [17,19,20]:

            self.branch_callback = master.prob.register_callback(DeltaBranching_plus)
            self.branch_callback.node_count = 0
            self.branch_callback.n = master.n
            self.branch_callback.supply = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.c         = master.c
            self.branch_callback.var_list = self.var_list
            self.branch_callback.log_values = self.log_values
            self.branch_callback.p_names = self.p_names
            self.branch_callback.y_names = self.y_names
            self.branch_callback.x_names = self.x_names
            self.branch_callback.no_fol_prob_solves = 0

        elif master.solution_method in [18]:

            self.branch_callback = master.prob.register_callback(KSBDeltaBranching_plus)
            self.branch_callback.node_count = 0
            self.branch_callback.n = master.n
            self.branch_callback.supply = master.supply
            self.branch_callback.i          = master.i
            self.branch_callback.d          = master.d
            self.branch_callback.A          = master.A
            self.branch_callback.b          = master.b
            self.branch_callback.c         = master.c
            self.branch_callback.var_list = self.var_list
            self.branch_callback.log_values = self.log_values
            self.branch_callback.p_names = self.p_names
            self.branch_callback.y_names = self.y_names
            self.branch_callback.x_names = self.x_names
            self.branch_callback.Y_init_size = master.ksb_no_sols
            self.branch_callback.u_values = []
            self.branch_callback.ksb_sols_index = []
            self.branch_callback.Y_init     = []
            self.branch_callback.sol_count  = 0
            self.branch_callback.fol_prob_nodes = 0
            self.branch_callback.branch_count = 0
            self.branch_callback.no_fol_prob_solves = 0

        # Incumbent Callbacks

        if master.solution_method in [11,17,19,20]:

            # Delta Branching
            self.inc_callback = master.prob.register_callback(DeltaIncumbent)
            self.inc_callback.n             = master.n
            self.inc_callback.i             = master.i
            self.inc_callback.d             = master.d
            self.inc_callback.A             = master.A
            self.inc_callback.b             = master.b
            self.inc_callback.c             = master.c
            self.inc_callback.supply        = master.supply
            self.inc_callback.x_names       = self.x_names
            self.inc_callback.xbar_names    = self.xbar_names
            self.inc_callback.y_names       = self.y_names
            self.inc_callback.z_names       = self.z_names
            self.inc_callback.bi_feas       = self.bi_feas
            self.inc_callback.ybar_sols     = []
            self.inc_callback.y_sols        = []
            self.inc_callback.no_fol_prob_solves = 0

        elif master.solution_method in [12,18]:

            # Delta Branching
            self.inc_callback = master.prob.register_callback(KSBDeltaIncumbent)
            self.inc_callback.n             = master.n
            self.inc_callback.i             = master.i
            self.inc_callback.d             = master.d
            self.inc_callback.A             = master.A
            self.inc_callback.b             = master.b
            self.inc_callback.c             = master.c
            self.inc_callback.supply        = master.supply
            self.inc_callback.x_names       = self.x_names
            self.inc_callback.xbar_names    = self.xbar_names
            self.inc_callback.y_names       = self.y_names
            self.inc_callback.z_names       = self.z_names
            self.inc_callback.bi_feas       = self.bi_feas
            self.inc_callback.ybar_sols     = []
            self.inc_callback.y_sols        = []
            self.inc_callback.no_fol_prob_solves = 0

    def solve(self,master,Y=[],min_obj=0):

        self.Y          = Y
        self.min_obj    = min_obj

        if master.solution_method in [9]:
            # Fixed column generation
            results = self.fixed_solve_column_generation(master)

        elif master.solution_method in [10]:
            # Dynamic column generation
            results = self.dynamic_solve_column_generation(master)

        elif master.solution_method in [11,12,17,18,19,20]:
            # Delta branching
            results = self.delta_branching(master)

        elif master.solution_method in [12]:
            # Delta branching with KSB
            results = self.delta_branching(master)

        return results

    def fixed_solve_column_generation(self,master):

        while True:
            self.columns += 1

            print("\n")
            print("Iteration:",self.columns)

            self.create_problem(master)

            master.prob.solve()

            if master.prob.solution.get_status() not in [101,102]:

                return [["Status",          master.prob.solution.get_status()],
                        ["Obj",             self.sol],
                        ["Bi_Feas",         self.bi_feas],
                        ["MIP_Gap",         self.mip_gap],
                        ["No_Lazy_Calls",   0],
                        ["No_Lazy_Cuts",    0],
                        ["No_Nodes",        self.total_no_nodes],
                        ["No_Branch_Nodes", self.total_branch_nodes],
                        ["Fol_Node_Count",  self.fol_node_count],
                        ["No_KSB_Sols",     0],
                        ["No_Lambda_Sols",  0],
                        ["Stage_2_Feas",    "NAP"],
                        ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

            if np.max([0,3600 - (time.time() - master.start_time)]) <= 0:
                self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
                break

            fol_prob = MS_FOL_PROB(master.n,master.i,master.d,master.A,master.b,
                                    master.prob.solution.get_values(self.x_names),
                                    master.prob.solution.get_values(self.xbar_names),
                                    master.c,
                                    master.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()
            self.no_fol_prob_solves += 1

            self.fol_node_count += fol_nodes

            cur_obj = np.sum([master.prob.solution.get_values(self.z_names[j])*(master.prob.solution.get_values(self.x_names[j])) for j in range(master.n)]) + \
                            np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*master.c[j] for j in range(master.n)])
            print("fol_obj", fol_obj)
            print("cur_obj", cur_obj)

            self.fol_node_count += fol_nodes
            self.total_no_nodes += self.node_callback.node_count
            self.total_branch_nodes += self.branch_callback.node_count

            if fol_obj <= cur_obj - 0.1:
                print("Bilevel Infeasible")
                print("Add Column")

                # if fol_ybar_sol in self.ybartilde_sols and :
                #     print("Same as previous solution")
                #     print(hi)
                self.sol = master.prob.solution.get_objective_value()
                self.ybartilde_sols.append(fol_ybar_sol)
                self.ytilde_sols.append(fol_y_sol)                

            else:
                print("\n")
                print("Bilevel Feasible")
                self.sol = master.prob.solution.get_objective_value()
                self.bi_feas = True
                self.mip_gap = 0
                break

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             self.sol],
                ["Bi_Feas",         self.bi_feas],
                ["MIP_Gap",         self.mip_gap],
                ["No_Lazy_Calls",   0],
                ["No_Lazy_Cuts",    0],
                ["No_Nodes",        self.total_no_nodes],
                ["No_Branch_Nodes", self.total_branch_nodes],
                ["Fol_Node_Count",  self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  0],
                ["Stage_2_Feas",    "NAP"],
                ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

    def dynamic_solve_column_generation(self,master):

        while True:
            self.columns += 1

            print("\n")
            print("Iteration:",self.columns)

            self.create_problem(master)

            master.prob.solve()   

            if master.prob.solution.get_status() not in [101,102]:

                print("BiFeas?",self.bi_feas)

                return [["Status",          master.prob.solution.get_status()],
                        ["Obj",             self.sol],
                        ["Bi_Feas",         self.bi_feas],
                        ["MIP_Gap",         self.mip_gap],
                        ["No_Lazy_Calls",   0],
                        ["No_Lazy_Cuts",    0],
                        ["No_Nodes",        self.total_no_nodes],
                        ["No_Branch_Nodes", self.total_branch_nodes],
                        ["Fol_Node_Count",  self.fol_node_count],
                        ["No_KSB_Sols",     0],
                        ["No_Lambda_Sols",  0],
                        ["Stage_2_Feas",    "NAP"],
                        ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]


            if np.max([0,3600 - (time.time() - master.start_time)]) <= 0:
                self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
                break

            fol_prob = MS_FOL_PROB(master.n,master.i,master.d,master.A,master.b,
                                    master.prob.solution.get_values(self.x_names),
                                    master.prob.solution.get_values(self.xbar_names),
                                    master.c,
                                    master.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()
            self.no_fol_prob_solves += 1

            self.fol_node_count += fol_nodes
            self.total_no_nodes += self.node_callback.node_count
            self.total_branch_nodes += self.branch_callback.node_count

            cur_obj = np.sum([master.prob.solution.get_values(self.z_names[j])*(master.prob.solution.get_values(self.x_names[j])) for j in range(master.n)]) + \
                            np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*master.c[j] for j in range(master.n)])
            print("fol_obj", fol_obj)
            print("cur_obj", cur_obj)

            if fol_obj <= cur_obj - 0.1:
                print("Bilevel Infeasible")
                print("Add Column")
                self.sol = master.prob.solution.get_objective_value()
                if list(np.array(fol_ybar_sol) + np.array(fol_y_sol)) in self.dyn_y_sols:
                    print("already got this")
                    print(hi)

                self.dyn_y_sols.append(list(np.array(fol_ybar_sol) + np.array(fol_y_sol)))

            else:
                print("\n")
                print("Bilevel Feasible")
                self.sol = master.prob.solution.get_objective_value()
                self.bi_feas = True
                self.mip_gap = 0
                break

        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             self.sol],
                ["Bi_Feas",         self.bi_feas],
                ["MIP_Gap",         self.mip_gap],
                ["No_Lazy_Calls",   0],
                ["No_Lazy_Cuts",    0],
                ["No_Nodes",        self.total_no_nodes],
                ["No_Branch_Nodes", self.total_branch_nodes],
                ["Fol_Node_Count",  self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  0],
                ["Stage_2_Feas",    "NAP"],
                ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves]]

    def delta_branching(self,master):

        self.create_problem(master)

        master.prob.parameters.preprocessing.reduce.set(0)

        master.prob.solve()

        try:
            self.sol = master.prob.solution.get_objective_value()
        except:
            self.sol = -1

        try:
            self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
        except:
            self.mip_gap = -1

        try:
            if master.prob.solution.get_status() in [101,102]:

                self.bi_feas = True

            else:

                self.bi_feas = False

        except:

            self.bi_feas = False


        return [["Status",          master.prob.solution.get_status()],
                ["Obj",             self.sol],
                ["Bi_Feas",         self.bi_feas],
                ["MIP_Gap",         self.mip_gap],
                ["No_Lazy_Calls",   0],
                ["No_Lazy_Cuts",    0],
                ["No_Nodes",        self.node_callback.node_count],
                ["No_Branch_Nodes", self.branch_callback.node_count],
                ["Fol_Node_Count",  self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  0],
                ["Stage_2_Feas",    "NAP"],
                ["No_Fol_Prob_Solves",self.no_fol_prob_solves + self.branch_callback.no_fol_prob_solves + self.inc_callback.no_fol_prob_solves]]

    # Variables
    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(j) for j in range(master.n)]

        self.var_list += self.xbar_names

        master.prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [float(master.supply[j]) for j in range(master.n)],
                                  types = "I" * master.n,
                                  names = self.xbar_names)

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(j) for j in range(master.n)]

        self.var_list += self.x_names

        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_y_vars(self,master):

        self.y_names = [[["y_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                            for k in range(master.d[i])]\
                                                                            for i in range(master.i)]

        # self.ybar_df = pd.DataFrame()

        # # Add variables
        for i in range(master.i):
            for k in range(master.d[i]):
                self.var_list += self.y_names[i][k]
                master.prob.variables.add(obj= [0] * master.n,
                                        lb = [0] * master.n,
                                        ub = [1] * master.n,
                                        types = "B" * master.n,
                                        names = self.y_names[i][k])
    
    def add_gamma_vars(self,master):

        self.gamma_names = ["gamma_" + str(j) for j in range(master.n)]

        self.var_list += self.gamma_names

        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [1] * master.n,
                                  types = "B" * master.n,
                                  names = self.gamma_names)
    
    def add_z_names(self,master):

        self.z_names = ["z_" + str(j) for j in range(master.n)]

        self.var_list += self.z_names

        master.prob.variables.add(obj   = master.c,
                                  lb    = [0] * master.n,
                                  ub    = [float(master.supply[j]) for j in range(master.n)],
                                  types = "C" * master.n,
                                  names = self.z_names)

    def add_alpha_vars(self,master):

        self.log_values = [int(np.floor(np.log2(j))) for j in master.supply]

        self.alpha_names = [["alpha_" + str(j) + "_" + str(l) for l in range(1+self.log_values[j])]\
                                                                            for j in range(master.n)]

        for j in range(master.n):

            self.var_list += self.alpha_names[j]

            master.prob.variables.add(obj   = [0] * (1+self.log_values[j]),
                                        lb    = [0] * (1+self.log_values[j]),
                                        ub    = [1] * (1+self.log_values[j]),
                                        types = "B" * (1+self.log_values[j]),
                                        names = self.alpha_names[j])    

    def add_p_vars(self,master):

        self.p_names = [["p_" + str(j) + "_" + str(l) for l in range(1+self.log_values[j])]\
                                                                            for j in range(master.n)]

        for j in range(master.n):

            self.var_list += self.p_names[j]

            master.prob.variables.add(obj   = [(2**l) for l in range(1+self.log_values[j])],
                                      lb    = [0] * (1+self.log_values[j]),
                                      ub    = [master.M] * (1+self.log_values[j]),
                                      types = "C" * (1+self.log_values[j]),
                                      names = self.p_names[j])

    def add_delta_vars(self,master):

        self.delta_names = [["delta_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                for sol in range(len(self.ytilde_sols))]

        self.deltaybar_names = [["deltaybar_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                for sol in range(len(self.ytilde_sols))]

        self.deltay_names = [["deltay_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                for sol in range(len(self.ytilde_sols))]

        for sol in range(len(self.ytilde_sols)):
            self.var_list += self.deltaybar_names[sol]
            self.var_list += self.deltay_names[sol]
            self.var_list += self.delta_names[sol]

            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = ["B"] * master.n,
                                    names = self.deltaybar_names[sol])
            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = ["B"] * master.n,
                                    names = self.deltay_names[sol])
            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = ["B"] * master.n,
                                    names = self.delta_names[sol])

    def add_ztilde_vars(self,master):

        self.ztilde_names = [["ztilde_" + str(sol) + "_" + str(j) for j in range(master.n)] for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            self.var_list += self.ztilde_names[sol]
            master.prob.variables.add(obj    = [0] * master.n,
                                        lb     = [0] * master.n,
                                        ub     = [float(master.supply[j]) for j in range(master.n)],
                                        types  = "I" * master.n,
                                        names  = self.ztilde_names[sol])

    def add_dynamic_delta_vars(self,master):

        self.dynamic_delta_names = [["delta_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [1] * master.n,
                                      types = "B" * master.n,
                                      names = self.dynamic_delta_names[sol])

    def add_q_vars(self,master):

        self.q_names = [[["q_" + str(sol) + "_" + str(j) + "_" + str(l) for l in range(1+self.beta_log_values[sol][j])] for j in range(master.n)] for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                self.var_list += self.q_names[sol][j]
                master.prob.variables.add(obj    = [0] * (1+self.beta_log_values[sol][j]),
                                        lb     = [0] * (1+self.beta_log_values[sol][j]),
                                        ub     = [master.M] * (1+self.beta_log_values[sol][j]),
                                        types  = "C" * (1+self.beta_log_values[sol][j]),
                                        names  = self.q_names[sol][j])

    def add_dynamic_delta_vars(self,master):

        self.dynamic_delta_names = [["delta_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [1] * master.n,
                                      types = "B" * master.n,
                                      names = self.dynamic_delta_names[sol])

    def add_t_vars(self,master):

        self.t_names = [["t_" + str(sol) + "_" + str(j) for j in range(master.n)]\
                                                                        for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [float(master.supply[j]) for j in range(master.n)],
                                      types = "C" * master.n,
                                      names = self.t_names[sol])

    def add_beta_vars(self,master):

        self.beta_log_values = [[0 for j in range(master.n)] for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                if self.dyn_y_sols[sol][j] >= 1:
                    self.beta_log_values[sol][j] = int(np.floor(np.log2(self.dyn_y_sols[sol][j])))
                else:
                    self.beta_log_values[sol][j] = 0

        self.beta_names = [[["beta_" + str(sol) + "_" + str(j) + "_" + str(l) for l in range(1+self.beta_log_values[sol][j])]\
                                                                            for j in range(master.n)]\
                                                                            for sol in range(len(self.dyn_y_sols))]

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                self.var_list += self.beta_names[sol][j]
                master.prob.variables.add(obj   = [0] * (1+self.beta_log_values[sol][j]),
                                            lb    = [0] * (1+self.beta_log_values[sol][j]),
                                            ub    = [1] * (1+self.beta_log_values[sol][j]),
                                            types = "B" * (1+self.beta_log_values[sol][j]),
                                            names = self.beta_names[sol][j])

    def add_not_in_Y_vars(self,master):

        self.Y_lessthan     = [["YLT_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(len(self.Y))]
        self.Y_greaterthan  = [["YGT_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(len(self.Y))]

        for l in range(len(self.Y)):

            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [1] * master.n,
                                      types = "B" * master.n,
                                      names = self.Y_lessthan[l])

            master.prob.variables.add(obj   = [0] * master.n,
                                      lb    = [0] * master.n,
                                      ub    = [1] * master.n,
                                      types = "B" * master.n,
                                      names = self.Y_greaterthan[l])


    # Constraints
    def budget_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                         senses = ["L"],
                                         rhs = [master.B],
                                         names = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def group_constraints(self,master):

        for i in range(master.i):
            for k in range(master.d[i]):
                for j in range(np.shape(master.A[i])[0]):
                    master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                                      val=list(master.A[i][j,:]))],
                                                        senses  = ["G"],
                                                        rhs     = [master.b[i][j]])

    def follower_supply_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                        val=[1 for i in range(master.i) for k in range(master.d[i])])],
                                             senses = ["L"],
                                             rhs = [float(master.supply[j])],
                                             names = ["Fol_supply_" + str(j)])

    def gamma_constraints(self,master):

        self.M_gamma = 2*max(master.supply)

        for j in range(master.n):

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                    [self.xbar_names[j]] + \
                                                                                    [self.gamma_names[j]],
                                                                val = [-1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                        [-1] + \
                                                                        [float(self.M_gamma)])],
                                    senses   = ["L"],
                                    rhs      = [float(self.M_gamma - master.supply[j])])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.gamma_names[j]] + \
                                                                                    [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                    [self.xbar_names[j]],
                                                                            val = [-float(self.M_gamma)] + \
                                                                                    [1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                    [1])],
                                                senses   = ["L"],
                                                rhs      = [float(master.supply[j])])

    def z_constraints(self,master):

        for j in range(master.n):

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                    [self.xbar_names[j]] + \
                                                                                    [self.z_names[j]],
                                                                val = [1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                        [1] + \
                                                                        [-1])],
                                    senses   = ["L"],
                                    rhs      = [float(master.supply[j])])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[j],self.gamma_names[j]],
                                                                val = [1,-float(master.supply[j])])],
                                    senses   = ["L"],
                                    rhs      = [0])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[j]] + \
                                                                                    [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                    [self.xbar_names[j]] + \
                                                                                    [self.gamma_names[j]],
                                                                val = [1] + \
                                                                        [-1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                        [-1] + \
                                                                        [float(master.supply[j])])],
                                    senses   = ["L"],
                                    rhs      = [0])

    def deltaybar_constraints(self,master):

        self.M_bar = 2*max(master.supply)

        for sol in range(len(self.ytilde_sols)):

            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltaybar_names[sol][j],self.xbar_names[j]],
                                                                                val = [-float(self.M_bar),1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.ybartilde_sols[sol][j] - 0.5)])     


                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltaybar_names[sol][j],self.xbar_names[j]],
                                                                                val = [float(self.M_bar),-1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.M_bar + 0.5 - self.ybartilde_sols[sol][j])])

    def deltay_constraints(self,master):

        for sol in range(len(self.ytilde_sols)):

            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltay_names[sol][j],self.xbar_names[j]],
                                                                                val = [-float(self.M_bar),-1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.ytilde_sols[sol][j] - 0.5 - master.supply[j])])     


                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.deltay_names[sol][j],self.xbar_names[j]],
                                                                                val = [float(self.M_bar),1])],
                                                        senses   = ["L"],
                                                        rhs      = [float(self.M_bar + 0.5 + master.supply[j] - self.ytilde_sols[sol][j])])

    def fixed_value_function_constraint(self,master):

        self.M_delta = master.M * master.n

        for sol in range(len(self.ytilde_sols)):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.p_names[j][l] for j in range(master.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(master.n) for l in range(1+self.log_values[j])]

            IND += [self.y_names[i][k][j]  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]            
            VAL += [float(master.c[j])  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

            IND += self.x_names
            VAL += [-self.ybartilde_sols[sol][j] for j in range(master.n)]

            IND += self.delta_names[sol]
            VAL += [self.M_delta for j in range(master.n)]

            RHS += np.sum([self.ybartilde_sols[sol][j]*master.c[j] for j in range(master.n)])
            RHS += np.sum([self.ytilde_sols[sol][j]*master.c[j] for j in range(master.n)])
            RHS += np.sum([self.M_delta for j in range(master.n)])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                                val = VAL)],
                                                    senses   = ["L"],
                                                    rhs      = [float(RHS)],
                                                    names    = ["VFC_" + str(sol)])
 
    def dynamic_delta_constraints(self,master):

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.dynamic_delta_names[sol][j]] + \
                                                                                        [self.xbar_names[j]],
                                                                                val = [float(master.supply[j])] + \
                                                                                        [-1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(self.dyn_y_sols[sol][j])])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.dynamic_delta_names[sol][j]] + \
                                                                                        [self.xbar_names[j]],
                                                                                val = [float(-master.supply[j])] + \
                                                                                        [1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(master.supply[j] - self.dyn_y_sols[sol][j])])


                # master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.dynamic_delta_names[sol][j]] + \
                #                                                                       [self.xbar_names[j]],
                #                                                                 val = [float(master.supply[j])] + \
                #                                                                       [-1])],
                #                                    senses   = ["L"],
                #                                    rhs      = [float(self.dyn_y_sols[sol][j])])

                # master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.dynamic_delta_names[sol][j]] + \
                #                                                                       [self.xbar_names[j]],
                #                                                                 val = [-float(master.supply[j])] + \
                #                                                                       [1])],
                #                                    senses   = ["L"],
                #                                    rhs      = [-float(self.dyn_y_sols[sol][j] + master.supply[j])])

    def ztilde_constraints(self,master):

        for sol in range(len(self.dyn_y_sols)):
            for j in range(master.n):

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.ztilde_names[sol][j],self.dynamic_delta_names[sol][j]],
                                                                                val = [1,float(-master.supply[j])])],
                                                   senses   = ["L"],
                                                   rhs      = [0])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.ztilde_names[sol][j],self.xbar_names[j]],
                                                                                val = [-1,1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(master.supply[j] - self.dyn_y_sols[sol][j])])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.ztilde_names[sol][j],self.dynamic_delta_names[sol][j],self.xbar_names[j]],
                                                                                val = [1,float(master.supply[j]),-1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(self.dyn_y_sols[sol][j])])

    def dynamic_value_function_constraint(self,master):

        for sol in range(len(self.dyn_y_sols)):

            IND = []
            VAL = []
            RHS = 0

            IND += [self.p_names[j][l] for j in range(master.n) for l in range(1+self.log_values[j])]
            VAL += [2**l for j in range(master.n) for l in range(1+self.log_values[j])]

            IND += [self.y_names[i][k][j]  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]            
            VAL += [float(master.c[j])  for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

            IND += [self.q_names[sol][j][l] for j in range(master.n) for l in range(1+self.beta_log_values[sol][j])]
            VAL += [-2**l for j in range(master.n) for l in range(1+self.beta_log_values[sol][j])]

            # IND += [self.ytilde_names[sol][i][k][j] for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]
            # VAL += [-float(master.c[j]) for i in range(master.i) for k in range(master.d[i]) for j in range(master.n)]

            RHS += float(np.sum([self.dyn_y_sols[sol][j]*master.c[j] for j in range(master.n)]))

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                                val = VAL)],
                                                    senses   = ["L"],
                                                    rhs      = [float(RHS)],
                                                    names    = ["VFC_" + str(sol)])

    def min_objective_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.p_names[j][l] for j in range(master.n) for l in range(1+self.log_values[j])] + \
                                                                              [self.z_names[j] for j in range(master.n)] + \
                                                                              [self.xbar_names[j] for j in range(master.n)],
                                                                        val = [float(2**l) for j in range(master.n) for l in range(1+self.log_values[j])] + \
                                                                              [float(master.c[j]) for j in range(master.n)] + \
                                                                              [float(-master.c[j]) for j in range(master.n)])],
                                           senses   = ["G"],
                                           rhs      = [self.min_obj])

    def not_in_Y_constraints(self,master):

        for l in range(len(self.Y)):

            for j in range(master.n):

                # Greater than variables
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.Y_greaterthan[l][j]] + \
                                                                                      [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                                val = [float(master.supply[j])] + \
                                                                                      [1 for i in range(master.i) for k in range(master.d[i])])],
                                                senses   = ["G"],
                                                rhs      = [float(self.Y[l][j] - 0.5)])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.Y_greaterthan[l][j]] + \
                                                                                      [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                                val = [-float(master.supply[j])] + \
                                                                                      [-1 for i in range(master.i) for k in range(master.d[i])])],
                                                senses   = ["G"],
                                                rhs      = [float(-self.Y[l][j] + 0.5 - master.supply[j])])

                # Less than variables
                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.Y_lessthan[l][j]] + \
                                                                                      [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                                val = [float(master.supply[j])] + \
                                                                                      [-1 for i in range(master.i) for k in range(master.d[i])])],
                                                senses   = ["G"],
                                                rhs      = [float(-self.Y[l][j] - 0.5)])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.Y_lessthan[l][j]] + \
                                                                                      [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                                val = [-float(master.supply[j])] + \
                                                                                      [1 for i in range(master.i) for k in range(master.d[i])])],
                                                senses   = ["G"],
                                                rhs      = [float(self.Y[l][j] + 0.5 - master.supply[j])])

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.Y_greaterthan[l] + self.Y_lessthan[l],
                                                                        val = [1] * (2*master.n))],
                                           senses   = ["G"],
                                           rhs      = [1])

    # Postprocessing
    def display_solution(self,master):

        print("Objective:",master.prob.solution.get_objective_value())
        print("INC      :",np.sum([np.sum([(2**l)*master.prob.solution.get_values(self.p_names[j][l]) for l in range(1+self.log_values[j])]) + master.c[j]*master.prob.solution.get_values(self.z_names[j]) for j in range(master.n)]))
        print("EXP      :",np.sum([master.c[j] * master.prob.solution.get_values(self.xbar_names[j]) for j in range(master.n)]))
        print("Fol Obj  :",np.sum([master.prob.solution.get_values(self.z_names[j])*(master.c[j] + master.prob.solution.get_values(self.x_names[j])) for j in range(master.n)]) + \
                            np.sum([np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])])*master.c[j] for j in range(master.n)]))

        vstack = np.vstack((master.supply,
                            master.prob.solution.get_values(self.xbar_names),
                            master.prob.solution.get_values(self.x_names),
                            master.c,
                            [np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)],
                            master.prob.solution.get_values(self.gamma_names),
                            master.prob.solution.get_values(self.z_names),
                            [np.sum([(2**l)*master.prob.solution.get_values(self.alpha_names[j][l]) for l in range(1+self.log_values[j])]) for j in range(master.n)],
                            [np.sum([(2**l)*master.prob.solution.get_values(self.p_names[j][l]) for l in range(1+self.log_values[j])]) for j in range(master.n)]
                            )).T

        df = pd.DataFrame(vstack,columns=["S","XBAR","X","C","Y","Gamma","Z","Alpha","P"])#,"U"])
        print("\n")
        print(df)











"""
class gamma:

    def __init__(self,master):

        master.prob = cplex.Cplex()
        master.prob.objective.set_sense(master.prob.objective.sense.maximize)
        master.prob.parameters.timelimit.set(3600.0)

        self.add_vars(master)
        self.add_constraints(master)
        self.register_callbacks(master)

    def add_vars(self,master):

        self.add_y_vars(master)
        self.add_x_vars(master)
        self.add_xbar_vars(master)
        self.add_gamma_vars(master)
        self.add_z_vars(master)
        self.add_alpha_vars(master)
        self.add_p_vars(master)

        
    
    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.group_constraints(master)
        self.follower_supply_constraint(master)
        self.gamma_bigM_constraints(master)
        # self.z_constraints(master)

        for j in range(master.n):

            binary_expansion_constraint(master.prob,
                                        [self.z_names[j]],
                                        self.alpha_names[j])

            # for l in range(self.log_values[j]):

            #     mccormick_constraints(master.prob,
            #                           (self.alpha_names[j][l],0,1),
            #                           (self.x_names[j],0,master.M),
            #                           self.p_names[j][l])

    def register_callbacks(self,master):

        pass

    def solve(self,master):

        if master.solution_method in [20]:

            self.solve_column_generation(master)

        self.show_solution(master)

        results = [["HI",0]]

        return results

    def solve_column_generation(self,master):

        while True:

            master.prob.variables.set_lower_bounds([(3,1),(11,1),(26,1),(36,1)])

            master.prob.solve()

            break

    # Variables

    def add_y_vars(self,master):

        self.y_names = [[["y_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(master.n)]\
                                                                            for k in range(master.d[i])]\
                                                                            for i in range(master.i)]

        # # Add variables
        for i in range(master.i):
            for k in range(master.d[i]):
                master.prob.variables.add(obj= [0] * master.n,
                                        lb = [0] * master.n,
                                        ub = [1] * master.n,
                                        types = "B" * master.n,
                                        names = self.y_names[i][k])

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [float(master.supply[j]) for j in range(master.n)],
                                  types = "I" * master.n,
                                  names = self.xbar_names)

    def add_gamma_vars(self,master):

        self.gamma_names = ["gamma_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [1] * master.n,
                                  types = "B" * master.n,
                                  names = self.gamma_names)

    def add_z_vars(self,master):

        self.z_names = ["z_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [float(master.supply[j]) for j in range(master.n)],
                                  types = "I" * master.n,
                                  names = self.z_names)

    def add_alpha_vars(self,master):

        self.log_values = [int(np.floor(np.log2(j))) for j in master.supply]

        self.alpha_names = [["alpha_" + str(j) + "_" + str(l) for l in range(1+self.log_values[j])]\
                                                              for j in range(master.n)]

        for j in range(master.n):
            master.prob.variables.add(obj   = [0] * (1+self.log_values[j]),
                                      lb    = [0] * (1+self.log_values[j]),
                                      ub    = [1] * (1+self.log_values[j]),
                                      types = "B" * (1+self.log_values[j]),
                                      names = self.alpha_names[j])

    def add_p_vars(self,master):

        self.p_names = [["p_" + str(j) + "_" + str(l) for l in range(1+self.log_values[j])]\
                                                              for j in range(master.n)]

        for j in range(master.n):
            master.prob.variables.add(obj   = [2**l for l in range(1+self.log_values[j])],
                                      lb    = [0] * (1+self.log_values[j]),
                                      ub    = [master.M] * (1+self.log_values[j]),
                                      types = "C" * (1+self.log_values[j]),
                                      names = self.p_names[j])

    # Constraints

    def budget_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                         senses = ["L"],
                                         rhs = [master.B],
                                         names = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def group_constraints(self,master):

        for i in range(master.i):
            for k in range(master.d[i]):
                for j in range(np.shape(master.A[i])[0]):
                    master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                                      val=list(master.A[i][j,:]))],
                                                        senses  = ["G"],
                                                        rhs     = [master.b[i][j]])

    def follower_supply_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])],
                                                                            val = [1 for i in range(master.i) for k in range(master.d[i])])],
                                               senses   = ["L"],
                                               rhs      = [float(master.supply[j])])

    def gamma_bigM_constraints(self,master):

        self.gamma_bigM = 2*max(master.supply)

        for j in range(master.n):


            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.gamma_names[j]] + \
                                                                                  [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [self.xbar_names[j]],
                                                                            val = [float(master.supply[j])] + \
                                                                                  [-1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [-1])],
                                               senses   = ["L"],
                                               rhs      = [self.gamma_bigM - master.supply[j] + 0.5])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.gamma_names[j]] + \
                                                                                  [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [self.xbar_names[j]],
                                                                            val = [-float(master.supply[j])] + \
                                                                                  [1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [1])],
                                               senses   = ["L"],
                                               rhs      = [float(master.supply[j] - 0.5)])

    def z_constraints(self,master):

        for j in range(master.n):

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[j]] + \
                                                                                  [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [self.xbar_names[j]],
                                                                            val = [-1] + \
                                                                                  [1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [1])],
                                               senses   = ["L"],
                                               rhs      = [float(master.supply[j])])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[j], self.gamma_names[j]],
                                                                            val = [1,-float(master.supply[j])])],
                                               senses   = ["L"],
                                               rhs      = [0])

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[j]] + \
                                                                                  [self.y_names[i][k][j] for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [self.xbar_names[j]] + \
                                                                                  [self.gamma_names[j]],
                                                                            val = [1] + \
                                                                                  [-1 for i in range(master.i) for k in range(master.d[i])] + \
                                                                                  [-1] + \
                                                                                  [float(master.supply[j])])],
                                               senses   = ["L"],
                                               rhs      = [0])



    # Post processing
    def show_solution(self,master):

        print("Objective:",master.prob.solution.get_objective_value())

        vstack = np.vstack((master.supply,
                            master.prob.solution.get_values(self.xbar_names),
                            master.prob.solution.get_values(self.x_names),
                            master.c,
                            [np.sum([master.prob.solution.get_values(self.y_names[i][k][j]) for i in range(master.i) for k in range(master.d[i])]) for j in range(master.n)],
                            master.prob.solution.get_values(self.z_names),
                            [np.sum([(2**l)*master.prob.solution.get_values(self.alpha_names[j][l]) for l in range(1+self.log_values[j])]) for j in range(master.n)],
                            [master.prob.solution.get_values(self.gamma_names)],
                            [np.sum([(2**l)*master.prob.solution.get_values(self.p_names[j][l]) for l in range(1+self.log_values[j])]) for j in range(master.n)],
                            )).T

        df = pd.DataFrame(vstack,columns=["S","XBAR","X","C","YSUM","Z","ALPHA","GAMMA","P"])#,"U"])
        print("\n")
        print(df)
















"""

"""Waste

master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [],
                                                                val = [])],
                                    senses   = [],
                                    rhs      = [])


"""




