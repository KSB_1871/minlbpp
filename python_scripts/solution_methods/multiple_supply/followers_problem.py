"""
Multiple Supply Followers Problem
"""
import cplex
import numpy as np
import pandas as pd

from cplex.callbacks import NodeCallback

class follower_node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1

class MS_FOL_PROB:

    def __init__(self,n,i,d,A,b,x,xbar,c,supply,show_sol=0):

        self.n = n
        self.i = i
        self.d = d
        self.A = A
        self.b = b
        self.x = x
        self.xbar = xbar
        self.c = c
        self.supply = supply

    def create_instance(self):

        self.fol_prob = cplex.Cplex()
        self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)
        self.fol_prob_settings()

        self.add_variables()
        self.add_constraints()

        self.node_callback = self.fol_prob.register_callback(follower_node_callback)
        self.node_callback.node_count = 0

    def fol_prob_settings(self):
        
        self.fol_prob.set_log_stream(None)
        self.fol_prob.set_error_stream(None)
        self.fol_prob.set_warning_stream(None)
        self.fol_prob.set_results_stream(None)
        self.fol_prob.parameters.mip.display.set(0)

    def add_variables(self):

        self.add_ybar_vars()
        self.add_y_vars()

    def add_constraints(self):

        self.add_group_constraints()
        self.add_supply_constraints()

        self.add_ybar_xbar_constraints()
        self.add_y_s_xbar_constraints()
        self.add_ybar_y_sum_constraints()
        

    def solve_followers_problem(self):

        self.create_instance()
        self.fol_prob.solve()

        return self.fol_prob.solution.get_objective_value(),\
                [np.sum([self.fol_prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],\
                [np.sum([self.fol_prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],\
                self.node_callback.node_count

    def ksb_solve_followers_problem(self,u_values,supply=[]):

        if max(supply) <= 1:

            self.create_instance()

            for udx, u_value in enumerate(u_values):

                j_positive = [j for j in range(self.n) if u_value[j] > 0.5]

                self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in j_positive],
                                                                                  val = [1 for i in range(self.i) for k in range(self.d[i]) for j in j_positive])],
                                                     senses   = ["L"],
                                                     rhs      = [float(len(j_positive) - 1)])
            
            self.fol_prob.solve()

            LB_names = []
            RB_names = []

            # print(self.fol_prob.solution.get_status())

            u_value = [np.sum([self.fol_prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i)\
                                                                                        for k in range(self.d[i])])\
                                                                                        for j in range(self.n)]

            for j in range(self.n):
                if u_value[j] >= 0.5:
                    LB_names += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])]
                    RB_names += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])]

            return LB_names, RB_names, self.node_callback.node_count, u_value

        else:

            # Add any constraints for solutions already found
            self.create_instance()
            # print("2.2")

            # Add u_binary variables
            self.u_pve_binary_names = [["u_pve_binary_" + str(o) + "_" + str(j) for j in range(self.n)] for o in range(len(u_values))]
            self.u_nve_binary_names = [["u_nve_binary_" + str(o) + "_" + str(j) for j in range(self.n)] for o in range(len(u_values))]

            for udx, u_value in enumerate(u_values):

                self.fol_prob.variables.add(obj     = [0] * self.n,
                                            lb      = [0] * self.n,
                                            ub      = [1] * self.n,
                                            types   = "B" * self.n,
                                            names   = self.u_pve_binary_names[udx])

                self.fol_prob.variables.add(obj     = [0] * self.n,
                                            lb      = [0] * self.n,
                                            ub      = [1] * self.n,
                                            types   = "B" * self.n,
                                            names   = self.u_nve_binary_names[udx])


            # Add big M constraints
            for udx,u_value in enumerate(u_values):
                for j in range(self.n):

                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [self.u_pve_binary_names[udx][j]],
                                                                                    val = [1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [-float(self.supply[j])])],
                                                        senses   = ["L"],
                                                        rhs      = [float(u_value[j] + 0.5)])

                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [self.u_pve_binary_names[udx][j]],
                                                                                    val = [-1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [float(self.supply[j])])],
                                                        senses   = ["L"],
                                                        rhs      = [float(-u_value[j] - 0.5 + self.supply[j])])


                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [self.u_nve_binary_names[udx][j]],
                                                                                    val = [-1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [-float(self.supply[j])])],
                                                        senses   = ["L"],
                                                        rhs      = [float(-u_value[j] + 0.5)])

                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [self.u_nve_binary_names[udx][j]],
                                                                                    val = [1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                                            [float(self.supply[j])])],
                                                        senses   = ["L"],
                                                        rhs      = [float(u_value[j] - 0.5 + self.supply[j])])

            # print("2.5")

            # Add greater than 0 constraint for u_binary
            for udx in range(len(u_values)):
                self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.u_pve_binary_names[udx] + self.u_nve_binary_names[udx],
                                                                                val = [1]*(2*self.n))],
                                                    senses   = ["G"],
                                                    rhs      = [1.0])


            # Solve the problem
            self.fol_prob.solve()

            LB_names = []
            RB_names = []

            # print(self.fol_prob.solution.get_status())

            u_value = [np.sum([self.fol_prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i)\
                                                                                        for k in range(self.d[i])])\
                                                                                        for j in range(self.n)]

            # print("2.8")

            for j in range(self.n):
                if u_value[j] >= 0.5:
                    LB_names += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])]
                    RB_names += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])]

            # print("2.9")

            
            return LB_names, RB_names, self.node_callback.node_count, u_value

    def solve_followers_problem_non_comp(self):

        self.create_instance()
        self.fol_prob.solve()

        return self.fol_prob.solution.get_objective_value(),\
                [[[self.fol_prob.solution.get_values(self.ybar_names[i][k][j]) for j in range(self.n)] for k in range(self.d[i])] for i in range(self.i)],\
                [[[self.fol_prob.solution.get_values(self.y_names[i][k][j]) for j in range(self.n)] for k in range(self.d[i])] for i in range(self.i)],\
                self.node_callback.node_count



    # Variables
    def add_ybar_vars(self):

        self.ybar_names = [[["ybar_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(self.n)]\
                                                                            for k in range(self.d[i])]\
                                                                            for i in range(self.i)]

        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj   = [float(self.x[j] + self.c[j]) for j in range(self.n)],
                                            lb    = [0] * self.n,
                                            ub    = [1] * self.n,
                                            types = "B" * self.n,
                                            names = self.ybar_names[i][k])

    def add_y_vars(self):

        self.y_names = [[["y_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(self.n)]\
                                                                            for k in range(self.d[i])]\
                                                                            for i in range(self.i)]

        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj   = [float(self.c[j]) for j in range(self.n)],
                                            lb    = [0] * self.n,
                                            ub    = [1] * self.n,
                                            types = "B" * self.n,
                                            names = self.y_names[i][k])

    # Constraints
    def add_group_constraints(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(np.shape(self.A[i])[0]):
                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.ybar_names[i][k] + self.y_names[i][k],
                                                                                      val=list(self.A[i][j,:]) + list(self.A[i][j,:]))],
                                                        senses  = ["G"],
                                                        rhs     = [self.b[i][j]])
    
    def add_supply_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                            [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                            [1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses = ["L"],
                                             rhs = [float(self.supply[j])],
                                             names = ["Fol_supply_" + str(j)])
 
    def add_ybar_xbar_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses  =["L"],
                                             rhs     =[self.xbar[j]])

    def add_y_s_xbar_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                              val = [1 for i in range(self.i) for k in range(self.d[i])])],
                                                 senses   = ["L"],
                                                 rhs      = [float(self.supply[j] - self.xbar[j])])

    def add_ybar_y_sum_constraints(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(self.n):
                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.ybar_names[i][k][j],self.y_names[i][k][j]],
                                                                                      val = [1,1])],
                                                         senses   = ["L"],
                                                         rhs      = [1])




# class FOL_PROB():

#     def __init__(self,n,i,d,A,b,x,xbar,c,supply,show_sol=0):

#         """
#         Inputs:

#         n   Number of commodities
#         i   Number of follower groups
#         d   Number of followers in each group
#         A   Constraints matrix for all followers
#         b   RHS of constraints for all followers
#         c   Cost assigned to every commodity (x+c if in callback)
#         """
#         self.n,self.i,self.d,self.A,self.b,self.x,self.xbar,self.c,self.supply = n,i,d,A,b,x,xbar,c,supply

#         self.fol_prob = cplex.Cplex()
#         self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)

#         self.fol_prob_settings()

#         self.add_ybar_variables()
#         self.add_y_variables()
        
#         self.group_constraints()
#         self.fol_supply_constraints()
#         self.fol_ybar_supply_constraints()
#         self.fol_y_ybar_equ_constraints()
#         self.fol_y_supply_constraints()

#         self.node_callback = self.fol_prob.register_callback(follower_node_callback)
#         self.node_callback.node_count = 0

#     def fol_prob_settings(self):
        
#         self.fol_prob.set_log_stream(None)
#         self.fol_prob.set_error_stream(None)
#         self.fol_prob.set_warning_stream(None)
#         self.fol_prob.set_results_stream(None)
#         self.fol_prob.parameters.mip.display.set(0)

#     def solve_followers_problem(self):

#         y_opt    = [1,0,1,0,1,1,0,0,0,1,1,0,0,0,0,1,0,0,1,1,0,0,1,0,1,1,0,1,1,1,0,1,0,0,1,1,0,0,0,1,1,0,1,1,1,0,1,0,1,1]
#         ybar_opt = [0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0]

#         self.fol_prob.solve()

#         print("Follower objective without:",self.fol_prob.solution.get_objective_value())
#         print("Opt Fol  objective without:",np.sum((np.array(ybar_opt)*(np.array(self.x)+np.array(self.c))) + (np.array(y_opt)*np.array(self.c))))

#         self.y_opt_names = ["KSB1_" + str(j) for j in range(self.n)]
#         self.ybar_opt_names = ["KSB2_" + str(j) for j in range(self.n)]

#         for j in range(len(y_opt)):
#             self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
#                                                                               val=[1 for i in range(self.i) for k in range(self.d[i])])],
#                                                  senses = ["E"],
#                                                  rhs    = [y_opt[j]],
#                                                  names  = [self.y_opt_names[j]])

#             self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.fol_ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
#                                                                               val=[1 for i in range(self.i) for k in range(self.d[i])])],
#                                                  senses = ["E"],
#                                                  rhs    = [ybar_opt[j]],
#                                                  names  = [self.ybar_opt_names[j]])

#         self.fol_prob.solve()
#         print("Follower objective with  :",self.fol_prob.solution.get_objective_value())

#         self.fol_prob.linear_constraints.delete(self.y_opt_names)
#         self.fol_prob.linear_constraints.delete(self.ybar_opt_names)

#         self.fol_prob.solve()

#         return self.fol_prob.solution.get_objective_value(),\
#                 [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) \
#                                                                                       for k in range(self.d[i])]) \
#                                                                                       for j in range(self.n)],\
#                 [np.sum([self.fol_prob.solution.get_values(self.fol_ybar_names[i][k][j]) for i in range(self.i) \
#                                                                                       for k in range(self.d[i])]) \
#                                                                                       for j in range(self.n)],\
#                 self.node_callback.node_count

#     def add_ybar_variables(self):

#         self.fol_ybar_names = [[["ybar_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(self.n)]\
#                                                                                 for k in range(self.d[i])]\
#                                                                                 for i in range(self.i)]

#         # # Add variables
#         for i in range(self.i):
#             for k in range(self.d[i]):
#                 self.fol_prob.variables.add(obj= [self.x[j] + self.c[j] for j in range(self.n)],
#                                 lb = [0] * len(self.fol_ybar_names[i][k]),
#                                 ub = [1] * len(self.fol_ybar_names[i][k]),
#                                 types = "I" * len(self.fol_ybar_names[i][k]),
#                                 names = self.fol_ybar_names[i][k])

#     def add_y_variables(self):

#         self.fol_y_names = [[["y_" + str(i) + "_" + str(k) + "_" + str(j) for j in range(self.n)]\
#                                                                           for k in range(self.d[i])]\
#                                                                           for i in range(self.i)]

#         # # Add variables
#         for i in range(self.i):
#             for k in range(self.d[i]):
#                 self.fol_prob.variables.add(obj= [self.c[j] for j in range(self.n)],
#                                 lb = [0] * len(self.fol_y_names[i][k]),
#                                 ub = [1] * len(self.fol_y_names[i][k]),
#                                 types = "I" * len(self.fol_y_names[i][k]),
#                                 names = self.fol_y_names[i][k])

#     def group_constraints(self):

#         for i in range(self.i):
#             for k in range(self.d[i]):
#                 for j in range(np.shape(self.A[i])[0]):
#                     self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.fol_ybar_names[i][k] + self.fol_y_names[i][k],
#                                                                                       val=list(self.A[i][j,:]) + list(self.A[i][j,:]))],
#                                                         senses = ["G"],
#                                                         rhs = [self.b[i][j]])

#     def fol_supply_constraints(self):

#         for j in range(self.n):
#             self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind= [self.fol_ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
#                                                                                     [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
#                                                                             val=[1 for i in range(self.i) for k in range(self.d[i])] + \
#                                                                                     [1 for i in range(self.i) for k in range(self.d[i])])],
#                                                  senses  =["L"],
#                                                  rhs     =[float(self.supply[j])])

#     def fol_ybar_supply_constraints(self):

#         for j in range(self.n):
#             self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.fol_ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
#                                                                               val = [1 for i in range(self.i) for k in range(self.d[i])])],
#                                                  senses   = ["L"],
#                                                  rhs      = [float(self.xbar[j])])

#     def fol_y_ybar_equ_constraints(self):

#         for j in range(self.n):
#             for i in range(self.i):
#                 for k in range(self.d[i]):
#                     self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.fol_y_names[i][k][j],self.fol_ybar_names[i][k][j]],
#                                                                                       val = [1,1])],
#                                                          senses   = ["L"],
#                                                          rhs      = [1])

#     def fol_y_supply_constraints(self):

#         for j in range(self.n):
#             self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
#                                                                                 val = [1 for i in range(self.i) for k in range(self.d[i])])],
#                                                     senses   = ["L"],
#                                                     rhs      = [float(self.supply[j] - self.xbar[j])])





# # self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = ,
# #                                                                     val = )],
# #                                         senses   = [],
# #                                         rhs      = [])