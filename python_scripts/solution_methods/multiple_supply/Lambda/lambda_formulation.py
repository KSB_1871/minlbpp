"""
Lambda method Duplicate Supply
02/11/21
"""

import pandas as pd
import numpy as np
import cplex
import time

from useful_functions.useful_functions import mccormick_constraints,binary_expansion_constraint
from solution_methods.multiple_supply.followers_problem import MS_FOL_PROB
from solution_methods.multiple_supply.Lambda.callbacks import *

from solution_methods.multiple_supply.gamma.gamma_formulation import gamma 

class lambda_method:

    def __init__(self,master):

        self.Y                  = []
        self.lambda_size        = 0
        self.fol_node_count     = 0
        self.node_nodes         = 0
        self.branch_nodes       = 0
        self.bi_feas            = False
        self.mip_gap            = 0
        self.stage2_feas        = True
        self.no_fol_prob_solves = 0

    def create_instance(self,master):

        master.prob = cplex.Cplex()
        master.prob.objective.set_sense(master.prob.objective.sense.maximize)
        master.prob.parameters.timelimit.set(np.max([0,3600 - (time.time() - master.start_time)]))

        self.prob_settings(master)

        self.add_vars(master)
        self.add_constraints(master)
        self.register_callbacks(master)

    def prob_settings(self,master):
        
        master.prob.set_log_stream(None)
        master.prob.set_error_stream(None)
        master.prob.set_warning_stream(None)
        master.prob.set_results_stream(None)
        master.prob.parameters.mip.display.set(0)

    def solve(self,master):

        # Get an initial solution
        fol_prob = MS_FOL_PROB(master.n,master.i,master.d,master.A,master.b,
                                    [0] * master.n,
                                    [0] * master.n,
                                    master.c,
                                    master.supply)

        fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()
        self.no_fol_prob_solves += 1

        self.Y.append(fol_y_sol)

        while True:
            self.lambda_size += 1

            print("\n")
            print("Iteration:",self.lambda_size)
            self.create_instance(master)

            master.prob.solve() 

            if master.prob.solution.get_status() not in [101,102]:

                return [["Status",          master.prob.solution.get_status()],
                        ["Obj",             self.sol],
                        ["Bi_Feas",         self.bi_feas],
                        ["MIP_Gap",         self.mip_gap],
                        ["No_Lazy_Calls",   0],
                        ["No_Lazy_Cuts",    0],
                        ["No_Nodes",        self.node_nodes],
                        ["No_Branch_Nodes", self.branch_nodes],
                        ["Fol_Node_Count",  self.fol_node_count],
                        ["No_KSB_Sols",     0],
                        ["No_Lambda_Sols",  0],
                        ["No_Fol_Prob_Solves",  self.no_fol_prob_solves]]
   

            if np.max([0,3600 - (time.time() - master.start_time)]) <= 0:
                self.mip_gap = master.prob.solution.MIP.get_mip_relative_gap()
                break

            fol_prob = MS_FOL_PROB(master.n,master.i,master.d,master.A,master.b,
                                        np.array(master.prob.solution.get_values(self.x_names)),
                                        np.array(master.prob.solution.get_values(self.xbar_names)),
                                        master.c,
                                        master.supply)

            fol_obj, fol_ybar_sol, fol_y_sol, fol_nodes = fol_prob.solve_followers_problem()
            self.no_fol_prob_solves += 1

            self.fol_node_count += fol_nodes
            self.node_nodes += self.node_callback.node_count
            self.branch_nodes += self.branch_callback.node_count

            # costs = [np.sum(np.array(self.Y[l]) * (np.array(master.prob.solution.get_values(self.x_names)) + np.array(master.c))) for l in range(self.lambda_size)]

            # print(costs)

            vstack = np.vstack((master.prob.solution.get_values(self.x_names),
                                             master.prob.solution.get_values(self.xbar_names),
                                             master.supply,
                                             ))

            col_names = ["X","XBAR","S"]

            for l in range(self.lambda_size):

                vstack = np.vstack((vstack,self.Y[l],
                                    master.prob.solution.get_values(self.z_names[l]),
                                    [np.sum([(2**m)*master.prob.solution.get_values(self.p_names[l][j][m]) for m in range(1+self.log_values[j])]) for j in range(master.n)]))
                col_names += ["Y" + str(l),"Z" + str(l),"P" + str(l)]

            res_df = pd.DataFrame(vstack.T,
                                    columns=col_names)

            costs = [np.sum(np.array(master.prob.solution.get_values(self.z_names[l]))*np.array(master.prob.solution.get_values(self.x_names))) + \
                        np.sum(np.array(self.Y[l])*np.array(master.c)) for l in range(self.lambda_size)]

            # print(res_df)
            
            costs2 = [np.sum([(2**(m))*master.prob.solution.get_values(self.p_names[l][j][m]) for j in range(master.n) for m in range(1+self.log_values[j])]) + \
                        np.sum(np.array(self.Y[l])*np.array(master.c)) for l in range(self.lambda_size)]

            print(costs,costs2)

            cur_obj = np.sum(master.prob.solution.get_values(self.lambda_names) * np.array(costs))

            print("Follower Obj:",fol_obj)
            print("Current Obj :",cur_obj)

            if fol_obj <= cur_obj - 0.5:
                print("Bilevel Infeasible")
                print("Add Column")
                self.sol = master.prob.solution.get_objective_value()

                fol_new_sol = list(np.array(fol_ybar_sol) + np.array(fol_y_sol))

                if fol_new_sol in self.Y:

                    print("New solution already in Y!!")
                    print(hi)

                else:

                    self.Y.append(fol_new_sol)

            else:
                print("\n")
                print("Bilevel Feasible")
                self.sol = master.prob.solution.get_objective_value()
                self.bi_feas = True
                self.mip_gap = 0
                break

        lambda_solution = master.prob.solution.get_objective_value()
        lambda_status   = master.prob.solution.get_status()

        # Do next stage
        print("\n")
        print("Stage 2")
      
        stage2 = gamma(master)

        results = stage2.solve(master,self.Y,lambda_solution)

        if results[0][1] == 103:

            self.stage2_feas = False

        if results[2][1] == True:

            # stage2 was solved
            return [["Status",      results[0][1]],
                ["Obj",             results[1][1]],
                ["Bi_Feas",         results[2][1]],
                ["MIP_Gap",         results[3][1]],
                ["No_Lazy_Calls",   results[4][1]],
                ["No_Lazy_Cuts",    results[5][1]],
                ["No_Nodes",        results[6][1] + self.node_nodes],
                ["No_Branch_Nodes", results[7][1] + self.branch_nodes],
                ["Fol_Node_Count",  results[8][1] + self.fol_node_count],
                ["No_KSB_Sols",     0],
                ["No_Lambda_Sols",  self.lambda_size],
                ["Stage_2_Feas",    self.stage2_feas],
                ["No_Fol_Prob_Solves",  self.no_fol_prob_solves + results[12][1]]]


        else:

            # stage 2 not solved
            return [["Status",          lambda_status],
                    ["Obj",             self.sol],
                    ["Bi_Feas",         self.bi_feas],
                    ["MIP_Gap",         self.mip_gap],
                    ["No_Lazy_Calls",   0],
                    ["No_Lazy_Cuts",    0],
                    ["No_Nodes",        self.node_nodes],
                    ["No_Branch_Nodes", self.branch_nodes],
                    ["Fol_Node_Count",  self.fol_node_count],
                    ["No_KSB_Sols",     0],
                    ["No_Lambda_Sols",  self.lambda_size],
                    ["Stage_2_Feas",    self.stage2_feas],
                    ["No_Fol_Prob_Solves",  self.no_fol_prob_solves + results[12][1]]]

    def add_vars(self,master):

        self.add_xbar_vars(master)
        self.add_x_vars(master)
        self.add_gamma_vars(master)
        self.add_z_names(master)
        self.add_lambda_vars(master)
        self.add_alpha_vars(master)
        self.add_p_vars(master)
        self.add_q_vars(master)
        self.add_r_vars(master)

        if master.solution_method == 19:

            # KKT variables
            self.add_mu_vars(master)
            self.add_nu_vars(master)

        elif master.solution_method == 20:

            # VF variables
            pass

    def add_constraints(self,master):

        self.budget_constraint(master)
        self.x_xbar_big_M_constraint(master)
        self.sum_lambda_constraint(master)
        self.gamma_constraints(master)
        self.z_constraints(master)

        for l in range(self.lambda_size):
            for j in range(master.n):

                binary_expansion_constraint(master.prob,[self.z_names[l][j]],self.alpha_names[l][j])

                mccormick_constraints(master.prob,
                                        (self.lambda_names[l],0,1),
                                        (self.z_names[l][j],0,float(master.supply[j])),
                                        self.r_names[l][j])

                for m in range(1+self.log_values[j]):

                    mccormick_constraints(master.prob,
                                            (self.x_names[j],0,master.M),
                                            (self.alpha_names[l][j][m],0,1),
                                            self.p_names[l][j][m])

                    mccormick_constraints(master.prob,
                                            (self.lambda_names[l],0,1),
                                            (self.p_names[l][j][m],0,master.M),
                                            self.q_names[l][j][m])

        if master.solution_method == 19:

            # KKT constraints
            self.KKT_first_order_constraint(master)
            self.KKT_complementarity_constraint(master)

        elif master.solution_method == 20:

            # VF constraints
            self.VF_constraints(master)

    def register_callbacks(self,master):

        self.node_callback = master.prob.register_callback(node_callback)
        self.node_callback.node_count = 0

        self.branch_callback = master.prob.register_callback(branch_callback)
        self.branch_callback.node_count = 0

    # Variables

    def add_xbar_vars(self,master):

        self.xbar_names = ["xbar_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [-float(master.c[j]) for j in range(master.n)],
                                  lb    = [0] * master.n,
                                  ub    = [float(master.supply[j]) for j in range(master.n)],
                                  types = "I" * master.n,
                                  names = self.xbar_names)

    def add_x_vars(self,master):

        self.x_names = ["x_" + str(j) for j in range(master.n)]

        master.prob.variables.add(obj   = [0] * master.n,
                                  lb    = [0] * master.n,
                                  ub    = [master.M] * master.n,
                                  types = "C" * master.n,
                                  names = self.x_names)

    def add_gamma_vars(self,master):

        self.gamma_names = [["gamma_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(self.lambda_size)]

        for l in range(self.lambda_size):

            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [1] * master.n,
                                    types = "B" * master.n,
                                    names = self.gamma_names[l])
    
    def add_z_names(self,master):

        self.z_names = [["z_" + str(l) + "_" + str(j) for j in range(master.n)] for l in range(self.lambda_size)]

        for l in range(self.lambda_size):

            master.prob.variables.add(obj   = [0] * master.n,
                                    lb    = [0] * master.n,
                                    ub    = [float(master.supply[j]) for j in range(master.n)],
                                    types = "C" * master.n,
                                    names = self.z_names[l])

    def add_lambda_vars(self,master):

        self.lambda_names = ["lam_" + str(l) for l in range(self.lambda_size)]

        for l in range(self.lambda_size):
            master.prob.variables.add(obj   = [0],#[np.sum(np.array(self.Y[l])*np.array(master.c))],
                                    lb    = [0],
                                    ub    = [1],
                                    types = "B",
                                    names = [self.lambda_names[l]])

    def add_alpha_vars(self,master):

        self.log_values = [int(np.floor(np.log2(j))) for j in master.supply]

        self.alpha_names = [[["alpha_" + str(l) + "_" + str(j) + "_" + str(m) for m in range(1+self.log_values[j])]
                                                                              for j in range(master.n)]
                                                                              for l in range(self.lambda_size)]

        for l in range(self.lambda_size):
            for j in range(master.n):

                master.prob.variables.add(obj   = [0] * (1+self.log_values[j]),
                                            lb    = [0] * (1+self.log_values[j]),
                                            ub    = [1] * (1+self.log_values[j]),
                                            types = "B" * (1+self.log_values[j]),
                                            names = self.alpha_names[l][j])  

    def add_p_vars(self,master):

        self.p_names = [[["p_" + str(l) + "_" + str(j) + "_" + str(m) for m in range(1+self.log_values[j])]
                                                                              for j in range(master.n)]
                                                                              for l in range(self.lambda_size)]

        for l in range(self.lambda_size):
            for j in range(master.n):

                master.prob.variables.add(obj   = [0] * (1+self.log_values[j]),
                                            lb    = [0] * (1+self.log_values[j]),
                                            ub    = [master.M] * (1+self.log_values[j]),
                                            types = "C" * (1+self.log_values[j]),
                                            names = self.p_names[l][j])  

    def add_q_vars(self,master):

        self.q_names = [[["q_" + str(l) + "_" + str(j) + "_" + str(m) for m in range(1+self.log_values[j])]
                                                                              for j in range(master.n)]
                                                                              for l in range(self.lambda_size)]

        for l in range(self.lambda_size):
            for j in range(master.n):

                master.prob.variables.add(obj     = [2**(m) for m in range(1+self.log_values[j])],
                                            lb    = [0] * (1+self.log_values[j]),
                                            ub    = [master.M] * (1+self.log_values[j]),
                                            types = "C" * (1+self.log_values[j]),
                                            names = self.q_names[l][j])  

    def add_r_vars(self,master):

        self.r_names = [["r_" + str(l) + "_" + str(j) for j in range(master.n)]
                                                       for l in range(self.lambda_size)]

        for l in range(self.lambda_size):

            master.prob.variables.add(obj     = [float(master.c[j]) for j in range(master.n)],
                                        lb    = [0] * master.n,
                                        ub    = [master.M] * master.n,
                                        types = "C" * master.n,
                                        names = self.r_names[l])  

    def add_mu_vars(self,master):

        self.mu_names = ["mu_" + str(l) for l in range(self.lambda_size)]

        master.prob.variables.add(obj   = [0] * self.lambda_size,
                                    lb    = [0] * self.lambda_size,
                                    types = "C" * self.lambda_size,
                                    names = self.mu_names)

    def add_nu_vars(self,master):

        self.nu_names = ["nu"]

        master.prob.variables.add(obj   = [0],
                                    lb = [-np.inf],
                                    ub = [np.inf],
                                    types = "C",
                                    names = self.nu_names)


    # Constraints

    def budget_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = master.c)],
                                           senses   = ["L"],
                                           rhs      = [master.B],
                                           names    = ['Budget'])

    def x_xbar_big_M_constraint(self,master):

        for j in range(master.n):
            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-master.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def sum_lambda_constraint(self,master):

        master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.lambda_names,
                                                                          val = [1] * len(self.lambda_names))],
                                             senses   = ["E"],
                                             rhs      = [1])

    def gamma_constraints(self,master):

        self.M_gamma = 2*max(master.supply)

        for l in range(self.lambda_size):

            for j in range(master.n):

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.gamma_names[l][j],self.xbar_names[j]],
                                                                                val = [float(self.M_gamma),-1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(self.M_gamma + self.Y[l][j] - master.supply[j])])        

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.gamma_names[l][j],self.xbar_names[j]],
                                                                                val = [float(-self.M_gamma),1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(-self.Y[l][j] + master.supply[j])])        

    def z_constraints(self,master):

        for l in range(self.lambda_size):

            for j in range(master.n):

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[l][j],self.xbar_names[j]],
                                                                                val = [-1,1])],
                                                   senses   = ["L"],
                                                   rhs      = [float(-self.Y[l][j] + master.supply[j])])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[l][j],self.gamma_names[l][j]],
                                                                                val = [1,float(-master.supply[j])])],
                                                   senses   = ["L"],
                                                   rhs      = [0.0])

                master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.z_names[l][j],self.xbar_names[j],self.gamma_names[l][j]],
                                                                                val = [1,-1,float(master.supply[j])])],
                                                   senses   = ["L"],
                                                   rhs      = [float(self.Y[l][j])])

    def KKT_first_order_constraint(self,master):

        for l in range(self.lambda_size):

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = [self.p_names[l][j][m] for j in range(master.n) for m in range(1+self.log_values[j])] + \
                                                                                    [self.mu_names[l],self.nu_names[0]],
                                                                            val = [2**(m) for j in range(master.n) for m in range(1+self.log_values[j])] + \
                                                                                    [-1,1])],
                                               senses   = ["E"],
                                               rhs      = [float(-np.sum(np.array(self.Y[l])*np.array(master.c)))])

    def KKT_complementarity_constraint(self,master):

        for l in range(self.lambda_size):
            
            master.prob.SOS.add(type = "1",
                                  SOS  = [[self.mu_names[l],self.lambda_names[l]],[1,2]])

    def VF_constraints(self,master):

        for o in range(self.lambda_size):
            IND = []
            VAL = []
            RHS = 0

            IND += [self.q_names[l][j][m] for l in range(self.lambda_size) for j in range(master.n) for m in range(1+self.log_values[j])]
            VAL += [2**(m) for l in range(self.lambda_size) for j in range(master.n) for m in range(1+self.log_values[j])]

            IND += self.lambda_names
            VAL += [np.sum(np.array(self.Y[l])*np.array(master.c)) for l in range(self.lambda_size)]

            IND += [self.p_names[o][j][m] for j in range(master.n) for m in range(1+self.log_values[j])]
            VAL += [-2**(m) for j in range(master.n) for m in range(1+self.log_values[j])]

            RHS = float(np.sum(np.array(self.Y[o])*np.array(master.c)))

            master.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = IND,
                                                                            val = VAL)],
                                               senses   = ["L"],
                                               rhs      = [RHS])