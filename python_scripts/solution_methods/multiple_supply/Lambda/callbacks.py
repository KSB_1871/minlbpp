"""
Lambda Callbacks
"""

from cplex.callbacks import  IncumbentCallback,NodeCallback,BranchCallback

class node_callback(NodeCallback):

    def __call__(self):

        self.node_count += 1

class branch_callback(BranchCallback):

    def __call__(self):

        self.node_count += self.get_num_branches()