"""
YBar column generation branch callback
"""
import numpy as np
import pandas as pd

from cplex.callbacks import LazyConstraintCallback
from cplex.callbacks import BranchCallback
from ybar_follower_problem import YBAR_FOL_PROB

class YBAR_COLGEN_LAZY(LazyConstraintCallback,BranchCallback):

    def __call__(self):

        # if self.abort_flag == False:
        if 1 == 1:

            self.no_lazy_callbacks += 1

            fol_prob = YBAR_FOL_PROB(self.get_values(self.x_names),self.get_values(self.xbar_names),0)
            fol_prob.x_names = self.x_names
            fol_prob.xbar_names = self.xbar_names
            fol_prob.i = self.i
            fol_prob.d = self.d
            fol_prob.n = self.n
            fol_prob.c = self.c
            fol_prob.supply = self.supply
            fol_prob.A = self.A
            fol_prob.b = self.b

            vstack = []
            col = []            

            zsum        = np.sum([self.get_values(self.z_names[i][k][j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)])
            ybarsum     = np.sum([self.get_values(self.ybar_names[i][k][j])*self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)])
            ysum        = np.sum([self.get_values(self.y_names[i][k][j])*self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)])
            cur_fol_obj = zsum + ybarsum + ysum

            fol_obj, fol_sol, fol_ybar_sol, fol_y_sol = fol_prob.solve_followers_problem()
            print("\n")
            print("CUR RESPONSE:",cur_fol_obj)
            print("FOL RESPONSE:",fol_obj)



            if fol_obj < cur_fol_obj - 0.1:
                print("BILEVEL INFEASIBLE")
                if self.solution_method == "CG":
                    """Column generation approach"""
                    self.no_lazy_cuts += 1
                    self.ybar_sols.append(fol_ybar_sol)
                    self.y_sols.append(fol_y_sol)
                    self.new_solution_ybar = fol_ybar_sol
                    self.new_solution_y    = fol_y_sol
                    # self.delta_count += 1
                    # self.abort_flag = True
                    self.abort()
                    return
        # else:

        #     self.abort_flag = False
        #     self.abort()