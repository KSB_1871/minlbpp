"""
Hpr Leader
"""

import cplex
import numpy as np
import pandas as pd
import time
import json

from ybar_colgen_node_callback import YBAR_COLGEN_NODE
from ybar_colgen_branch_callback import YBAR_COLGEN_BRANCH
from ybar_colgen_lazy_callback import YBAR_COLGEN_LAZY

class Prob():

    def __init__(self):

        print("Prob Class")
        self.prob = cplex.Cplex()
        self.prob.objective.set_sense(self.prob.objective.sense.maximize)
        self.default_settings()

    def default_settings(self):

        print("defulat settings")
        self.solution_display = 1

    def settings_after_json(self):

        np.random.seed(self.seed)

        self.c = [float(np.random.randint(low=1,high=self.n)) for j in range(self.n)]
        self.M = 0.5*self.n
        self.B = self.n*10
        self.d = [self.d for i in range(self.i)]
        self.delta_count = 0
        self.delta_names = []
        self.delta_ybar_sols = []
        self.total_no_nodes = 0
        self.total_no_branch_nodes = 0

        if self.single_supply == True:
            self.supply = np.ones((self.n))
        else:
            self.supply = np.random.randint(low=1,high=np.sum(self.d)+1,size=self.n)
            self.supply.astype(float)


    def solve_problem(self):

        print("SOLVING")
        self.add_vars_to_prob()
        self.add_constraints_to_prob()
        self.display_settings()
        self.solver_settings()

        if self.ksb_method == True:
            self.ksb_sols = []
            self.ksb_sols_index = []

        # self.register_ksb_branching_callback()

        self.register_lazy_callback()
        self.register_node_callback()
        self.register_branch_callback()

        while True:

            # if self.delta_count == 10:
            #     break

            print("SOLVING")
            self.prob.solve()
            print("END SOLVE")
            print("STATUS:",self.prob.solution.get_status())

            if self.solution_method == "CG":

                if self.prob.solution.get_status() in [113,114]:
                    """We have aborted due to bilevel infeasible solution"""
                    print("Delta:",self.delta_count)
                    print(self.lazy_callback.new_solution_ybar)
                    self.delta_ybar_sols.append(self.lazy_callback.new_solution_ybar)
                    self.create_delta_vars()
                    self.delta_constraints()
                    self.value_function_constraints()

                    self.delta_count += 1

                    # print("\n")
                    # print("Delta Ybar Sols",self.delta_count)
                    # for i in range(self.delta_count):
                    #     print(self.delta_ybar_sols[i])



                else:
                    """We have a bilevel optimal solution"""
                    break

        self.end_time = time.time()

        if self.show_solution != 0:
            self.show_solution()

        self.save_results()

    def register_lazy_callback(self):

        self.lazy_callback = self.prob.register_callback(YBAR_COLGEN_LAZY)

        self.lazy_callback.x_names = self.x_names
        self.lazy_callback.xbar_names = self.xbar_names
        self.lazy_callback.i = self.i
        self.lazy_callback.d = self.d
        self.lazy_callback.n = self.n
        self.lazy_callback.c = self.c
        self.lazy_callback.supply = self.supply
        self.lazy_callback.A = self.A
        self.lazy_callback.b = self.b
        self.lazy_callback.ybar_names = self.ybar_names
        self.lazy_callback.y_names = self.y_names
        self.lazy_callback.z_names = self.z_names
        self.lazy_callback.no_lazy_callbacks = 0
        self.lazy_callback.no_lazy_cuts = 0
        self.lazy_callback.solution_method = self.solution_method
        self.lazy_callback.delta_count = self.delta_count
        self.lazy_callback.delta_names = self.delta_names
        self.lazy_callback.delta_ybar_sols = self.delta_ybar_sols
        self.lazy_callback.ybar_sols = []
        self.lazy_callback.y_sols = []
        self.lazy_callback.abort_flag = False
        
    def register_node_callback(self):

        self.node_callback = self.prob.register_callback(YBAR_COLGEN_NODE)
        self.node_callback.node_count = 0
        self.node_callback.xbar_names = self.xbar_names

    def register_branch_callback(self):
        self.branch_callback = self.prob.register_callback(YBAR_COLGEN_BRANCH)
        self.branch_callback.branch_count = 0
        self.branch_callback.branch_node_count = 0
        # self.branch_callback.ksb_method = self.ksb_method
        # self.branch_callback.sol_count = 0
        # self.branch_callback.generate_solutions = self.generate_solutions
        # self.branch_callback.generate_solutions_on_fly = self.generate_solutions_on_fly
        # self.branch_callback.ksb_no_sols = self.ksb_no_sols
        # self.branch_callback.y_df = self.y_df
        # self.branch_callback.ksb_sols = []
        # self.branch_callback.ksb_sols_index = []

    def solver_settings(self):

        self.prob.parameters.preprocessing.linear.set(0)
        self.prob.parameters.mip.strategy.search.set(self.prob.parameters.mip.strategy.search.values.traditional)
        self.prob.parameters.timelimit.set(3600)

    def display_settings(self):

        print("DISPLAY SETTINGS")

        self.prob.parameters.mip.interval.set(1)

        # self.prob.set_log_stream(None)
        # self.prob.set_error_stream(None)
        # self.prob.set_warning_stream(None)
        # self.prob.set_results_stream(None)
        # self.prob.parameters.mip.display.set(0)

    def read_json(self,input_file):

        with open(input_file) as json_file:
            data = json.load(json_file)
            for key in data.keys():
                setattr(self,key,data[key])

    def add_vars_to_prob(self):

        # Add variables
        self.create_xbar_vars()
        self.create_x_vars()
        self.create_ybar_vars()
        self.create_y_vars()

        
        
        self.create_z_vars()

        #self.create_alpha_vars()
        # self.create_beta_vars()
        # self.create_u_vars()

    def add_constraints_to_prob(self):

        self.minimum_objective_constraint()
        self.budget_constraint()
        self.x_xbar_big_M_constraint()
        self.group_constraints()
        self.follower_supply_constraint()
        self.ybar_xbar_constraint()
        self.y_supply_x_constraint()
        self.z_mccormick_constraints()
        # self.u_mccormick_constraints()

    # VARIABLES

    def create_ybar_vars(self):

        self.ybar_names = [[["ybar_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        self.ybar_df = pd.DataFrame()

        # # Add variables
        for i in range(self.i):
            for j in range(self.d[i]):
                self.ybar_df = self.ybar_df.append(self.ybar_names[i][j])
                self.prob.variables.add(obj= [float(self.c[j]) for j in range(self.n)],
                                lb = [0] * len(self.ybar_names[i][j]),
                                ub = [1] * len(self.ybar_names[i][j]),
                                types = "I" * len(self.ybar_names[i][j]),
                                names = self.ybar_names[i][j])

    def create_y_vars(self):

        self.y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        self.y_df = pd.DataFrame()

        # # Add variables
        for i in range(self.i):
            for j in range(self.d[i]):
                self.y_df = self.y_df.append(self.y_names[i][j])
                self.prob.variables.add(obj= [0] * len(self.y_names[i][j]),
                                lb = [0] * len(self.y_names[i][j]),
                                ub = [1] * len(self.y_names[i][j]),
                                types = "I" * len(self.y_names[i][j]),
                                names = self.y_names[i][j])

    def create_x_vars(self):

        self.x_names = ["x_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [0] * self.n,
                                lb = [0] * self.n,
                                ub = [self.M] * self.n,
                                types = "C" * self.n,
                                names = self.x_names)

    def create_xbar_vars(self):

        self.xbar_names = ["xbar_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(obj= [-float(self.c[j]) for j in range(self.n)],
                                lb = [0] * self.n,
                                ub = [float(self.supply[i]) for i in range(self.n)],
                                types = "I" * self.n,
                                names = self.xbar_names)

    def create_z_vars(self):

        self.z_names = [[["z_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        self.z_df = pd.DataFrame()

        # # Add variables
        for i in range(self.i):
            for j in range(self.d[i]):
                self.z_df = self.ybar_df.append(self.z_names[i][j])
                self.prob.variables.add(obj= [1] *self.n,
                                lb = [0] * len(self.z_names[i][j]),
                                ub = [self.M] * len(self.z_names[i][j]),
                                types = "I" * len(self.z_names[i][j]),
                                names = self.z_names[i][j])

    def create_delta_vars(self):

        self.delta_names.append(["delta_" + str(self.delta_count) + "_" + str(j) for j in range(self.n)])

        self.prob.variables.add(obj   = [0] * self.n,
                                lb    = [0] * self.n,
                                ub    = [1] * self.n,
                                types = "B" * self.n,
                                names = self.delta_names[self.delta_count])

    # CONSTRAINTS
    def minimum_objective_constraint(self):

        self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.z_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                                          [self.ybar_names[i][k][j]  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                                          [self.xbar_names[j] for j in range(self.n)],
                                                                      val=[1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                                          [self.c[j]  for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
                                                                          [-self.c[j]  for j in range(self.n)])],
                                         senses   = ["G"],
                                         rhs      = [0])

    def budget_constraint(self):

        self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = self.c)],
                                         senses = ["L"],
                                         rhs = [self.B],
                                         names = ['Budget'])

    def x_xbar_big_M_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-self.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def k_card_constraints(self):

        for i in range(self.i):
            self.A[i] = np.append(self.A[i],np.ones((1,self.n)),axis=0)
            self.b[i] = np.append(self.b[i],np.array([[self.k_card_rhs]]))

    def knapsack_constraints(self):

        for i in range(self.i):
            self.A[i] = np.append(self.A[i],np.array([self.c]),axis=0)
            self.b[i] = np.append(self.b[i],np.array([[self.knapsack_rhs]]))

    def partition_matroid_constraints(self):

        primes = []
        number = 1000
        for num in range(2,number + 1):
            if len(primes) == self.partition_matroid_no:
                break

            if num > 1:
                for i in range(2,num):
                    if (num % i) == 0:
                        break
                else:
                    primes += [num]

        if len(primes) > 0:
            for i in range(self.i):
                self.A[i] = np.append(self.A[i],np.array([[1 if j%prime == 0 else 0 for j in range(self.n)] for prime in primes]),axis=0)
                self.b[i] = np.append(self.b[i],np.ones((self.partition_matroid_no))*self.partition_matroid_rhs)

    def group_constraints(self):

        self.A = [np.zeros((0,self.n)) for i in range(self.i)]
        self.b = [np.zeros((0,1)) for i in range(self.i)]

        self.k_card_constraints()
        self.knapsack_constraints()
        self.partition_matroid_constraints()

        for i in range(self.i):
            self.A[i] = self.A[i].astype(float)
            self.b[i] = self.b[i].astype(float)

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(np.shape(self.A[i])[0]):
                    self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.ybar_names[i][k] + self.y_names[i][k],
                                                                                      val=list(self.A[i][j,:]) + list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = [self.b[i][j]])

    def follower_supply_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                            [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                            [1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses = ["L"],
                                             rhs = [float(self.supply[j])],
                                             names = ["Fol_supply_" + str(j)])

    def ybar_xbar_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + [self.xbar_names[j]],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])] + [-1])],
                                             senses  =["L"],
                                             rhs     =[0])

    def y_supply_x_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + [self.xbar_names[j]],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])] + [1])],
                                             senses  =["L"],
                                             rhs     =[float(self.supply[j])])

    def z_mccormick_constraints(self):

        """
        z^{ik}_j = ybar^{ik}_j * x_j
        """
        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(self.n):

                    self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[i][k][j],self.x_names[j],self.ybar_names[i][k][j]],
                                                                                val=[-1,1,self.M])],
                                                     senses  =["L"],
                                                     rhs     =[self.M])

                    self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[i][k][j],self.ybar_names[i][k][j]],
                                                                                val=[1,-self.M])],
                                                     senses  =["L"],
                                                     rhs     =[0])

                    self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[i][k][j],self.x_names[j]],
                                                                                val=[1,-1])],
                                                     senses  =["L"],
                                                     rhs     =[0])

    def delta_constraints(self):

        self.deltaM = 100
        self.Mbar = [float(self.supply[j]) for j in range(self.n)]

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.xbar_names[j], self.delta_names[self.delta_count][j]],
                                                                        val=[1,-self.Mbar[j]])],
                                            senses   = ["L"],
                                            rhs      = [float(-0.5+self.lazy_callback.new_solution_ybar[j])])

            self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.xbar_names[j], self.delta_names[self.delta_count][j]],
                                                                          val=[-1,self.Mbar[j]])],
                                             senses   = ["L"],
                                             rhs      = [float(0.5+self.Mbar[j]-self.lazy_callback.new_solution_ybar[j])])

    def value_function_constraints(self):

        IND = []
        VAL = []

        """Z"""
        IND += [self.z_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        VAL += [1 for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        """Ybar * C"""
        IND += [self.ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        VAL += [float(self.c[j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        """Y * C"""
        IND += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        VAL += [float(self.c[j]) for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)]
        """Ybar tilde x"""
        IND += [self.x_names[j] for j in range(self.n)]
        VAL += [-float(self.lazy_callback.new_solution_ybar[j]) for j in range(self.n)]
        """Delta M"""
        IND += [self.delta_names[self.delta_count][j] for j in range(self.n)]
        VAL += [self.deltaM for j in range(self.n)]

        RHS = 0
        RHS += np.sum([self.lazy_callback.new_solution_ybar[j]*self.c[j] for j in range(self.n)])
        RHS += np.sum([self.lazy_callback.new_solution_y[j]*self.c[j] for j in range(self.n)])
        RHS += np.sum([self.deltaM for j in range(self.n)])

        self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=IND,val=VAL)],
                                         senses   = ["L"],
                                         rhs      = [float(RHS)],
                                         names    = ["ValFun" + str(self.delta_count)])

    # POST PROCESSING

    def show_solution(self):

        print("STATUS :",self.prob.solution.get_status())

        if self.prob.solution.get_status() == 101:

            self.solution_display = 2
            print("DISPLAY:",self.solution_display)


            if self.solution_display == 1:


                # for i in range(self.i):
                #     for k in range(self.d[i]):
                #         print(self.prob.solution.get_values(self.z_names[i][k]))

                z_sum       = [np.sum(self.prob.solution.get_values(self.z_names[i][k])) for i in range(self.i) for k in range(self.d[i])]
                ybar_sum    = [np.sum([self.prob.solution.get_values(self.ybar_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]
                y_sum       = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]

                print("OBJ    = ",np.round(self.prob.solution.get_objective_value(),decimals=2))
                print("INC    = ",np.round(np.sum(z_sum)+np.sum(ybar_sum),decimals=2))
                print("EXP    = ",np.round(np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]),decimals=2))
                print("F.COST = ",np.round(np.sum(z_sum)+np.sum(ybar_sum)+np.sum(y_sum),decimals=2))

            elif self.solution_display == 2:

                z_sum       = [np.sum(self.prob.solution.get_values(self.z_names[i][k])) for i in range(self.i) for k in range(self.d[i])]
                ybar_sum    = [np.sum([self.prob.solution.get_values(self.ybar_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]
                y_sum       = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j])*self.c[j] for j in range(self.n)]) for i in range(self.i) for k in range(self.d[i])]

                print("OBJ    = ",np.round(self.prob.solution.get_objective_value(),decimals=2))
                print("INC    = ",np.round(np.sum(z_sum)+np.sum(ybar_sum),decimals=2))
                print("EXP    = ",np.round(np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]),decimals=2))
                print("F.COST = ",np.round(np.sum(z_sum)+np.sum(ybar_sum)+np.sum(y_sum),decimals=2))


                # Create VStack

                vstack = np.vstack((self.supply,
                                    self.prob.solution.get_values(self.xbar_names),
                                    self.prob.solution.get_values(self.x_names),
                                    [np.sum([self.prob.solution.get_values(self.z_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    self.c,
                                    [np.sum([self.prob.solution.get_values(self.ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                                    )).T

                # vstack = np.vstack((self.supply,
                #                     self.prob.solution.get_values(self.xbar_names),
                #                     self.prob.solution.get_values(self.x_names),
                #                     self.prob.solution.get_values(self.z_names),
                #                     self.c,
                #                     [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                #                     self.prob.solution.get_values(self.u_names))).T

                df = pd.DataFrame(vstack,columns=["S","XBAR","X","Z","C","YBARSUM","YSUM"])#,"U"])
                print("\n")
                print(df)

            # ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
            # print("YSUM = ",[i for i in range(self.n) if ysum[i] > 0.5])
            # print("XBAR = ",[i for i in range(self.n) if self.prob.solution.get_values(self.xbar_names[i]) > 0.5])

    def save_results(self):
        

        ksb_opt = -1

        try:
            obj = np.around(self.prob.solution.get_objective_value(),decimals=2)
        except:
            obj = -1

        if self.prob.solution.get_status() == 101:

            ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

            if self.ksb_method == True:
                print("\n")
                print("YSUM   = ",[j for j in range(self.n) if ysum[j] > 0.5])
                for i in range(len(self.branch_callback.ksb_sols)):
                    self.branch_callback.ksb_sols_index[i].sort()
                    print("Y ",i,"  = ",self.branch_callback.ksb_sols_index[i])

                if self.branch_callback.branch_node_count == 0:
                    ksb_opt = -2

                if [j for j in range(self.n) if ysum[j] > 0.5] in self.branch_callback.ksb_sols_index:
                    ksb_opt = self.branch_callback.ksb_sols_index.index([j for j in range(self.n) if ysum[j] > 0.5])

        results_data = [["seed",            self.seed],
                        ["single_supply",   self.single_supply],
                        ["KSB_method",      self.ksb_method],
                        ["KSB_no_sols",     self.ksb_no_sols],
                        ["n",               self.n],
                        ["i",               self.i],
                        ["d_sum",           np.sum(self.d)],
                        ["k_card_rhs",      self.k_card_rhs],
                        ["knapsack_rhs",    self.knapsack_rhs],
                        ["par_mat_no",      self.partition_matroid_no],
                        ["par_mat_rhs",     self.partition_matroid_rhs],
                        ["No_nodes",        self.node_callback.node_count],
                        ["No_branch_nodes", self.branch_callback.branch_node_count],
                        ["No_lazy_calls",   self.lazy_callback.no_lazy_callbacks],
                        ["No_lazy_cuts",    self.lazy_callback.no_lazy_cuts],
                        ["Obj",             obj],
                        ["KSB_Sol",         ksb_opt],
                        ["Time",            np.round(self.end_time - self.start_time,decimals=4)],
                        ["Status",          self.prob.solution.get_status()]]

        results_df = pd.DataFrame(results_data).T

        print(results_df.T)

        saved_name = str(self.seed) + "_" + \
                     str(self.single_supply) + "_" + \
                     str(self.ksb_no_sols) + "_" + \
                     str(self.ksb_method) + "_" + \
                     str(self.n) + "_" + \
                     str(self.i) + "_" + \
                     str(np.sum(self.d)) + "_" + \
                     str(self.k_card_rhs) + "_" + \
                     str(self.knapsack_rhs) + "_" + \
                     str(self.partition_matroid_no) + "_" + \
                     str(self.partition_matroid_rhs)

        results_df.to_csv(saved_name + '.txt',sep=";",header=False,index=False)

