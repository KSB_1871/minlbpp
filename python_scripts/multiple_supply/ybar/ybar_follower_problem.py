"""
YBar Followers problem
"""

import cplex
import numpy as np
import pandas as pd

class YBAR_FOL_PROB():

    def __init__(self,x,xbar,show_sol=0):

        self.fol_prob = cplex.Cplex()
        self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)

        self.x = x
        self.xbar = xbar
        self.show_sol = show_sol

    def solve_followers_problem(self):

        self.add_y_ybar_variables()
        self.group_constraints()
        self.fol_supply_constraints()
        self.ybar_xbar_constraint()
        self.ybar_y_constraint()
        self.y_supply_xbar_constraint()
        self.fol_prob_settings()
        self.fol_prob.solve()

        if self.show_sol == 1:
            self.fol_show_solutions()

        obj_value   = self.fol_prob.solution.get_objective_value()
        sol         = [np.sum([self.fol_prob.solution.get_values(self.fol_ybar_names[i][k][j]) + self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
        ybar_sol    = [np.sum([self.fol_prob.solution.get_values(self.fol_ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
        y_sol       = [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

        return obj_value, sol, ybar_sol, y_sol

    def fol_prob_settings(self):
        self.fol_prob.set_log_stream(None)
        self.fol_prob.set_error_stream(None)
        self.fol_prob.set_warning_stream(None)
        self.fol_prob.set_results_stream(None)
        self.fol_prob.parameters.mip.display.set(0)

    def add_y_ybar_variables(self):

        self.fol_ybar_names = [[["ybar_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        # # Add variables
        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj= [self.c[j] + self.x[j] for j in range(self.n)],
                                lb = [0] * self.n,
                                ub = [1] * self.n,
                                types = "I" * self.n,
                                names = self.fol_ybar_names[i][k])

        self.fol_y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        # # Add variables
        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj= [self.c[j] for j in range(self.n)],
                                lb = [0] * len(self.fol_y_names[i][k]),
                                ub = [1] * len(self.fol_y_names[i][k]),
                                types = "I" * len(self.fol_y_names[i][k]),
                                names = self.fol_y_names[i][k])

    def group_constraints(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(np.shape(self.A[i])[0]):
                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.fol_ybar_names[i][k] + self.fol_y_names[i][k],
                                                                                      val=list(self.A[i][j,:]) + list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = [self.b[i][j]])

    def fol_supply_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + \
                                                                            [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])] + \
                                                                            [1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses = ["L"],
                                             rhs = [float(self.supply[j])],
                                             names = ["Fol_supply_" + str(j)])

    def ybar_xbar_constraint(self):
        
        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_ybar_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses  =["L"],
                                             rhs     =[self.xbar[j]])

    def ybar_y_constraint(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(self.n):
                    self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_ybar_names[i][k][j],self.fol_y_names[i][k][j]],
                                                                                    val=[1,1])],
                                                         senses  =["L"],
                                                         rhs     =[1])

    def y_supply_xbar_constraint(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                            val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                                 senses  =["L"],
                                                 rhs     =[float(self.supply[j] - self.xbar[j])])

    def fol_show_solutions(self):

        print("\n")
        print("FOL PROB OBJ:",self.fol_prob.solution.get_objective_value())

        vstack = np.vstack((self.supply,
                            self.x,
                            self.xbar,
                            self.c,
                            [np.sum([self.fol_prob.solution.get_values(self.fol_ybar_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                            [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                            [np.sum([self.fol_prob.solution.get_values(self.fol_ybar_names[i][k][j]) + self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                            )).T
        df = pd.DataFrame(vstack,columns=["S","X","XBAR","C","YBARSUM","YSUM","YTOT"])

        print(df)


















