"""
YBar Column Generation Node Callback
"""

import cplex
from cplex.callbacks import  NodeCallback

class YBAR_COLGEN_NODE(NodeCallback):

    def __call__(self):

        self.node_count += 1