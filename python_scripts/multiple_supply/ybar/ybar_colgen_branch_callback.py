"""
YBar column generation branch callback
"""

from cplex.callbacks import BranchCallback

class YBAR_COLGEN_BRANCH(BranchCallback):

    def __call__(self):

        self.branch_count += 1
        self.branch_node_count += self.get_num_branches()
