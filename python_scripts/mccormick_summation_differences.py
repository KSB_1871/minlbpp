"""
McCormick summation differences
"""

import cplex

n = 10
m = 10
M = 10

print("**********************************")
print("DMC - BEMC")

# DMC - BEMC
prob_DMCBEMC = cplex.Cplex()

# x variables
x_names = ["x_" + str(j) for j in range(n)]

prob_DMCBEMC.variables.add(obj   = [0] * n,
                   lb    = [0] * n,
                   ub    = [M] * n,
                   types = "C" * n,
                   names = x_names)

# y variables
y_names = [["y_" + str(i) + "_" + str(j) for j in range(n)] for i in range(m)]

for i in range(m):
    prob_DMCBEMC.variables.add(obj   = [0] * n,
                       lb    = [0] * n,
                       ub    = [1] * n,
                       types = "B" * n,
                       names = y_names[i])

# alpha variables
alpha_names = [["y_" + str(i) + "_" + str(j) for j in range(n)] for i in range(m)]

for i in range(m):
    prob_DMCBEMC.variables.add(obj   = [0] * n,
                       lb    = [0] * n,
                       ub    = [1] * n,
                       types = "B" * n,
                       names = y_names[i])

prob_DMCBEMC.solve()

print("**********************************")
print("BEMC - DMC")

# BEMC - DMC
prob_BEMCDMC = cplex.Cplex()

# x variables
x_names = ["x_" + str(j) for j in range(n)]

prob_BEMCDMC.variables.add(obj   = [0] * n,
                   lb    = [0] * n,
                   ub    = [M] * n,
                   types = "C" * n,
                   names = x_names)

# y variables
y_names = [["y_" + str(i) + "_" + str(j) for j in range(n)] for i in range(m)]

for i in range(m):
    prob_BEMCDMC.variables.add(obj   = [0] * n,
                       lb    = [0] * n,
                       ub    = [1] * n,
                       types = "B" * n,
                       names = y_names[i])

# alpha variables
alpha_names = [["y_" + str(i) + "_" + str(j) for j in range(n)] for i in range(m)]

for i in range(m):
    prob_BEMCDMC.variables.add(obj   = [0] * n,
                       lb    = [0] * n,
                       ub    = [1] * n,
                       types = "B" * n,
                       names = y_names[i])

prob_BEMCDMC.solve()