"""
Followers Problem for HPR
"""
import cplex
import numpy as np
import pandas as pd

class FOL_PROB():

    def __init__(self,x,xbar):

        print("Followers Problem")
        self.fol_prob = cplex.Cplex()
        self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)

        self.fol_x_vals = x
        self.fol_xbar_vals = xbar

    def solve_fol_prob(self,show_sol):


        self.gen_fol_prob()
        self.fol_prob_settings()
        self.fol_prob.solve()

        if show_sol == 1:
            self.fol_show_solutions()

        return self.fol_prob.solution.get_objective_value(),[[self.fol_prob.solution.get_values(self.fol_y_names[i][k]) for k in range(self.d[i])] for i in range(self.i)]

    def fol_prob_settings(self):
        self.fol_prob.set_log_stream(None)
        self.fol_prob.set_error_stream(None)
        self.fol_prob.set_warning_stream(None)
        self.fol_prob.set_results_stream(None)
        self.fol_prob.parameters.mip.display.set(0)

    def gen_fol_prob(self):
        self.fol_add_y_variables()
        self.fol_add_z_variables()
        self.fol_add_group_constraints()
        self.fol_supply_constraint()
        self.fol_z_constraints()

    def fol_add_y_variables(self):

        # Create Names
        self.fol_y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        # # Add variables
        for i in range(self.i):
            for k in range(self.d[i]):
                self.fol_prob.variables.add(obj= self.c,
                                lb = [0] * len(self.fol_y_names[i][k]),
                                ub = [1] * len(self.fol_y_names[i][k]),
                                types = "I" * len(self.fol_y_names[i][k]),
                                names = self.fol_y_names[i][k])

    def fol_add_z_variables(self):

        # Names
        self.fol_z_names = ["z_" + str(i) for i in range(self.n)]

        # Add z variables
        self.fol_prob.variables.add(obj = self.fol_x_vals,
                                lb = [0] * len(self.fol_z_names),
                                ub = self.supply,
                                types = "I" * len(self.fol_z_names),
                                names = self.fol_z_names)       

    def fol_x_values(self,x):

        self.fol_x_vals = x

    def fol_xbar_values(self,xbar):

        self.fol_xbar_vals = xbar

    def fol_add_group_constraints(self):

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(self.A_rows[i]):
                    self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.fol_y_names[i][k],val=list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = list(self.b[i][j]))

                self.fol_prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.fol_y_names[i][k],
                                                                              val=[1 for j in range(self.n)])],
                                                senses = ["E"],
                                                rhs = [self.min_y])

    def fol_supply_constraint(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                            val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                                    senses = ["L"],
                                                    rhs = [self.supply[j]],
                                                    names=['supply'])

    def fol_z_constraints(self):

        for j in range(self.n):
            self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_z_names[j]] + [self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                            val=[1] + [-1 for i in range(self.i) for k in range(self.d[i])])],
                                                    senses=["G"],
                                                    rhs=[self.fol_xbar_vals[j]-self.supply[j]])


    def fol_show_solutions(self):

        print("\n")
        print("FOL PROB OBJ:",self.fol_prob.solution.get_objective_value())
        vstack = np.vstack((self.supply,
                            self.fol_xbar_vals,
                            self.fol_prob.solution.get_values(self.fol_z_names),
                            self.fol_x_vals,
                            self.c,
                            [np.sum([self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)],
                            )).T
       
        df = pd.DataFrame(vstack,columns=["S","XBAR","Z","X","C","YSUM"])

        print(df)

# class FOL_PROB_NO_TAX():







    def quadratic_cutting_plane(self):

        ysum = [np.sum([int(self.fol_prob.solution.get_values(self.fol_y_names[i][k][j])) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]

        lin_ind = []
        lin_val = []

        quad_ind1 = []
        quad_ind2 = []
        quad_val  = []

        rhs = 1

        dummy = [[self.fol_y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] for j in range(self.n)]


        for j in range(self.n):

            if ysum[j] > 0.5:

                lin_ind += dummy[j]
                lin_val += [float(-2*ysum[j]) for i in range(len(dummy[j]))]

                for jj in range(len(dummy[j])):

                    quad_ind1 += [dummy[j][jj] for i in range(jj,len(dummy[j]))]
                    quad_ind2 += [dummy[j][i] for i in range(jj,len(dummy[j]))]
                    quad_val  += [1] + [2 for i in range(jj+1,len(dummy[j]))]

                rhs += float(-(ysum[j]**2))

            else:

                lin_ind += dummy[j]
                lin_val += [1 for i in range(len(dummy[j]))]

        # print("1",len(lin_ind),lin_ind)
        # print("2",len(lin_val),lin_val)
        # print("3",len(quad_ind1),quad_ind1)
        # print("4",len(quad_ind2),quad_ind2)
        # print("5",len(quad_val),quad_val)
        # print("6",rhs)

        self.fol_prob.quadratic_constraints.add(lin_expr=cplex.SparsePair(lin_ind,lin_val),
                                                quad_expr=cplex.SparseTriple(quad_ind1,quad_ind2,quad_val),
                                                sense="G",
                                                rhs=float(rhs))

    def linear_cutting_plane(self):

        print([self.fol_y_names[i][k][j]for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) > 0.5])
        if len([self.fol_y_names[i][k][j]for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) > 0.5]) != 8:
            print(hi)

        self.fol_prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.fol_y_names[i][k][j]for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) > 0.5],
                                                                        val=[1]*len([self.fol_y_names[i][k][j]for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) > 0.5]))],
                                             senses=["L"],
                                             rhs=[-1 + len([self.fol_y_names[i][k][j]for i in range(self.i) for k in range(self.d[i]) for j in range(self.n) if self.fol_prob.solution.get_values(self.fol_y_names[i][k][j]) > 0.5])])