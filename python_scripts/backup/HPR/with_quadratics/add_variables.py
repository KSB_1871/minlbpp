"""
HPR with quads add variables
"""
import numpy as np

class add_variables():

    def add_all_variables(self):

        self.add_x()
        self.add_z()
        self.add_xbar()
        self.add_y()
        self.add_p()

    def add_x(self):

        # Create Names
        self.x_names = ["x_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(lb = [0] * self.n,
                                ub = [self.M] * self.n,
                                types = "C" * self.n,
                                names = self.x_names)

    def add_z(self):
        # z[j] = max{0,sum_i,sum_d[i] y[i][d[i]][j] - (supply[j] - xbar[j])}

        # Names
        self.z_names = ["z_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(lb = [0] * self.n,
                                ub = self.supply,
                                types = "I" * self.n,
                                names = self.z_names)

    def add_xbar(self):
        # If leader purchases commodity j, xbar[j] >= 1

        # Create Names
        self.xbar_names = ["xbar_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(lb = [0] * self.n,
                                ub = self.supply,
                                types = "I" * self.n,
                                names = self.xbar_names)
                                #obj= [-float(self.c[j]) for j in range(self.n)],

    def add_y(self):
        # Followers responses

        # Create Names
        self.y_names = [[["y_" + str(i) + "_" + str(j) + "_" + str(k) for k in range(self.n)] for j in range(self.d[i])] for i in range(self.i)]

        # # Add variables
        for i in range(self.i):
            for j in range(self.d[i]):
                self.prob.variables.add(lb = [0] * len(self.y_names[i][j]),
                                ub = [1] * len(self.y_names[i][j]),
                                types = "I" * len(self.y_names[i][j]),
                                names = self.y_names[i][j])

    def add_p(self):
        # p[j] = 1 iff (supply[j] - xbar[j]) >= sum_i,sum_d[i] y[i][d[i]][j]
        # p[j] = 0 iff (supply[j] - xbar[j]) <= sum_i,sum_d[i] y[i][d[i]][j]

        self.p_names = ["p_" + str(i) for i in range(self.n)]

        # Add variables
        self.prob.variables.add(lb = [0] * self.n,
                                ub = [1] * self.n,
                                types = "I" * self.n,
                                names = self.p_names)

    def add_alpha(self):
        # alpha is the binary exapnsion of z
        self.logk = [int(np.floor(np.log2(self.supply[j]))) + 1 for j in range(self.n)]

        self.alpha_names = [["alpha_" + str(j) + "_" + str(l) for l in range(self.logk[j])] for j in range(self.n)]

        for j in range(self.n):
            self.prob.variables.add(lb = [0] * self.logk[j],
                                    ub = [1] * self.logk[j],
                                    types = "I" * self.logk[j],
                                    names = self.alpha_names[j])

    def add_q(self):

        self.q_names = [["q_" + str(j) + "_" + str(l) for l in range(self.logk[j])] for j in range(self.n)]

        for j in range(self.n):
            self.prob.variables.add(lb = [0] * self.logk[j],
                                    ub = [self.M] * self.logk[j],
                                    types = "C" * self.logk[j],
                                    names = self.q_names[j])
        
    def add_r(self):

        self.r_names += [["r_" + str(self.no_solves) + "_" + str(j) for j in range(self.n)]]

        self.prob.variables.add(lb = [0] * self.n,
                                ub = [1] * self.n,
                                types = "B" * self.n,
                                names = self.r_names[self.no_solves])

    def add_t(self):

        self.t_names += [["t_" + str(self.no_solves) + "_" + str(j) for j in range(self.n)]]

        self.prob.variables.add(lb = [0] * self.n,
                                ub = self.supply,
                                types = "I" * self.n,
                                names = self.t_names[self.no_solves])

    def add_beta(self):

        # beta is the binary exapnsion of t
        self.beta_names += [[["beta_" + str(self.no_solves) + "_" + str(j) + "_" + str(l) for l in range(self.logk[j])] for j in range(self.n)]]

        for j in range(self.n):
            self.prob.variables.add(lb = [0] * self.logk[j],
                                    ub = [1] * self.logk[j],
                                    types = "I" * self.logk[j],
                                    names = self.beta_names[self.no_solves][j])

    def add_u(self):

        self.u_names += [[["u_" + str(self.no_solves) + "_" + str(j) + "_" + str(l) for l in range(self.logk[j])] for j in range(self.n)]]

        for j in range(self.n):
            self.prob.variables.add(lb = [0] * self.logk[j],
                                    ub = [self.M] * self.logk[j],
                                    types = "C" * self.logk[j],
                                    names = self.u_names[self.no_solves][j])
