"""
HPR with quadratics
"""
import numpy as np
import cplex
import pandas as pd

from add_variables import add_variables
from add_constraints import add_constraints
from set_objective import set_objective
from lazy_constraint_callback import lazy_callback

from followers_problem import FOL_PROB


class HPR(add_variables,add_constraints,set_objective):

    def __init__(self,json_name):

        print("HPR Input at ",json_name)
        np.random.seed(0)
        self.n = 5
        self.i = 1
        self.d = [1]
        self.A_rows = [0,0]
        self.A_coef_min = 0
        self.A_coef_max = 20
        self.b_coef_min = 3
        self.b_coef_max = 5
        self.min_y = 3
        self.c_min = 1
        self.c_max = 10
        self.supply = np.ones((self.n))
        self.c = [float(np.random.randint(low=self.c_min,high=self.c_max)) for j in range(self.n)]#[2,3,4,5,6]
        self.M = 1000000
        self.B = 25

        self.prob = cplex.Cplex()
        self.prob.objective.set_sense(self.prob.objective.sense.maximize)

        self.add_all_variables()
        self.add_all_constraints()
        self.set_quadratic_objective()

        self.prob.parameters.optimalitytarget.set(3)


    def solve_hpr(self,show_results=0):

        self.lazy_call = self.prob.register_callback(lazy_callback)

        if show_results == 0:
            self.prob.set_log_stream(None)
            self.prob.set_error_stream(None)
            self.prob.set_warning_stream(None)
            self.prob.set_results_stream(None)
            self.prob.parameters.mip.display.set(0)

        self.prob.solve()

        x_results = np.array([self.prob.solution.get_values(self.x_names)]).T
        z_results = np.array([self.prob.solution.get_values(self.z_names)]).T
        y_results = np.array([[np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]]).T

        results_df = pd.DataFrame(data=np.hstack((np.array([self.c]).T,
                                                  x_results,
                                                  z_results,
                                                  y_results)),
                                  columns=['C','X','Z','Y'])

        print(results_df)

    def solve_hpr_column_generation(self,show_results=0):

        self.no_solves = 0
        self.r_names = []
        self.t_names = []
        self.beta_names = []
        self.u_names = []
        self.y_sum_solutions = []
                
        if show_results == 0:
            self.prob.set_log_stream(None)
            self.prob.set_error_stream(None)
            self.prob.set_warning_stream(None)
            self.prob.set_results_stream(None)
            self.prob.parameters.mip.display.set(0)

        while True:

            if self.no_solves >= 3:
                break

            self.prob.solve()

            x_results = np.array([self.prob.solution.get_values(self.x_names)]).T
            xbar_results = np.array([self.prob.solution.get_values(self.xbar_names)]).T
            z_results = np.array([self.prob.solution.get_values(self.z_names)]).T
            y_results = np.array([[np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]]).T

            if self.no_solves == 0:

                results_df = pd.DataFrame(data=np.hstack((np.array([self.supply]).T,
                                                        np.array([self.c]).T,
                                                        xbar_results,
                                                        x_results,
                                                        z_results,
                                                        y_results)),
                                        columns=['S','C','XBAR','X','Z','Y'])

            else:

                alpha_results   = np.array([[np.sum([self.prob.solution.get_values(self.alpha_names[j][l]) for l in range(self.logk[j])]) for j in range(self.n)]]).T
                q_results       = np.array([[np.sum([(2**l)*self.prob.solution.get_values(self.q_names[j][l]) for l in range(self.logk[j])]) for j in range(self.n)]]).T
                
                r_results       = np.array([[self.prob.solution.get_values(self.r_names[o][j]) for o in range(self.no_solves)] for j in range(self.n)])
                t_results       = np.array([[self.prob.solution.get_values(self.t_names[o][j]) for o in range(self.no_solves)] for j in range(self.n)])

                results_df = pd.DataFrame(data=np.hstack((np.array([self.supply]).T,
                                                            np.array([self.c]).T,
                                                            xbar_results,
                                                            x_results,
                                                            z_results,
                                                            alpha_results,
                                                            q_results,
                                                            y_results,
                                                            np.array(self.y_sum_solutions).T,
                                                            r_results,
                                                            t_results)),
                                         columns=['S','C','XBAR','X','Z','ALPHA','Q','Y'] + ['YSOL_' + str(o) for o in range(self.no_solves)] + \
                                                                                            ['R_' + str(o) for o in range(self.no_solves)] + \
                                                                                            ['T_' + str(o) for o in range(self.no_solves)])




            print("LEADER********************")
            print(results_df)
            print("**************************")

            fol_prob = FOL_PROB(self.prob.solution.get_values(self.x_names),self.prob.solution.get_values(self.xbar_names))
            fol_prob.x_names = self.x_names
            fol_prob.xbar_names = self.xbar_names
            fol_prob.i = self.i
            fol_prob.d = self.d
            fol_prob.n = self.n
            fol_prob.c = self.c
            fol_prob.supply = self.supply
            fol_prob.A_rows = self.A_rows
            fol_prob.A = self.A
            fol_prob.b = self.b
            fol_prob.y_names = self.y_names
            fol_prob.z_names = self.z_names
            fol_prob.min_y = self.min_y

            fol_obj, fol_sol = fol_prob.solve_fol_prob(show_sol=1)

            # print(fol_obj,fol_sol)

            curr_fol_obj = np.sum(np.array(self.prob.solution.get_values(self.z_names)) * np.array(self.prob.solution.get_values(self.x_names)) + \
                        np.array([np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * np.array(self.c))

            if fol_obj < curr_fol_obj:
                print("CURR = ",curr_fol_obj," > ",fol_obj," NEW")

                # self.ysum = [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
                # self.y_sum_solutions += [[np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]]

                self.ysum = [np.sum([fol_sol[i][k][j] for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]
                self.y_sum_solutions += [[np.sum([fol_sol[i][k][j] for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]]

                if self.no_solves <= 0:
                    self.add_alpha()
                    self.add_q()

                    self.z_alpha_binary_expan_constraint()
                    self.add_q_constraints()

                # self.add_r()
                # self.add_t()
                # self.add_beta()
                # self.add_r_constraints()
                # self.add_t_constraints()
                # self.t_beta_binary_expan_constraint()
                # self.add_u()
                # self.add_u_constraints()
                # self.add_value_func_constraints()

            else:
                break



            self.no_solves += 1






hpr = HPR("data.json")
hpr.solve_hpr_column_generation(0)


