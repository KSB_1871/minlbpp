"""
Add constraints for HPR with quadratics
"""
import cplex
import numpy as np

class add_constraints():

    def __init__(self):
        print("ADD CONSTRAINTS CLASS")
        
    def add_all_constraints(self):

        self.group_constraints()
        self.budget_constraint()
        self.x_xbar_big_M_constraint()
        self.fol_supply_constraint()
        self.p_constraints()
        self.z_max_constraints()


    def group_constraints(self):

        self.A = [i for i in range(self.i)]
        self.b = [i for i in range(self.i)]

        for i in range(self.i):

            # Create Constraints matrix
            self.A[i] = np.random.randint(  low=self.A_coef_min,
                                    high=self.A_coef_max,
                                    size=(self.A_rows[i],self.n))

            self.b[i] = np.random.randint(  low=self.b_coef_min,
                                    high=self.b_coef_max,
                                    size=(self.A_rows[i],1))

            self.A[i] = self.A[i].astype(float)
            self.b[i] = self.b[i].astype(float)

        for i in range(self.i):
            for k in range(self.d[i]):
                for j in range(self.A_rows[i]):
                    self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                                      val=list(self.A[i][j,:]))],
                                                        senses = ["G"],
                                                        rhs = list(self.b[i][j]))

                # Equality constraint for follower solutions

                self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=self.y_names[i][k],
                                                                              val=[1 for j in range(self.n)])],
                                                senses = ["E"],
                                                rhs = [self.min_y])

    def budget_constraint(self):

        # Add budget constraint

        self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind = self.xbar_names,val = self.c)],
                                         senses = ["L"],
                                         rhs = [self.B],
                                         names = ['Budget'])

    def x_xbar_big_M_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr = [cplex.SparsePair(ind=[self.x_names[j],self.xbar_names[j]],
                                                                          val=[1,-self.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["x_xbar_M_" + str(j)])

    def fol_supply_constraint(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])],
                                                                        val=[1 for i in range(self.i) for k in range(self.d[i])])],
                                             senses = ["L"],
                                             rhs = [self.supply[j]],
                                             names = ["Fol_supply_" + str(j)])

    def p_constraints(self):


        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.p_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + [self.xbar_names[j]],
                                                                        val=[self.supply[j]] + [1 for i in range(self.i) for k in range(self.d[i])] + [1])],
                                             senses = ["G"],
                                             rhs = [self.supply[j]],
                                             names = ["p_con_1_" + str(j)])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.p_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + [self.xbar_names[j]],
                                                                        val=[-self.supply[j]] + [-1 for i in range(self.i) for k in range(self.d[i])] + [-1])],
                                             senses = ["G"],
                                             rhs = [-2*self.supply[j]],
                                             names = ["p_con_2_" + str(j)])

    def z_max_constraints(self):

        for j in range(self.n):
            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + [self.xbar_names[j]],
                                                                        val=[1] + [-1 for i in range(self.i) for k in range(self.d[i])] + [-1])],
                                             senses = ["G"],
                                             rhs = [-self.supply[j]],
                                             names = ["z_con_1_" + str(j)])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j]] + [self.p_names[j]],
                                                                        val=[1] + [self.supply[j]])],
                                             senses = ["L"],
                                             rhs = [self.supply[j]],
                                             names = ["z_con_2_" + str(j)])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j]] + [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])] + [self.xbar_names[j]] + [self.p_names[j]],
                                                                        val=[1] + [-1 for i in range(self.i) for k in range(self.d[i])] + [-1] + [-self.supply[j]])],
                                             senses = ["L"],
                                             rhs = [-self.supply[j]],
                                             names = ["z_con_3_" + str(j)])

    def z_alpha_binary_expan_constraint(self):

        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.z_names[j]] + [self.alpha_names[j][l] for l in range(self.logk[j])],
                                                                    val=[-1] + [2**l for l in range(self.logk[j])]) for j in range(self.n)],
                                         senses  =["E" for j in range(self.n)],
                                         rhs     =[0 for j in range(self.n)])

    def t_beta_binary_expan_constraint(self):

        print(self.t_names[self.no_solves])

        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.t_names[self.no_solves][j]] + [self.beta_names[self.no_solves][j][l] for l in range(self.logk[j])],
                                                                    val=[-1] + [2**l for l in range(self.logk[j])]) for j in range(self.n)],
                                         senses  =["E" for j in range(self.n)],
                                         rhs     =[0 for j in range(self.n)])

    def add_q_constraints(self):

        for j in range(self.n):
            for l in range(self.logk[j]):
                self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.q_names[j][l]] + [self.x_names[j]] + [self.alpha_names[j][l]],
                                                                            val=[1] + [-1] + [-self.M])],
                                             senses = ["G"],
                                             rhs = [-self.M],
                                             names = ["q_con_1_" + str(j) + "_" + str(l)])

                self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.q_names[j][l]] + [self.alpha_names[j][l]],
                                                                            val=[1] + [-self.M])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["q_con_2_" + str(j) + "_" + str(l)])

                self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.q_names[j][l]] + [self.x_names[j]],
                                                                            val=[1] + [-1])],
                                             senses = ["L"],
                                             rhs = [0],
                                             names = ["q_con_3_" + str(j) + "_" + str(l)])

    def add_r_constraints(self):
        
        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.r_names[self.no_solves][j],self.xbar_names[j]],
                                                                    val=[self.supply[j],-1]) for j in range(self.n)],
                                         senses=["G" for j in range(self.n)],
                                         rhs=[(self.y_sum_solutions[self.no_solves][j] - self.supply[j]) for j in range(self.n)])

        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.r_names[self.no_solves][j],self.xbar_names[j]],
                                                                    val=[-self.supply[j],1]) for j in range(self.n)],
                                         senses=["G" for j in range(self.n)],
                                         rhs=[(-self.y_sum_solutions[self.no_solves][j]) for j in range(self.n)])

    def add_t_constraints(self):

        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.t_names[self.no_solves][j],self.xbar_names[j]],
                                                                    val=[1,-1]) for j in range(self.n)],
                                         senses = ["G" for j in range(self.n)],
                                         rhs = [(self.y_sum_solutions[self.no_solves][j] - self.supply[j]) for j in range(self.n)])

        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.t_names[self.no_solves][j],self.r_names[self.no_solves][j]],
                                                                    val=[1,-self.supply[j]]) for j in range(self.n)],
                                         senses = ["L" for j in range(self.n)],
                                         rhs = [0 for j in range(self.n)])

        self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.t_names[self.no_solves][j],self.xbar_names[j],self.r_names[self.no_solves][j]],
                                                                    val=[1,-1,self.supply[j]]) for j in range(self.n)],
                                         senses = ["L" for j in range(self.n)],
                                         rhs = [(self.y_sum_solutions[self.no_solves][j]) for j in range(self.n)])

    def add_u_constraints(self):

        print(self.no_solves)

        for j in range(self.n):

            # print([[self.u_names[self.no_solves][j][l],self.beta_names[self.no_solves][j][l]] for l in range(self.logk[j])])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[self.no_solves][j][l],self.beta_names[self.no_solves][j][l]],
                                                                        val=[1,-self.supply[j]]) for l in range(self.logk[j])],
                                             senses  =["L" for l in range(self.logk[j])],
                                             rhs     =[0 for l in range(self.logk[j])])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[self.no_solves][j][l],self.x_names[j]],
                                                                        val=[1,-1]) for l in range(self.logk[j])],
                                             senses  =["L" for l in range(self.logk[j])],
                                             rhs     =[0 for l in range(self.logk[j])])

            self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.u_names[self.no_solves][j][l],self.beta_names[self.no_solves][j][l],self.x_names[j]],
                                                                        val=[-1,self.supply[j],1]) for l in range(self.logk[j])],
                                             senses  =["L" for l in range(self.logk[j])],
                                             rhs     =[0 for l in range(self.logk[j])])


    def add_value_func_constraints(self):

        print("H")
        # self.prob.linear_constraints.add(lin_expr=[cplex.SparsePair(ind=[self.q_names[j][l] for j in range(self.n) for l in range(self.logk[j])] + \
        #                                                                 [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
        #                                                                 [self.u_names[self.no_solves][j][l] for j in range(self.n) for l in range(self.logk[j])],
        #                                                             val=[2**l for j in range(self.n) for l in range(self.logk[j])] + \
        #                                                                 [self.c[j] for i in range(self.i) for k in range(self.d[i]) for j in range(self.n)] + \
        #                                                                 [-2**l for j in range(self.n) for l in range(self.logk[j])])],
        #                                  senses  =["L"],
        #                                  rhs     =[float(np.sum(np.array(self.y_sum_solutions[self.no_solves])*np.array(self.c)))])