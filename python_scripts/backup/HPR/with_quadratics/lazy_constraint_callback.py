"""
HPR quad Lazy constraint
"""
from cplex.callbacks import  LazyConstraintCallback
# from followers_problem import FOL_PROB
import cplex
import numpy as np
import pandas as pd

class lazy_callback(LazyConstraintCallback):

    def __call__(self):

        print("HI")
        print(self.get_values(["x_" + str(i)for i in range(5)]))
        print(self.get_values(["x_0"]))

        if self.get_values("x_0") > 50:
            self.add(constraint=cplex.SparsePair(ind=["x_0"],
                                                    val=[1]),
                                                    sense='L',
                                                    rhs=50)