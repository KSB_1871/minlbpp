"""
HPR with quadratics objective
"""
import numpy as np
import cplex

class set_objective():

    def set_quadratic_objective(self):

        qmat = [[[i+self.n],[0.5]] for i in range(self.n)]

        qmat += [[[i],[0.5]] for i in range(self.n)]

        for i in range(3):
            qmat += [[[i],[0]] for i in range(self.n)]


        self.prob.objective.set_quadratic(qmat)