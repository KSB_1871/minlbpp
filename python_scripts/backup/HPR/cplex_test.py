"""
CPLEX test
"""
import numpy as np
import cplex

from leaders_problem import Prob
from followers_problem import FOL_PROB

# from add_variables import ADD_VARIABLES
# from add_constraints import ADD_CONSTRAINTS
# from followers_problem import FOL_PROB


# class Prob(ADD_VARIABLES,ADD_CONSTRAINTS,FOL_PROB):

#     def __init__(self):

#         print("Prob class")
#         self.prob = cplex.Cplex()
#         self.prob.objective.set_sense(self.prob.objective.sense.maximize)
#         self.fol_prob = cplex.Cplex()
#         self.fol_prob.objective.set_sense(self.fol_prob.objective.sense.minimize)
#         np.random.seed(0)


# Get problem specific information
Prob = Prob()
Prob.n = 20
Prob.i = 2
Prob.d = [1,2]
Prob.A_rows = [0,0]
Prob.A_coef_min = 0
Prob.A_coef_max = 20
Prob.b_coef_min = 3
Prob.b_coef_max = 5
Prob.min_y = 3
Prob.c_min = 1
Prob.c_max = 10
Prob.supply = np.ones((Prob.n))
Prob.c = [float(np.random.randint(low=Prob.c_min,high=Prob.c_max)) for j in range(Prob.n)]#[2,3,4,5,6]
Prob.M = 1000000
Prob.B = 20
Prob.show_lazy_callback = 0


# Add variables
Prob.add_all_variables()
Prob.add_all_constraints()
Prob.add_lazy_callback()
Prob.add_node_callback()

# Add constraints
Prob.budget_constraint()

# Define Followers Problem
#Prob.solve_fol_prob([10 for i in range(Prob.n)],[2 for i in range(Prob.n)])



#Prob.fol_show_solutions()

Prob.solve_leaders_problem()

# Prob.solve_fol_prob(Prob.prob.solution.get_values(Prob.x_names),Prob.prob.solution.get_values(Prob.xbar_names))
# Prob.fol_show_solutions()