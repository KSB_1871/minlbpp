"""
HPR LAZY CONSTRAINT CALLBACK
"""
from cplex.callbacks import  LazyConstraintCallback
from followers_problem import FOL_PROB
import cplex
import numpy as np
import pandas as pd

# The class BendersLazyConsCallback 
# allows to add Benders' cuts as lazy constraints.
# 
class LazyConsCallback(LazyConstraintCallback):
        
    def __call__(self):    

        # Add to number of callbacks
        self.no_lazy_callbacks += 1

        # Current solution
        curr_vstack = np.vstack((self.supply,
                                 self.c,
                                 self.get_values(self.xbar_names),
                                 self.get_values(self.x_names),
                                 [np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)])).T

        curr_df = pd.DataFrame(curr_vstack,columns=['S','C','XBAR','X','YSUM'])


        fol_prob = FOL_PROB(self.get_values(self.x_names),self.get_values(self.xbar_names))
        fol_prob.x_names = self.x_names
        fol_prob.xbar_names = self.xbar_names
        fol_prob.i = self.i
        fol_prob.d = self.d
        fol_prob.n = self.n
        fol_prob.c = self.c
        fol_prob.supply = self.supply
        fol_prob.A_rows = self.A_rows
        fol_prob.A = self.A
        fol_prob.b = self.b
        fol_prob.y_names = self.y_names
        fol_prob.z_names = self.z_names
        fol_prob.logk = self.logk
        fol_prob.q_names = self.q_names
        fol_prob.min_y = self.min_y

        fol_obj, fol_sol = fol_prob.solve_fol_prob(show_sol=0)

        curr_fol_obj = np.array(self.get_values(self.z_names)) * np.array(self.get_values(self.x_names)) + \
                    np.array([np.sum([self.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)]) * np.array(self.c)

        if fol_obj < np.sum(curr_fol_obj):

            # Add number of cuts
            self.no_lazy_cuts += 1

            IND = []
            VAL = []

            # THIS WORKS WHEN THE SUPPLY IS 1 FOR ALL COMMODITIES

            for j in range(self.n):
                # Q
                IND += self.q_names[j]
                VAL += [2**l for l in range(self.logk[j])]
                
                # Y
                IND += [self.y_names[i][k][j] for i in range(self.i) for k in range(self.d[i])]
                VAL += [self.c[j] for i in range(self.i) for k in range(self.d[i])]
            
                # X 
                IND += [self.x_names[j] for i in range(self.i) for k in range(self.d[i])]
                VAL += [-fol_sol[i][k][j] for i in range(self.i) for k in range(self.d[i])]
            

            self.add(constraint=cplex.SparsePair(ind=IND,
                                                 val=VAL),
                     sense = "L",
                     rhs = np.sum([fol_sol[i][k][j]*self.c[j] for j in range(self.n) for i in range(self.i) for k in range(self.d[i])]))

        if self.show_lazy_callback == 1:

            print("\n")
            print("INTEGER SOLUTION")
            print("CUR FOL OBJ = ",np.sum(curr_fol_obj))
            print("NEW FOL OBJ = ",fol_obj)

            if fol_obj > np.sum(curr__fol_obj) + 0.0001:
                print("Cut added")
