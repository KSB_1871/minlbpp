"""
Leaders Problem for HPR
"""
import numpy as np
import cplex

from add_variables import ADD_VARIABLES
from add_constraints import ADD_CONSTRAINTS
from followers_problem import FOL_PROB
from lazy_constraint_callback import LazyConsCallback
from node_callback import Node_Callback
import pandas as pd

class Prob(ADD_VARIABLES,ADD_CONSTRAINTS,FOL_PROB):

    def __init__(self):

        print("Prob class")
        self.prob = cplex.Cplex()
        self.prob.objective.set_sense(self.prob.objective.sense.maximize)

        np.random.seed(0)

    def add_lazy_callback(self):

        self.lazy_call = self.prob.register_callback(LazyConsCallback)
        self.lazy_call.x_names = self.x_names
        self.lazy_call.xbar_names = self.xbar_names
        self.lazy_call.i = self.i
        self.lazy_call.d = self.d
        self.lazy_call.n = self.n
        self.lazy_call.c = self.c
        self.lazy_call.supply = self.supply
        self.lazy_call.A_rows = self.A_rows
        self.lazy_call.A = self.A
        self.lazy_call.b = self.b
        self.lazy_call.y_names = self.y_names
        self.lazy_call.z_names = self.z_names
        self.lazy_call.logk = self.logk
        self.lazy_call.q_names = self.q_names
        self.lazy_call.min_y = self.min_y
        self.lazy_call.show_lazy_callback = self.show_lazy_callback
        self.lazy_call.no_lazy_callbacks = 0
        self.lazy_call.no_lazy_cuts = 0

    def add_node_callback(self):
        
        self.node_call = self.prob.register_callback(Node_Callback)
        self.node_call.node_count = 0

    

    def solve_leaders_problem(self):

        self.lead_prob_settings()
        #self.add_lazy_callback()
        self.prob.solve()
        self.lead_show_solutions()

    def lead_prob_settings(self):

        print("HI")
        # self.prob.set_log_stream(None)
        # self.prob.set_error_stream(None)
        # self.prob.set_warning_stream(None)
        # self.prob.set_results_stream(None)
        # self.prob.parameters.mip.display.set(0)

    def lead_show_solutions(self):

        print("OBJ    = ",self.prob.solution.get_objective_value())
        print("INC    = ",np.sum([self.prob.solution.get_values(self.x_names[j])*self.prob.solution.get_values(self.z_names[j]) for j in range(self.n)]))
        print("EXP    = ",np.sum([self.prob.solution.get_values(self.xbar_names[j])*self.c[j] for j in range(self.n)]))

        print("F.COST = ",np.sum([self.prob.solution.get_values(self.x_names[j])*self.prob.solution.get_values(self.z_names[j]) for j in range(self.n)]) + np.sum([np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])])*self.c[j] for j in range(self.n)]))

        # Create VStack

        vstack = np.vstack((self.supply,
                            self.prob.solution.get_values(self.xbar_names),
                            self.prob.solution.get_values(self.z_names),
                            self.prob.solution.get_values(self.x_names),
                            self.c,
                            [np.sum([self.prob.solution.get_values(self.y_names[i][k][j]) for i in range(self.i) for k in range(self.d[i])]) for j in range(self.n)])).T

        df = pd.DataFrame(vstack,columns=["S","XBAR","Z","X","C","YSUM"])
        print("\n")
        print(df)

        print("\n")

        fol_prob = FOL_PROB(self.prob.solution.get_values(self.x_names),
                                                   self.prob.solution.get_values(self.xbar_names))
        fol_prob.x_names = self.x_names
        fol_prob.xbar_names = self.xbar_names
        fol_prob.i = self.i
        fol_prob.d = self.d
        fol_prob.n = self.n
        fol_prob.c = self.c
        fol_prob.supply = self.supply
        fol_prob.A_rows = self.A_rows
        fol_prob.A = self.A
        fol_prob.b = self.b
        fol_prob.y_names = self.y_names
        fol_prob.z_names = self.z_names
        fol_prob.logk = self.logk
        fol_prob.q_names = self.q_names
        fol_prob.min_y = self.min_y
        fol_obj, fol_sol = fol_prob.solve_fol_prob(show_sol=1)

        # Technical Results
        tech_results = [["NO_LAZY_CALLBACKS",self.lazy_call.no_lazy_callbacks],
                        ["NO_LAZY_CUTS",self.lazy_call.no_lazy_cuts],
                        ["NO_NODES_SOL",self.node_call.node_count]]

        tech_results_df = pd.DataFrame(tech_results)

        print("\n")
        print(tech_results_df)

        


    
