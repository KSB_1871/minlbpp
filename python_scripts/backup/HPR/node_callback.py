"""
HPR NODE CALLBACK
"""
from cplex.callbacks import  NodeCallback


# The class BendersLazyConsCallback 
# allows to add Benders' cuts as lazy constraints.
# 
class Node_Callback(NodeCallback):
        
    def __call__(self):    

        self.node_count += 1
