"""
Iterativly solve followers problem
"""
import numpy as np
import pandas as pd
import cplex
import sys

sys.path.append('../HPR')

from followers_problem import FOL_PROB
# from followers_problem import FOL_PROB_NO_TAX

np.random.seed(0)

n = 9
m = 10
ygen = FOL_PROB([0 for i in range(n)],[0 for i in range(n)])
ygen.n = n
ygen.m = m
ygen.i = 2
ygen.d = [2,2]
ygen.A_rows = [0,0]
ygen.A_coef_min = 0
ygen.A_coef_max = 20
ygen.b_coef_min = 3
ygen.b_coef_max = 5
ygen.min_y = 2
ygen.c_min = 1
ygen.c_max = 10
ygen.supply = np.ones((ygen.n))
ygen.c = [float(np.random.randint(low=ygen.c_min,high=ygen.c_max)) for j in range(ygen.n)]
ygen.Y = []
ygen.Y_cost = []

ygen.fol_prob = cplex.Cplex()
ygen.fol_prob.objective.set_sense(ygen.fol_prob.objective.sense.minimize)

ygen.gen_fol_prob()
ygen.fol_prob_settings()

print(ygen.c)

count = 0
while True:

    if len(ygen.Y) == ygen.m:
        break

    try:
        ygen.fol_prob.solve()
        ygen.fol_show_solutions()

        ysum = [np.sum([int(ygen.fol_prob.solution.get_values(ygen.fol_y_names[i][k][j])) for i in range(ygen.i) for k in range(ygen.d[i])]) for j in range(ygen.n)]

        print("YSUM = ",ysum,ygen.fol_prob.solution.get_objective_value())

        y_sol = [[ygen.fol_prob.solution.get_values(ygen.fol_y_names[i][k]) for k in range(ygen.d[i])] for i in range(ygen.i)]
        y_sol_names = [ygen.fol_y_names[i][k][j] for i in range(ygen.i) for k in range(ygen.d[i]) for j in range(ygen.n) if y_sol[i][k][j] > 0.5]

        if ysum not in ygen.Y:
            ygen.Y += [ysum]
            ygen.Y_cost += [ygen.fol_prob.solution.get_objective_value()]
            Y_df = pd.DataFrame(np.vstack((np.array(ygen.Y).T,np.array(ygen.Y_cost))),index=[i for i in range(ygen.n)] + ["Cost"])
            print(Y_df)




        #ygen.quadratic_cutting_plane()
        ygen.linear_cutting_plane()



    except:
        print("Fol Problem has no solution")
        break

print("\n")
print("YSUM")
# ygen.Y = np.round(ygen.Y,0)
print(ygen.c)
Y_df = pd.DataFrame(np.vstack((np.array(ygen.Y).T,np.array(ygen.Y_cost))),index=[i for i in range(ygen.n)] + ["Cost"])
print(Y_df)
